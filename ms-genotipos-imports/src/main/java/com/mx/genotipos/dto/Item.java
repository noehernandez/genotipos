/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

import lombok.Data;

/**
 *
 * @author NS-448
 */
@Data
public class Item {
    private Integer id;
    private String name;

    public Item(Integer id) {
        this.id = id;
    }
    public Item(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public Item(){};
}
