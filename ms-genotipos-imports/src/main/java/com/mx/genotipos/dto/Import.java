/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

import java.util.List;
import lombok.*;

/**
 *
 * @author NS-448
 */
@Data
//@AllArgsConstructor
@NoArgsConstructor
public class Import {
    
    private Integer id;
    private String sample;
    private String observations;
    private String importId;
    private String importDate;
    private Integer importedProfiles;
    private String user;
    private String title;
    private boolean active;
    private List<Item> labels;
    private Item source;

    public Import(Integer id,String sourceName, String sample, String observations, String importId, String importDate, Integer importedProfiles, String user, String title, boolean active, List<Item> labels, Integer sourceId) {
        this.id = id;
        this.sample = sample;
        this.observations = observations;
        this.importId = importId;
        this.importDate = importDate;
        this.importedProfiles = importedProfiles;
        this.user = user;
        this.title = title;
        this.active = active;
        this.labels = labels;
        this.source = new Item(sourceId,sourceName);
    }
    
    

}
