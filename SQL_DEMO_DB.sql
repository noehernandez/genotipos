
create database demo_db
use demo_db

CREATE TABLE [dbo].[CAT_TIPO_MARCADOR](
	[tm_id] [int]primary key IDENTITY(1,1) NOT NULL,
	[tm_descripcion] [varchar](50) NULL,
	[tm_fecha_registro] [date] NULL,
	[tm_baja_logica] [bit] NULL);

insert into CAT_TIPO_MARCADOR
(tm_descripcion,tm_fecha_registro,tm_baja_logica)
values
('AUTOSIMICO',getdate(),1),
('CROMOSOMA Y',getdate(),1),
('CROMOSOMA X',getdate(),1);

CREATE TABLE [dbo].[CAT_MARCADORES](
	[mar_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[mar_nombre] [varchar](50) NULL,
	[mar_baja_logica] [int] NULL,
	[mar_fecha_registro] [date] NULL,
	[mar_tipo] [int] references CAT_TIPO_MARCADOR(tm_id),
	[mar_usuario_creo] [int] NULL,
	[mar_usuario_edito] [int] NULL);

insert into CAT_MARCADORES
(mar_nombre,mar_baja_logica,mar_fecha_registro,mar_tipo)
values
('D8S1179',1,getdate(),1),
('D21S11',1,getdate(),1),
('D7S820',1,getdate(),1),
('CSF1PO',1,getdate(),1),
('D3S1358',1,getdate(),1),
('TH01',1,getdate(),1),
('D13S317',1,getdate(),1),
('D16S539',1,getdate(),1),
('D2S1338',1,getdate(),1),
('D19S433',1,getdate(),1),
('VWA',1,getdate(),1),
('TPOX',1,getdate(),1),
('D18S51',1,getdate(),1),
('AMEL',1,getdate(),1),
('D5S818',1,getdate(),1),
('FGA',1,getdate(),1),
('D1S1656',1,getdate(),1),
('D2S441',1,getdate(),1),
('D10S1248',1,getdate(),1),
('D12S391',1,getdate(),1),
('D22S1045',1,getdate(),1),
('SE33',1,getdate(),1),
('PENTAD',1,getdate(),1),
('PENTAE',1,getdate(),1),
('D6S1043',1,getdate(),1),
('D22S645',1,getdate(),1),
('YINDEL',1,getdate(),1),
('DYS391',1,getdate(),2),
('DYS456',1,getdate(),2),
('DYS389I',1,getdate(),2),
('DYS390',1,getdate(),2),
('DYS389II',1,getdate(),2),
('DYS458',1,getdate(),2),
('DYS19',1,getdate(),2),
('DYS385',1,getdate(),2),
('DYS393',1,getdate(),2),
('DYS439',1,getdate(),2),
('DYS635',1,getdate(),2),
('DYS392',1,getdate(),2),
('YGATAH4',1,getdate(),2),
('DYS437',1,getdate(),2),
('DYS438',1,getdate(),2),
('DYS448',1,getdate(),2),
('DYS481',1,getdate(),2),
('DYS533',1,getdate(),2),
('DYS549',1,getdate(),2),
('DYS570',1,getdate(),2),
('DYS576',1,getdate(),2),
('DYS643',1,getdate(),2),
('DYS387S1',1,getdate(),2),
('DYS449',1,getdate(),2),
('DYS460',1,getdate(),2),
('DYS518',1,getdate(),2),
('DYS627',1,getdate(),2),
('DXS7132',1,getdate(),3),
('DXS7423',1,getdate(),3),
('DXS8378',1,getdate(),3),
('DXS10074',1,getdate(),3),
('DXS10079',1,getdate(),3),
('DXS10101',1,getdate(),3),
('DXS10103',1,getdate(),3),
('DXS10134',1,getdate(),3),
('DXS10135',1,getdate(),3),
('DXS10146',1,getdate(),3),
('DXS10148',1,getdate(),3),
('HPRTB',1,getdate(),3);

CREATE TABLE [dbo].[CAT_TIPO_PERFIL](
	[tp_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[tp_descripcion] [varchar](50) NULL,
	[tp_fecha_registro] [date] NULL,
	[tp_baja_logica] [bit] NULL);

insert into CAT_TIPO_PERFIL
(tp_descripcion,tp_fecha_registro,tp_baja_logica)
values ('PERFIL ACTIVO',getdate(),1),
('PERFIL INACTIVO',getdate(),1),
('PERFIL DUPLICADO',getdate(),1),
('PERFIL EN REVISION',getdate(),1);

CREATE TABLE [dbo].[FUENTES](
	[fnt_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[fnt_nombre_fuente] [varchar](250) NULL,
	[fnt_identificador_interno] [varchar](250) NULL,
	[fnt_nombre_contacto] [varchar](250) NULL,
	[fnt_correo_contacto] [varchar](250) NULL,
	[fnt_telefono_contacto] [varchar](250) NULL,
	[fnt_otro_telefono_contacto] [varchar](250) NULL,
	[fnt_baja_logica] [int] NULL,
	[fnt_fecha_registro] [date] NULL,
	[fnt_identificador_externo] [varchar](250) NULL);

CREATE TABLE [dbo].[IMPORTACIONES](
	[imp_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[imp_identificador] [varchar](250) NULL,
	[imp_titulo] [varchar](250) NULL,
	[imp_fecha_importacion] [date] NULL,
	[imp_usuario] [varchar](250) NULL,
	[imp_observaciones] [varchar](500) NULL,
	[imp_baja_logica] [int] NULL,
	[imp_tipo_muestra] [varchar](100) NULL,
	[imp_ruta_archivo] [varchar](100) NULL,
	[imp_fuente_id] [int] references FUENTES(fnt_id));

CREATE TABLE [dbo].[PERFILES](
	[per_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[per_identificador] [varchar](150) NULL,
	[per_tipo] [int] references CAT_TIPO_PERFIL(tp_id),
	[per_baja_logica] [int] NULL,
	[per_fecha_registro] [date] NULL,
	[per_perfil_original] [int] NULL,
	[per_usuario_reviso] [int] NULL,
	[per_usuario_creo] [int] NULL,
	[per_id_importacion] [int] references IMPORTACIONES(imp_id),
	[per_motivo_desestimacion] [varchar](100) NULL);

	alter table perfiles add per_edo_id int
	alter table perfiles add per_identificador_interno varchar(150)

CREATE TABLE [dbo].[METADATOS](
	[cat_met_id] [int] IDENTITY(1,1) NOT NULL,
	[cat_met_descripcion] [varchar](255) NULL,
	[cat_met_fecha_registro] [date] NULL,
	[cat_met_baja_logica] [int] NULL);


CREATE TABLE [dbo].[MYP_METADATOS_PERFILES](
	[PER_PERFILES_per_id] [int] references PERFILES(per_id),
	[CAT_MET_METADATOS_cat_met_id] [int] references METADATOS(cat_met_id),
	[myp_valor] [varchar](255) NULL,
	[myp_baja_logica] [int] NULL,
	[myp_fecha_registro] [date] NULL,
	[myp_id] [int] IDENTITY(1,1) NOT NULL);


CREATE TABLE [dbo].[MARCADORES_PERFILES](
	[mp_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[mp_id_perfil] [int] references PERFILES(per_id),
	[mp_id_marcador] [int] references CAT_MARCADORES(mar_id),
	[mp_alelo_uno] [varchar](10) NULL,
	[mp_alelo_dos] [varchar](10) NULL,
	[mp_baja_logica] [int] NULL,
	[mp_fecha_registro] [date] NULL);

CREATE TABLE [dbo].[CATEGORIAS](
	[cat_categoria_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[cat_descripcion] [varchar](100) NULL,
	[cat_baja_logica] [int] NULL,
	[cat_fecha_registro] [date] NULL,
	[cat_id_usuario] [int] NULL);

CREATE TABLE [dbo].[ETIQUETAS](
	[eti_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[eti_categoria_id] [int] references CATEGORIAS(cat_categoria_id),
	[eti_descripcion] [varchar](100) NULL,
	[eti_baja_logica] [int] NULL,
	[eti_fecha_registro] [date] NULL,
	[eti_id_usuario] [int] NULL);

CREATE TABLE [dbo].[CAT_FRECUENCIAS](
	[fre_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[fre_nombre] [varchar](100) NULL,
	[fre_nombre_archivo] [varchar](100) NULL,
	[fre_baja_logica] [int] NULL,
	[fre_fecha_registro] [date] NULL,
	[fre_ruta_archivo] [varchar](500) NULL,
	[fre_id_importacion] [varchar](50) NULL,
	[fre_default] [bit] NULL);

	alter table CAT_FRECUENCIAS drop column fre_id_importacion
	alter table CAT_FRECUENCIAS drop column fre_id_usuario
	alter table CAT_FRECUENCIAS add fre_usuario int
	alter table CAT_FRECUENCIAS drop column fre_id_importacion
	alter table CAT_FRECUENCIAS add fre_id_importacion as 'I-'+CONVERT(varchar(30),fre_usuario)+'-'+CONVERT(varchar(30),fre_id)

CREATE TABLE [dbo].[CAT_ALELOS](
	[ale_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[ale_descripcion] [varchar](10) NULL,
	[ale_baja_logica] [int] NULL,
	[ale_fecha_registro] [date] NULL);

CREATE TABLE [dbo].[ALELOS_FRECUENCIA](
	[ayf_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[ayf_fre_id] [int] references CAT_FRECUENCIAS(fre_id),
	[ayf_ale_id] [int] references CAT_ALELOS(ale_id),
	[ayf_descripcion] [varchar](10) NULL,
	[ayf_valor_alelo] [float] NULL,
	[ayf_mar_id] [int] references CAT_MARCADORES(mar_id));

CREATE TABLE [dbo].[PERFIL_ETIQUETAS](
	[pye_eti_id] [int] references ETIQUETAS(eti_id), 
	[pye_per_id] [int] references PERFILES(per_id),
	[pye_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[pye_baja_logica] [int] NULL,
	[pye_fecha_registro] [date] NULL);

CREATE TABLE [dbo].[CAT_EDO_ESTADOS](
	[edo_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[edo_clave] [varchar](10) NULL,
	[edo_nombre] [varchar](50) NULL,
	[edo_abreviacion] [varchar](200) NULL,
	[edo_baja_logica] [int] NULL);


--=================================================
--============TABLAS DE BUSQUEDA===================
--=================================================
CREATE TABLE [dbo].[BUSQUEDA](
	[bus_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[bus_numero] [varchar](50) NULL,
	[cat_fuente_id] [int] NULL,
	[bus_usuario] [int] NULL,
	[bus_fecha_busqueda] [date] NULL,
	[bus_motivo] [varchar](50) NULL,
	[bus_descripcion] [varchar](50) NULL,
	[bus_marcadores_minimos] [int] NULL,
	[bus_exclusiones_maximas] [int] NULL,
	[bus_tipo] [varchar](50) NULL,
	[bus_estatus] [varchar](50) NULL,
	[bus_comentarios] [varchar](200) NULL,
	[cat_fre_id] [int] NULL,
	[bus_descartar_per_revision] [int] NULL,
	[bus_perfil_objetivo] [int] NULL,
	[bus_perfil_padre] [int] NULL,
	[bus_perfil_madre] [int] NULL);

	CREATE TABLE [dbo].[ETIQUETAS_BUSQUEDA](
	[eb_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[eb_bus_id] [int] references BUSQUEDA(bus_id),
	[eb_nombre] [varchar](100) NULL,
	[eb_perfil] [varchar](100) NULL);

CREATE TABLE [dbo].[RESULTADO_COMPATIBLE](
	[rc_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[rc_bus_id] [int] references BUSQUEDA(bus_id),
	[edo_id] [int] NULL,
	[rc_perfil_objetivo] [int] NULL,
	[rc_perfil_compatible] [int] NULL,
	[rc_ip] [varchar](50) NULL,
	[rc_pp] [varchar](50) NULL,
	[rc_amel] [varchar](50) NULL,
	[rc_perfil_madre] [int] NULL,
	[rc_perfil_padre] [int] NULL);

CREATE TABLE [dbo].[MARCADOR_COMPATIBLE](
	[mc_id] [int] primary key IDENTITY(1,1) NOT NULL,
	[mc_rc_id] [int] references RESULTADO_COMPATIBLE(rc_id),
	[cat_mar_id] [int] NULL,
	[cat_mar] [varchar](50) NULL,
	[mc_alelox_origen] [varchar](50) NULL,
	[mc_aleloy_origen] [varchar](50) NULL,
	[mc_alelox_compatible] [varchar](50) NULL,
	[mc_aleloy_compatible] [varchar](50) NULL,
	[mc_coincide_alelox] [int] NULL,
	[mc_coincide_aleloy] [int] NULL,
	[mc_alelox_madre] [varchar](50) NULL,
	[mc_aleloy_madre] [varchar](50) NULL,
	[mc_coincide_alelox_madre] [int] NULL,
	[mc_coincide_aleloy_madre] [int] NULL);

--=================================================
--=================================================
--=================================================



CREATE TABLE [dbo].[CAT_ACCION](
	[cat_accion_id] [int] IDENTITY(1,1) NOT NULL,
	[cat_tipo] [varchar](50) NULL,
	[cat_fecha_alta] [date] NULL,
	[cat_descripcion] [varchar](200) NULL,
	[cat_baja_logica] [int] NULL);


CREATE TABLE [dbo].[CAT_ERROR](
	[cat_error_id] [int] IDENTITY(1,1) NOT NULL,
	[cat_tipo] [varchar](50) NULL,
	[cat_fecha_alta] [date] NULL,
	[cat_descripcion] [varchar](200) NULL,
	[cat_baja_logica] [int] NULL);


CREATE TABLE [dbo].[CAT_LOG](
	[log_id] [int] IDENTITY(1,1) NOT NULL,
	[log_usuario] [int] NULL,
	[log_fecha] [date] NULL,
	[log_descripcion] [varchar](50) NULL,
	[CAT_ERROR_cat_error_id] [int] NULL,
	[CAT_ACCION_cat_accion_id] [int] NULL,
	[cat_baja_logica] [int] NULL);



----------------- VISTAS -------------------------------------------
--------------------------------------------------------------------
create view VISTA_IMPORTACIONES
as 
select 
F.fnt_nombre_fuente,
I.imp_id,
I.imp_identificador,
I.imp_baja_logica,
(select count(*) from PERFILES P where P.per_id_importacion=I.imp_id) as perfiles,
'Admin' as usuario,
I.imp_titulo,
I.imp_tipo_muestra,
I.imp_observaciones,
convert(varchar, I.imp_fecha_importacion, 101) as fecha_importacion,
I.imp_fuente_id
 from IMPORTACIONES I
left join FUENTES F on F.fnt_id=I.imp_fuente_id

create view VISTA_PERFILES
AS
select 
P.per_id,
P.per_identificador,
P.per_identificador_interno,
P.per_baja_logica,
convert(varchar, P.per_fecha_registro, 101) as per_fecha_registro,
P.per_perfil_original,
P.per_id_importacion,
(select count(*) from MARCADORES_PERFILES MP where MP.mp_id_perfil = P.per_id) as marcadores,
(select count(*) from MARCADORES_PERFILES MP where MP.mp_id_perfil = P.per_id and mp_alelo_uno=mp_alelo_dos) as homocigotos,
'ADMIN' as usuario,
TP.tp_id,
TP.tp_descripcion
from PERFILES P
left join CAT_TIPO_PERFIL TP on TP.tp_id = P.per_tipo ;

create view VISTA_BUSQUEDAS
as
select 
--*
B.bus_id,
B.bus_numero,
F.fnt_id,
F.fnt_nombre_fuente,
'ADMIN' as usuario,
B.bus_usuario as id_usuario,
B.bus_fecha_busqueda,
B.bus_motivo,
B.bus_descripcion,
B.bus_tipo,
B.bus_estatus,
B.bus_comentarios,
B.bus_marcadores_minimos,
B.bus_exclusiones_maximas,
CF.fre_id,
CF.fre_nombre,
B.bus_descartar_per_revision
from [dbo].[BUSQUEDA] B 
left join [dbo].[FUENTES] F on B.cat_fuente_id=F.fnt_id
left join [dbo].[CAT_FRECUENCIAS] CF on B.cat_fre_id=CF.fre_id



create view VISTA_RESULTADO_COMPATIBLE
as
select 
--* 
RC.rc_id,
RC.rc_bus_id,
'Mexico' as estado,
PO.per_id as perfil_objetivo_id,
PO.per_identificador as perfil_objetivo,
PC.per_id as perfil_compatible_id,
PC.per_identificador as perfil_compatible,
PM.per_id as perfil_madre_id,
PM.per_identificador as perfil_madre,
PP.per_id as perfil_padre_id,
PP.per_identificador as perfil_padre,
RC.rc_amel,
RC.rc_ip,
RC.rc_pp
from [dbo].[RESULTADO_COMPATIBLE] RC
left join [dbo].[PERFILES] PO on RC.rc_perfil_objetivo=PO.per_id
left join [dbo].[PERFILES] PC on RC.rc_perfil_compatible=PC.per_id
left join [dbo].[PERFILES] PM on RC.rc_perfil_madre=PM.per_id
left join [dbo].[PERFILES] PP on RC.rc_perfil_padre=PP.per_id

----------------- VISTAS -------------------------------------------
--------------------------------------------------------------------

------------------------- PROCEDIMIENTOS ---------------------------
--------------------------------------------------------------------
create PROCEDURE [dbo].[SP_BusquedaEtiquetaPerfil]
 (
   @user_id_list varchar(max),
   @user_id_list_Perfil varchar(max)
 )
AS
BEGIN
  
   SET NOCOUNT ON;
   SELECT cat_Eti_Etiquedas.eti_descripcion,pye_perfile.pye_per_id,
   myp.mp_id_marcador,
   myp.mp_alelo_uno,myp.mp_alelo_dos,catMAR.mar_nombre, per_perfiles.per_identificador,
   catEdos.edo_abreviacion, per_perfiles.per_edo_id
   FROM [dbo].[ETIQUETAS]  cat_Eti_Etiquedas
   INNER JOIN [dbo].[PERFIL_ETIQUETAS] pye_perfile ON pye_perfile.pye_eti_id = cat_Eti_Etiquedas.eti_id
   INNER JOIN [dbo].[MARCADORES_PERFILES] myp on myp.mp_id_perfil = pye_perfile.pye_per_id
   INNER JOIN [dbo].[CAT_MARCADORES] catMAR on catMAR.mar_id = myp.mp_id_marcador
   INNER JOIN [dbo].[PERFILES] per_perfiles on per_perfiles.per_id = pye_perfile.pye_per_id
   LEFT JOIN [dbo].[CAT_EDO_ESTADOS] catEdos on catEdos.edo_id = per_perfiles.per_edo_id
   WHERE cat_Eti_Etiquedas.eti_id IN (SELECT * FROM StringSplit(@user_id_list,',')) 
   and per_perfiles.per_tipo IN ( SELECT * FROM StringSplit(@user_id_list_Perfil,',') )
   order by cat_Eti_Etiquedas.eti_descripcion

END

select * from [CAT_EDO_ESTADOS]


create PROCEDURE [dbo].[SP_BusquedaPerfil]
(
   @user_id_list varchar(max)
 )
AS
BEGIN 
	
   SET NOCOUNT ON;
   select per_perfiles.per_id,
   myp.mp_id_marcador,
   myp.mp_alelo_uno, myp.mp_alelo_dos, 
   catMAR.mar_nombre,
   per_perfiles.per_identificador,
   catEdos.edo_abreviacion, per_perfiles.per_edo_id
   from  [dbo].[PERFILES]  per_perfiles
   INNER JOIN [dbo].[MARCADORES_PERFILES] myp on myp.mp_id_perfil = per_perfiles.per_id
   INNER JOIN [dbo].[CAT_MARCADORES] catMAR on catMAR.mar_id = myp.mp_id_marcador
   LEFT JOIN [dbo].[CAT_EDO_ESTADOS] catEdos on catEdos.edo_id = per_perfiles.per_edo_id
   where per_perfiles.per_id IN (SELECT * FROM StringSplit(@user_id_list,','))

END

------------------------- PROCEDIMIENTOS ---------------------------
--------------------------------------------------------------------

create FUNCTION [dbo].[StringSplit]
(
    @String  VARCHAR(MAX), @Separator CHAR(1)
)
RETURNS @RESULT TABLE(Value VARCHAR(MAX))
AS
BEGIN     
 DECLARE @SeparatorPosition INT = CHARINDEX(@Separator, @String ),
        @Value VARCHAR(MAX), @StartPosition INT = 1
 
 IF @SeparatorPosition = 0  
  BEGIN
   INSERT INTO @RESULT VALUES(@String)
   RETURN
  END
     
 SET @String = @String + @Separator
 WHILE @SeparatorPosition > 0
  BEGIN
   SET @Value = SUBSTRING(@String , @StartPosition, @SeparatorPosition- @StartPosition)
 
   IF( @Value <> ''  ) 
    INSERT INTO @RESULT VALUES(@Value)
   
   SET @StartPosition = @SeparatorPosition + 1
   SET @SeparatorPosition = CHARINDEX(@Separator, @String , @StartPosition)
  END    
     
 RETURN
END









---------------------------- querys
use PG2MX;

use demo_db

select * from CAT_ALE_ALELOS

select * from [demo_db].dbo.CAT_ALELOS 

select * from demo_db.dbo.CAT_FRECUENCIAS

alter table CAT_FRECUENCIAS drop column fre_id_importacion
alter table CAT_FRECUENCIAS add fre_id_importacion as 'I-'+CONVERT(varchar(30),fre_id_usuario)+'-'+CONVERT(varchar(30),fre_id)

select AF.ayf_id, AF.ayf_fre_id,AF.ayf_ale_id, CA.ale_descripcion, AF.ayf_valor_alelo, AF.ayf_mar_id, CM.mar_nombre
from ALELOS_FRECUENCIA AF 
left join CAT_ALELOS CA on AF.ayf_ale_id=CA.ale_id
left join CAT_MARCADORES CM on AF.ayf_mar_id=CM.mar_id

delete from ALELOS_frecuencia


insert into [demo_db].dbo.CAT_ALELOS (ale_descripcion, ale_baja_logica, ale_fecha_registro)
select cat_ale_descripcion,cat_ale_baja_logica,getdate() from PG2MX.dbo.CAT_ALE_ALELOS

select * from [dbo].[ETIQUETAS]

select * from [dbo].[PERFIL_ETIQUETAS]

insert into [dbo].[PERFIL_ETIQUETAS] 
(pye_per_id,pye_eti_id,pye_baja_logica,pye_fecha_registro)
values
(1,1,1getdate)


select * from 
[dbo].[CAT_TIPO_MARCADOR]

select * from [dbo].[CAT_MARCADORES]

--eliminar perfiles
delete  from PERFIL_ETIQUETAS;
delete from MARCADORES_PERFILES;
delete from PERFILES;
delete from IMPORTACIONES;
-----------------------

select * from PERFILES;
select * from IMPORTACIONES;

select * from [dbo].[CAT_FRECUENCIAS]
select * from [dbo].[ETIQUETAS]
select * from [dbo].[PERFIL_ETIQUETAS]

select * from [dbo].[MARCADORES_PERFILES]

 SELECT cat_Eti_Etiquedas.eti_descripcion,pye_perfile.pye_per_id,
   myp.mp_id_marcador,
   myp.mp_alelo_uno,myp.mp_alelo_dos,catMAR.mar_nombre, per_perfiles.per_identificador,
   catEdos.edo_abreviacion, per_perfiles.per_edo_id
   FROM [demo_db].[dbo].[ETIQUETAS]  cat_Eti_Etiquedas
   INNER JOIN [demo_db].[dbo].[PERFIL_ETIQUETAS] pye_perfile ON pye_perfile.pye_eti_id = cat_Eti_Etiquedas.eti_id
   INNER JOIN [demo_db].[dbo].[MARCADORES_PERFILES] myp on myp.mp_id_perfil = pye_perfile.pye_per_id
   INNER JOIN [demo_db].[dbo].[CAT_MARCADORES] catMAR on catMAR.mar_id = myp.mp_id_marcador
   INNER JOIN [demo_db].[dbo].[PERFILES] per_perfiles on per_perfiles.per_id = pye_perfile.pye_per_id
   LEFT JOIN [demo_db].[dbo].[CAT_EDO_ESTADOS] catEdos on catEdos.edo_id = per_perfiles.per_edo_id
   WHERE cat_Eti_Etiquedas.eti_id IN (4) 
   and per_perfiles.per_tipo IN (4)
   order by cat_Eti_Etiquedas.eti_descripcion