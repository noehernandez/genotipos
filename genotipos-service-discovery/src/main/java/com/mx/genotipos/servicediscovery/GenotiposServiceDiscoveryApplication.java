package com.mx.genotipos.servicediscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class GenotiposServiceDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(GenotiposServiceDiscoveryApplication.class, args);
	}

}
