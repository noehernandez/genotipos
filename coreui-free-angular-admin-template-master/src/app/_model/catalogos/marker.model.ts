export interface Marker {
    id:number;
    name: string;
    type: string;
    typeId:number;
    userWhoCreated: string;
    userWhoEdited: string;
    date: string;
    
}