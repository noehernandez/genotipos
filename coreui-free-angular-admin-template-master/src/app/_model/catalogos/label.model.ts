export interface Label {
    id: number;
    name: string;
    associatedProfiles: number;
    categoryId : number;
    creationDate: string;
}