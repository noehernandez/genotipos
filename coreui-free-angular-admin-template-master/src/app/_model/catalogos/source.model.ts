export interface Source {
    
    id: number;
    name: string;
    internalId: string;
    externalId: string;
    contactName: string;
    contactEmail: string;
    contactPhone: string;
    otherContactPhone: string;
    
}

