export interface FrequencyImport {
    id: number;
    importId: string;
    name: string;
    fileName: string;
    user: string;
    date: string;
    assignedByDefault: boolean;
}