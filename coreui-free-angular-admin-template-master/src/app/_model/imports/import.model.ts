export interface Import {
    
    id: number;
    source: string ;
    importId: string ;
    importDate: string ;
    importedProfiles: number;
    user: string ;
    title: string ;
    active: boolean;
    
}