import { Label } from '../catalogos/label.model';

export interface SaveSearch{
    etiquetasObjetivo?: Label[];
    etiquetasAComparar?: Label[];
    profile?: number;
    idPerfilProgenitor?: number;
    idPerfilSeguro?: number;
    idPerfilHijo?: number;
    identificadorPerfil?: string;
    idTablaFrecuencia?: number;
    idFuente?: number;
    tablaFrecuencia?: string;
    fuente?: string;
    motivo?: string;
    exclusiones?: number;
    descripcion?: string;
    marcadoresMinimos?: number;
    descartarPerfilesRevision?: boolean;
    idUsuario?: number;
    usuario?: string;
    tipo?: string;
    numero?: string;
    fecha?: string;
    estatus?: string;
    id?: number;
    tipoBusqueda?: number;
    busquedas?: any[];
}