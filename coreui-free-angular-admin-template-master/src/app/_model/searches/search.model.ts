export interface Search{
    idPerfil:number;
    idPerfilProgenitor: number;
    idPerfilSeguro:number;
    idPerfilHijo:number;
    listaIdEntiquetasOrigen: number[];
    listaIdEntiquetasProgenitor: number[];
    numeroExclusiones: number;
    numeroMarcadores: number;
    idTablaFrecuencia: number;
    descartarPerfilesRevision: boolean;
}