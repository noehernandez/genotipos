import { Metadata } from '../profiles/metadata.model';

export interface DetalleBusqueda extends Busqueda{
    conclusiones?: string;
    marcadores?: string;
    exclusiones?: string;
    tablaFrecuencias?: string;
    descartarPerfilesRevision?: number;
    etiquetasObjetivo?: string[];
    etiquetasAComparar?: string[];
    perfilesCompatibles?: PerfilesCompatibles[];
}

export interface Busqueda{
    id?: number;
    numero?:string;
    fuente?:string;
    motivo?:string;
    descripcion?:string;
    usuario?:string;
    tipoBusqueda?:string;
    estatus?:string;
    fecha?:string;
}

export interface PerfilesCompatibles{
    id?: number;
    genotipoObjetivo?: string;
    genotipoCompatible?: string;
    perfilPadre?: string;
    perfilMadre?: string;
    perfilObjetivo?: string;
    estado?: string;
    amel?: string;
    ip?: string;
    pp?: string;
    metadatosObjetivo?: Metadata[];
    metadatosPadre?: Metadata[];
    metadatosMadre?: Metadata[];
    marcadoresCompatibles?: MarcadoresCompatibles;
}

export interface MarcadoresCompatibles{
    marcador: string;
    idMarcador: number;
    objetivo: PerfilCompatible;
    compatible: PerfilCompatible;
    perfilPadre: PerfilCompatible;
    perfilMadre: PerfilCompatible;
    aleloXOrigen: string;
    aleloYOrigen: string;
    aleloXProgenitor: string;
    aleloYProgenitor: string;
}

export interface PerfilCompatible{
    alelo1: string;
    coincideAlelo1: number;
    alelo2: string;
    coincideAlelo2: number;
}