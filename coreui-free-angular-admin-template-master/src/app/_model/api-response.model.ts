export interface APIResponse {
    process: boolean;
    identifier: number;
    message: string;
    responseObject: any;
    responseList: any;
}