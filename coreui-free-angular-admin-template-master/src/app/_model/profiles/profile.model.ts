import { Marker } from '../catalogos/marker.model';
import { MarkerProfile } from './marker-profile.model';

export interface Profile{

    id: number;
    internalId: string;
    externalId: string;
    markers: number;
    homocigotos: number;
    user: string;
    status; string
    registrationDate: string;
    markersProfile: MarkerProfile[];
}