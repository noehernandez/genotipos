export interface MarkerProfile{

    id: number;
    alele1: string;
    alele2: string;
    name: string;
}