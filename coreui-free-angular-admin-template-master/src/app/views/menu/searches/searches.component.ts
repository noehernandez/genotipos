import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, NgForm, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { LabelsService } from '../../../_services/catalogs/labels.service';
import { SourcesService } from '../../../_services/catalogs/sources.service';
import { Profile } from '../../../_model/profiles/profile.model';
import { ProfilesService } from '../../../_services/profiles/profiles.service';
import { Label } from '../../../_model/catalogos/label.model';
import { FrequencyImport } from '../../../_model/catalogos/frequency-import.model';
import { FrequenciesService } from '../../../_services/catalogs/frequencies.service';
import { Search } from '../../../_model/searches/search.model';
import { SearchesService } from '../../../_services/searches/searches.service';
import { SaveSearch } from '../../../_model/searches/save-search.model';
import { Source } from '../../../_model/catalogos/source.model';
import { Busqueda } from '../../../_model/searches/search-detail.model';

@Component({
  selector: 'app-searches',
  templateUrl: './searches.component.html'
})
export class SearchesComponent implements OnInit {

  individualSearchForm: FormGroup;
  grupalSearchForm: FormGroup;
  tripleSearchForm: FormGroup;
  isLoading: boolean=false;
  grupalIsLoading: boolean=false;
  //catalogs
  profiles: Profile[]=[];
  labels: Label[]=[];
  sources: Source[]=[];
  frequencies: FrequencyImport[]=[];

  saveSearch: SaveSearch = {};
  
  //table
  filter = new FormControl('');

  busquedas$data: Busqueda[] = [];
  busquedas$: Busqueda[] = [];
  page = 1;
  pageSize = 100;
  collectionSize = 1;
  get busquedas(): Busqueda[] {
    return this.busquedas$
      .map((busquedas$, i) => ({ id: i + 1, ...busquedas$ }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  constructor(
    private formBuilder: FormBuilder,
    private labelsService: LabelsService,
    private sourcesService: SourcesService,
    private profilesService: ProfilesService,
    private frequenciesService: FrequenciesService,
    private searchesService: SearchesService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.initializeIndividualSearchForm();
    this.initializeGrupalSearchForm();
    this.initializeTripleSearchForm();
    this.getProfiles();
    this.getLabels();
    this.getSources();
    this.getFrequencies();
    this.getBusquedas();
    this.filter.valueChanges.subscribe(value => {
      this.busquedas$ = this.busquedas$data.filter(busqueda$ => {
        return busqueda$.numero.includes(value)
          || busqueda$.fuente.includes(value)
          || busqueda$.motivo.includes(value)
          || busqueda$.descripcion.toString().includes(value)
          || busqueda$.usuario.includes(value)
          || busqueda$.tipoBusqueda.includes(value)
          || busqueda$.estatus.includes(value)
          || busqueda$.fecha.includes(value);
      });
      this.collectionSize = this.busquedas$.length;
    });
  }

  public initializeIndividualSearchForm(){
    this.individualSearchForm = this.formBuilder.group({
      profile: [null, Validators.required],
      labelsToCompare: [null],
      frequency: [null, Validators.required],
      minimumMarkers: [null],
      maximumExclusions: [null],
      source: [null, Validators.required],
      description: [null,],
      discardProfilesInReview:[null]
    });
  }
  public initializeGrupalSearchForm(){
    this.grupalSearchForm = this.formBuilder.group({
      targetLabels: [null, Validators.required],
      labelsToCompare: [null],
      frequency: [null, Validators.required],
      minimumMarkers: [null],
      maximumExclusions: [null],
      source: [null, Validators.required],
      description: [null],
      discardProfilesInReview:[null]
    });
  }
  public initializeTripleSearchForm(){
    this.tripleSearchForm = this.formBuilder.group({
      source: [null, Validators.required],
      labels: [null],
      title: [null, Validators.required],
      sample: [null],
      observations: [null],
      file: [null, Validators.required],
    });
  }

  public getBusquedas(){
    this.searchesService.getActiveSearchs(1).subscribe(data=>{
      if(data.process){
        this.busquedas$data = data.responseList;
          this.busquedas$ = this.busquedas$data
          this.collectionSize = this.busquedas$data.length;
      }
    });
  }
  public getBusquedaDetalle(busqueda:Busqueda){
    this.searchesService.setSearchId(busqueda.id);
          this.redirect('/searches/result');
  }
  public getProfiles(){
    this.profilesService.getProfileItems(1).subscribe(data =>{
      if(data.process){
        this.profiles=data.responseList;
        console.log('Perfiles ',this.profiles);
      }
    });
  }
  public getLabels(){
    this.labelsService.getActiveLabels().subscribe(data=>{
      if(data.process){
        this.labels=data.responseList;
      }
    });
  }
  public getSources(){
    this.sourcesService.getActiveSources().subscribe(data=>{
      if(data.process){
        this.sources=data.responseList;
      }
    });
  }
  public getFrequencies(){
    this.frequenciesService.getActiveFrequencies().subscribe(data=>{
      if(data.process){
        this.frequencies=data.responseList;
      }
    });
  }

  public individualSearch(){
    this.isLoading =true;
    let individualSearchRequest = this.createRequestForTheIndividualSearchService();
    console.log(individualSearchRequest);
    this.searchesService.searchByProfile(individualSearchRequest).subscribe(data=>{
      console.log(data);
      let searchResult=<any[]>data.busquedas;
      this.saveSearch = <SaveSearch>individualSearchRequest;
      this.saveSearch.busquedas=searchResult;
      this.saveSearch.etiquetasAComparar=this.individualSearchForm.value.labelsToCompare;
      console.log(this.saveSearch);
      this.searchesService.saveSearch(this.saveSearch).subscribe(data=>{
        console.log(data);
        if(data.process){
          this.showAlert();
          this.searchesService.setSearchId(data.identifier);
          this.redirect('/searches/result');
        }else{

        }
        this.isLoading =false;
        
      });
      
    });
    
  }
  public grupalSearch(){
    this.grupalIsLoading =true;
    let grupalSearchRequest = this.createRequestForTheGrupalSearchService();
    console.log(grupalSearchRequest);
    this.searchesService.searchByLabels(grupalSearchRequest).subscribe(data=>{
      console.log(data);
      let searchResult=<any[]>data.busquedas;
      this.saveSearch = <SaveSearch>grupalSearchRequest;
      this.saveSearch.busquedas=searchResult;
      this.saveSearch.etiquetasObjetivo=this.grupalSearchForm.value.targetLabels;
      this.saveSearch.etiquetasAComparar=this.grupalSearchForm.value.labelsToCompare;
      console.log(this.saveSearch);
      this.searchesService.saveSearch(this.saveSearch).subscribe(data=>{
        console.log(data);
        if(data.process){
          this.showAlert();
          this.searchesService.setSearchId(data.identifier);
          this.redirect('/searches/result');
        }else{

        }
        this.grupalIsLoading =false;
        
      });
      
    });
    
  }

  private showAlert(){
    swal.fire({
      text: 'Busqueda Exitosa',
      showCancelButton: false,
      confirmButtonClass: 'btn btn-success',
      confirmButtonText: 'aceptar',
      buttonsStyling: false

    });
  }

  public redirect(path: string) {
    this.router.navigate([path]);
  }

  public createRequestForTheIndividualSearchService():Search{
    console.log(this.individualSearchForm.value);
    let labelsToCompare : number[]=[];
    let labelsSelected=<Label[]>this.individualSearchForm.value.labelsToCompare;
    labelsSelected.forEach(label=>{ labelsToCompare.push(label.id)})
    // console.log('etiquetas',labelsToCompare);
    let searchRequest = {
      idPerfil: this.individualSearchForm.value.profile.id,
      idPerfilProgenitor: null,
      idPerfilSeguro:null,
      idPerfilHijo:null,
      listaIdEntiquetasOrigen: null,
      listaIdEntiquetasProgenitor: labelsToCompare,
      numeroExclusiones: this.individualSearchForm.value.maximumExclusions,
      numeroMarcadores: this.individualSearchForm.value.minimumMarkers,
      idTablaFrecuencia: this.individualSearchForm.value.frequency.id,
      idFuente: this.individualSearchForm.value.source.id,
      descartarPerfilesRevision: this.individualSearchForm.value.discardProfilesInReview,
      tipo:'INDIVIDUAL',
      descripcion : this.individualSearchForm.value.description,
    };
    return searchRequest;
  }

  public createRequestForTheGrupalSearchService():Search{
    console.log(this.individualSearchForm.value);
    let labelsToCompare : number[]=[];
    let labelsSelected=<Label[]>this.grupalSearchForm.value.labelsToCompare;
    labelsSelected.forEach(label=>{ labelsToCompare.push(label.id)})
    let targetLabels : number[]=[];
    let targetLabelsSelected=<Label[]>this.grupalSearchForm.value.labelsToCompare;
    targetLabelsSelected.forEach(label=>{ targetLabels.push(label.id)})
    // console.log('etiquetas',labelsToCompare);
    let searchRequest = {
      idPerfil: null,
      idPerfilProgenitor: null,
      idPerfilSeguro:null,
      idPerfilHijo:null,
      listaIdEntiquetasOrigen: targetLabels,
      listaIdEntiquetasProgenitor: labelsToCompare,
      numeroExclusiones: this.grupalSearchForm.value.maximumExclusions,
      numeroMarcadores: this.grupalSearchForm.value.minimumMarkers,
      idTablaFrecuencia: this.grupalSearchForm.value.frequency.id,
      idFuente: this.grupalSearchForm.value.source.id,
      descartarPerfilesRevision: this.grupalSearchForm.value.discardProfilesInReview,
      tipo:'GRUPAL',
      descripcion : this.grupalSearchForm.value.description,
    };
    return searchRequest;
  }
}
