import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchesComponent} from './searches.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { CompatibleResultComponent } from './compatible-result/compatible-result.component';


const routes: Routes = [
  {
    path: '',
    component: SearchesComponent,
    data: {
      title: 'Busquedas'
    }
  },
  {
    path: 'result',
    component: SearchResultComponent,
    data: {
      title: 'Resultado de la busqueda'
    }
  },
  {
    path: 'compatible-result',
    component: CompatibleResultComponent,
    data: {
      title: 'Resultado compatible'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchesRoutingModule { }
