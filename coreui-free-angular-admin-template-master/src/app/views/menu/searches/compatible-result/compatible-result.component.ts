import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchesService } from '../../../../_services/searches/searches.service';
import { PerfilesCompatibles } from '../../../../_model/searches/search-detail.model';

@Component({
  selector: 'app-compatible-result',
  templateUrl: './compatible-result.component.html',
  styleUrls: []
})
export class CompatibleResultComponent implements OnInit {

  perfilesCompatibles: PerfilesCompatibles={}

  constructor(
    private searchesService: SearchesService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getCompatibleResult();
  }

  public getCompatibleResult(){
    let compatibleResultId = this.searchesService.getCompatibleResultId();
    this.searchesService.getCompatibleMarkers(compatibleResultId).subscribe(data=>{
      if(data.process){
        this.perfilesCompatibles = data.responseObject;
        console.log(this.perfilesCompatibles);
      }
    });
  }

  public redirect(path: string) {
    this.router.navigate([path]);
  }
}
