import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterModule } from '../../master/master.module';
// routing
import { SearchesRoutingModule} from './searches-routing.module';

// component
import { SearchesComponent } from './searches.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { CompatibleResultComponent } from './compatible-result/compatible-result.component';


@NgModule({
  declarations: [SearchesComponent, SearchResultComponent, CompatibleResultComponent],
  imports: [
    CommonModule,
    SearchesRoutingModule,
    MasterModule,
  ]
})
export class SearchesModule { }
