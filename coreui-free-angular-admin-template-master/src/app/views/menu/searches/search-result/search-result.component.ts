import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchesService } from '../../../../_services/searches/searches.service';
import { DetalleBusqueda, Busqueda, PerfilesCompatibles } from '../../../../_model/searches/search-detail.model';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: []
})
export class SearchResultComponent implements OnInit {

  //form control
  filter = new FormControl('');

  perfilesCompatibles$data: PerfilesCompatibles[] = [];
  perfilesCompatibles$: PerfilesCompatibles[] = [];
  page = 1;
  pageSize = 100;
  collectionSize = 1;
  get perfilesCompatibles(): PerfilesCompatibles[] {
    return this.perfilesCompatibles$
      .map((perfilesCompatibles$, i) => ({ id: i + 1, ...perfilesCompatibles$ }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  searchDetail: DetalleBusqueda = {};

  constructor(
    private searchesService: SearchesService,
    private router: Router,
  ) { }


  ngOnInit() {
    this.getSearchResult();

    this.filter.valueChanges.subscribe(value => {
      this.perfilesCompatibles$ = this.perfilesCompatibles$data.filter(perfilCompatible$ => {
        return perfilCompatible$.genotipoObjetivo.includes(value)
          || perfilCompatible$.genotipoCompatible.includes(value)
          || perfilCompatible$.estado.includes(value)
          || perfilCompatible$.amel.toString().includes(value)
          || perfilCompatible$.pp.includes(value)
          || perfilCompatible$.ip.includes(value);
      });
      this.collectionSize = this.perfilesCompatibles$.length;
    });
  }

  getSearchResult() {
    let idSearch=this.searchesService.getSearchId();
    // let idSearch = 39;
    if (idSearch != null) {
      this.searchesService.getSearchResult(idSearch).subscribe(data => {
        if (data.process) {
          this.searchDetail = data.responseObject;
          // console.log('searchDetail -> ', this.searchDetail);
          this.perfilesCompatibles$data = this.searchDetail.perfilesCompatibles;
          this.perfilesCompatibles$ = this.perfilesCompatibles$data
          this.collectionSize = this.perfilesCompatibles$data.length;
        }

      });
    }
  }

  showCompatibleResult(perfilCompatible: PerfilesCompatibles){
    // console.log(perfilCompatible);
    this.searchesService.setCompatibleResultId(perfilCompatible.id);
    this.redirect('/searches/compatible-result')
  }

  public redirect(path: string) {
    this.router.navigate([path]);
  }

}
