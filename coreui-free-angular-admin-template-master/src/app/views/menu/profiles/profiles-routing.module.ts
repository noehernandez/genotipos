import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfilesComponent} from './profiles.component';
import { ProfilesAddComponent } from './profiles-add/profiles-add.component';
import { ProfilesDuplicatesComponent } from './profiles-duplicates/profiles-duplicates.component';
import { ProfilesRevisionComponent } from './profiles-revision/profiles-revision.component';
import { ProfilesDimissedComponent } from './profiles-dimissed/profiles-dimissed.component';
import { ProfileDetailComponent } from './profile-detail/profile-detail.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Perfiles'
    },
    children: [
      {
        path: 'search',
        component: ProfilesComponent,
      },
      {
        path: 'add',
        component: ProfilesAddComponent,
        data: {
          title: 'Agregar'
        },
      },
      {
        path: 'duplicates',
        component: ProfilesDuplicatesComponent,
      },
      {
        path: 'revision',
        component: ProfilesRevisionComponent,
      },
      {
        path: 'dimissed',
        component: ProfilesDimissedComponent,
      },
      {
        path: 'detail',
        component: ProfileDetailComponent,
      },
      {
        path: '**',
        redirectTo: 'search'
        
      },]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfilesRoutingModule { }
