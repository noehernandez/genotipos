import { Component, OnInit } from '@angular/core';
import { ProfilesService } from '../../../../_services/profiles/profiles.service';
import { FormBuilder } from '@angular/forms';
import { Profile } from '../../../../_model/profiles/profile.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-profile-detail',
  templateUrl: './profile-detail.component.html',
  styleUrls: []
})
export class ProfileDetailComponent implements OnInit {

  profile: Profile;

  constructor(
    private profilesService: ProfilesService,
    private router: Router,
    // private formBuilder: FormBuilder,
    // private router: Router,
  ) { }

  ngOnInit() {
    this.profile = this.profilesService.getProfile();
    console.log(this.profile);
    this.getProfileDetail();
  }

  public getProfileDetail(){
    this.profilesService.getProfileDetail(this.profile.id).subscribe(data=>{
      // console.log(data);
      if(data.process){
        this.profile = data.responseObject;
        console.log('perfie ... ',this.profile);
        
      }
    });
  }
  public redirect(path: string) {
    this.router.navigate([path]);
  }

}
