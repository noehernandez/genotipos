import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// routing
import { ProfilesRoutingModule} from './profiles-routing.module';

// component
import { ProfilesComponent } from './profiles.component';
import { ProfilesAddComponent } from './profiles-add/profiles-add.component';
import { ProfilesDuplicatesComponent } from './profiles-duplicates/profiles-duplicates.component';
import { ProfilesRevisionComponent } from './profiles-revision/profiles-revision.component';
import { ProfilesDimissedComponent } from './profiles-dimissed/profiles-dimissed.component';
import { ProfileDetailComponent } from './profile-detail/profile-detail.component';


@NgModule({
  declarations: [ProfilesComponent, ProfilesAddComponent, ProfilesDuplicatesComponent, ProfilesRevisionComponent, ProfilesDimissedComponent, ProfileDetailComponent],
  imports: [
    CommonModule,
    ProfilesRoutingModule,
  ]
})
export class ProfilesModule { }
