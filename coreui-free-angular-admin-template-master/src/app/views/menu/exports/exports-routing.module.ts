import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExportsComponent } from './exports.component';


const routes: Routes = [
  {
    path: '',
    component: ExportsComponent,
    data: {
      title: 'Exportaciones'
    }
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExportsRoutingModule { }
