import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExportsComponent } from './exports.component';

// routing
import { ExportsRoutingModule } from './exports-routing.module';


@NgModule({
  declarations: [ExportsComponent],
  imports: [
    CommonModule,
    ExportsRoutingModule,
  ]
})
export class ExportsModule { }
