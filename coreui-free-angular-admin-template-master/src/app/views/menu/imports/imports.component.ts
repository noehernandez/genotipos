import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, NgForm, Validators, FormGroup } from '@angular/forms';
import {ModalDirective} from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

// services
import { ImportsService } from '../../../_services/imports/imports.service';
import { Import } from '../../../_model/imports/import.model';
// import {ModalDirective} from 'ngx-bootstrap/modal';
import swal from 'sweetalert2';
import { Label } from '../../../_model/catalogos/label.model';
import { LabelsService } from '../../../_services/catalogs/labels.service';
import { Source } from '../../../_model/catalogos/source.model';
import { SourcesService } from '../../../_services/catalogs/sources.service';

@Component({
  selector: 'app-imports',
  templateUrl: './imports.component.html'
})
export class ImportsComponent implements OnInit {

  @ViewChild('importProfileModal', {static: false}) public importProfileModal: ModalDirective;

  // declare forms control
  filter = new FormControl('');
  isLoading: boolean;

  imports$data: Import[]=[];
  imports$: Import[]=[];
  page = 1;
  pageSize = 100;
  collectionSize = 1;
  get imports(): Import[] {
    return this.imports$
      .map((import$, i) => ({id: i + 1, ...import$}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  //form
  archivoSelected: File;
  importForm: FormGroup;
  labels: Label[]=[];
  sources: Source[]=[];
  

  constructor(
    private importsService: ImportsService,
    private labelsService: LabelsService,
    private sourcesService: SourcesService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getAllImports();
    this.getLabels();
    this.getSources();
    this.isLoading=false;
    //add event to filter
    this.filter.valueChanges.subscribe( value =>{
      this.imports$ = this.imports$data.filter(import$ => {
        return import$.importId.includes(value) 
        || import$.source.includes(value) 
        || import$.importDate.includes(value)
        || import$.importedProfiles.toString().includes(value)
        || import$.user.includes(value)
        || import$.title.includes(value);
      });
      this.collectionSize = this.imports$.length;
    });

    //initialize importForm
    this.importForm = this.formBuilder.group({
      source: [null, Validators.required],
      labels: [null],
      title: [null, Validators.required],
      sample: [null],
      observations: [null],
      file: [null, Validators.required],
    });
  }

  getLabels(){
    this.labelsService.getActiveLabels().subscribe(data => {
      if(data.process){
        this.labels=data.responseList;
      }
    });
  }
  getSources(){
    this.sourcesService.getActiveSources().subscribe(data => {
      if(data.process){
        this.sources = data.responseList;
      }
    })
  }

  // methods
  public getAllImports(){
    this.importsService.getAllImports().subscribe(data =>{
      console.log(data);
      this.imports$data=data.responseList;
      this.imports$=this.imports$data
      this.collectionSize = this.imports$data.length;
    });
  }

  public onFileSelect(files: FileList) {
    this.archivoSelected = files[0];
  }

  public restartImportForm(){
    console.log("reestableciendo fform ..");
    this.importForm = this.formBuilder.group({
      source: [null, Validators.required],
      labels: [null],
      title: [null, Validators.required],
      sample: [null],
      observations: [null],
      file: [null, Validators.required],
    });
  }

  public startImport(){
    this.isLoading=true;
    console.log("iniciado importacion ...");
    let importProfiles = <Import>this.importForm.value;
    console.log("valores formulario ...",importProfiles);
    let importstr: string = JSON.stringify(importProfiles);

    this.importsService.importProfiles(this.archivoSelected,importstr).subscribe(data  =>{
      console.log(data);
      if(data.process){
        console.log("Exitosa importacion ...");
        
        swal.fire({
          text: 'Se ha realizado la carga con exito',
          showCancelButton: false,
          confirmButtonClass: 'btn btn-success',
          confirmButtonText: 'aceptar',
          buttonsStyling: false

        });
        this.importProfileModal.hide();
        this.getAllImports();
      }else{
        swal.fire({
          text: 'Error al realizar la importacion',
          showCancelButton: false,
          confirmButtonClass: 'btn btn-danger',
          confirmButtonText: 'aceptar',
          buttonsStyling: false

        });
        this.isLoading=false;
      }
    });
  }
  public showProfilesImport(import$:Import){
    this.importsService.setImport(import$);
    this.redirect('/imports/profiles');
  }

  public redirect(path: string) {
    this.router.navigate([path]);
  }
}
