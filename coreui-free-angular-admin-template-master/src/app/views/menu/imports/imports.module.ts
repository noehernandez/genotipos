import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterModule } from '../../master/master.module';
import { ModalModule } from 'ngx-bootstrap/modal';
// import { ModalsComponent } from '../../notifications/modals.component';

// /components
import { ImportsComponent } from './imports.component';

// routing
import { ImportsRoutingModule } from './imports-routing.module';
import { ProfilesImportComponent } from './profiles-import.component';

@NgModule({
  declarations: [
    ImportsComponent,
    ProfilesImportComponent,
    // ModalsComponent,
  ],
  imports: [
    CommonModule,
    ImportsRoutingModule,
    MasterModule,
    ModalModule.forRoot(),
  ]
})
export class ImportsModule { }
