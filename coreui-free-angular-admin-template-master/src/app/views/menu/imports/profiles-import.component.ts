import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, NgForm, Validators, FormGroup } from '@angular/forms';
import {ModalDirective} from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

// services
import { ImportsService } from '../../../_services/imports/imports.service';
import { Import } from '../../../_model/imports/import.model';
// import {ModalDirective} from 'ngx-bootstrap/modal';
import swal from 'sweetalert2';
import { ProfilesService } from '../../../_services/profiles/profiles.service';
import { Profile } from '../../../_model/profiles/profile.model';

@Component({
  selector: 'app-profiles-import',
  templateUrl: './profiles-import.component.html',
  styleUrls: []
})
export class ProfilesImportComponent implements OnInit {

  // declare forms control
  filter = new FormControl('');

  profiles$data: Profile[]=[];
  profiles$: Profile[]=[];
  page = 1;
  pageSize = 50;
  collectionSize = 1;
  get profiles(): Profile[] {
    return this.profiles$
      .map((profile$, i) => ({id: i + 1, ...profile$}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  import$: Import;
  // profiles: Profile[]=[];
  constructor(
    private importsService: ImportsService,
    private profilesService: ProfilesService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) { }

  ngOnInit() {
    this.import$=this.importsService.getImport();
    console.log("importacion ...",this.import$);
    this.getImportedProfiles(this.import$);

    this.filter.valueChanges.subscribe( value =>{
      this.profiles$ = this.profiles$data.filter(profile => {
        return ('').concat(profile.internalId).includes(value) 
        || profile.externalId.concat('').includes(value)
        || profile.markers.toString().includes(value)
        || profile.homocigotos.toString().includes(value)
        || profile.user.toString().includes(value)
        || profile.status.toString().includes(value)
        || profile.registrationDate.toString().includes(value);
      });
      this.collectionSize = this.profiles$.length;
    });
  }

  public getImportedProfiles(import$: Import){
    this.profilesService.getImportedProfiles(import$.id).subscribe(data=>{
      if(data.process){
        this.profiles$data=data.responseList;
        this.profiles$=this.profiles$data
        this.collectionSize = this.profiles$data.length;
        console.log(this.profiles$data);
        
      }
      
    });
  }

  public viewProfileDetail(profile: Profile){
    console.log('perfil selecte .. ',profile.id);
    this.profilesService.setProfile(profile);
    this.redirect('/profiles/detail');
  }

  public redirect(path: string) {
    this.router.navigate([path]);
  }

}
