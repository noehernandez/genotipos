import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImportsComponent} from './imports.component';
import { ProfilesImportComponent } from './profiles-import.component';


const routes: Routes = [
  {
    path: '',
    component: ImportsComponent,
    data: {
      title: 'Importaciones'
    }
  },
  {
    path: 'profiles',
    component: ProfilesImportComponent,
    data: {
      title: 'Perfiles-Importados'
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImportsRoutingModule { }
