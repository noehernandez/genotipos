import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormBuilder, NgForm, Validators, FormGroup } from '@angular/forms';
import {ModalDirective} from 'ngx-bootstrap/modal';
// services
import { CategoriesService } from '../../../../_services/catalogs/categories.service';
import { Category } from '../../../../_model/catalogos/category.model';
import swal from 'sweetalert2';
import { LabelsService } from '../../../../_services/catalogs/labels.service';
import { Label } from '../../../../_model/catalogos/label.model';
// import {ModalDirective} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
})
export class CategoriesComponent  implements OnInit {

  @ViewChild('addCategoryModal', {static: false}) public addCategoryModal: ModalDirective;
  @ViewChild('editCategoryModal', {static: false}) public editCategoryModal: ModalDirective;
  @ViewChild('addLabelModal', {static: false}) public addLabelModal: ModalDirective;
  @ViewChild('deleteCategoryModal', {static: false}) public deleteCategoryModal: ModalDirective;

  // declare forms filter
  filter = new FormControl('');
  categories$data: Category[]=[];
  categories$: Category[]=[];
  page = 1;
  pageSize = 100;
  collectionSize = 1;
  get categories(): Category[] {
    return this.categories$
      .map((category, i) => ({id: i + 1, ...category}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }
  
  //forms 
  categoriesForm: FormGroup;
  selectedCategoryForm: FormGroup;
  labelsForm: FormGroup;
  // declare variables
  selectedCategory: Category = {} as Category;
  labelsCategory: Label[]=[];
  constructor(
    private categoriesService: CategoriesService,
    private labelsService: LabelsService,
    private formBuilder: FormBuilder,
  ) {   }

  ngOnInit() {
    this.getActiveCategories();

    this.filter.valueChanges.subscribe( value =>{
      this.categories$ = this.categories$data.filter(categoria => {
        return categoria.id.toString().includes(value) 
        || categoria.name.includes(value)
        || categoria.associatedLabels.toString().includes(value);
      });
      this.collectionSize = this.categories$.length;
    });

    //initialize categoriesForm
    this.restartCategoryForm();
    this.restartSelectedCategoryForm();
    //initialize labelsForm
    this.restartLabelForm();
  }

  public restartCategoryForm(){
    console.log("reestableciendo fform categorias..");
    this.categoriesForm = this.formBuilder.group({
      name: [null, Validators.required],
    });
  }
  public restartSelectedCategoryForm(){
    console.log("reestableciendo fform categorias..");
    this.selectedCategoryForm = this.formBuilder.group({
      id:[this.selectedCategory.id],
      name: [this.selectedCategory.name, Validators.required],
    });
  }
  public restartLabelForm(){
    console.log("reestableciendo fform categorias..");
    this.labelsForm = this.formBuilder.group({
      name: [null, Validators.required],
      categoryId: [null],
    });
  }
  // methods category
  public getActiveCategories(){
    this.categoriesService.getActiveCategories().subscribe(data =>{
      console.log(data);
      this.categories$data=data.responseList;
      this.categories$=this.categories$data
      this.collectionSize = this.categories$data.length;
    });
  }

  public addCategory(){
    let category= <Category> this.categoriesForm.value;
    console.log('añadiendo categoria ..',category);
    this.categoriesService.addCategory(category).subscribe(data =>{
      if(data.process){
        swal.fire({
          text: 'Categoria guardada exitosamente!!',
          showCancelButton: false,
          confirmButtonClass: 'btn btn-success',
          confirmButtonText: 'aceptar',
          buttonsStyling: false
        });
        this.addCategoryModal.hide();
        this.getActiveCategories();
      }else{
        swal.fire({
          text: 'Error al guardar la categoria',
          showCancelButton: false,
          confirmButtonClass: 'btn btn-danger',
          confirmButtonText: 'aceptar',
          buttonsStyling: false

        });
      }
    });
  }

  public deleteCategory(){
    if(this.selectedCategory != null){
      this.categoriesService.deleteCategory(this.selectedCategory).subscribe(data=>{
        if(data.process){
          console.log("eliminacion exitosa ..");
          this.getActiveCategories();
          this.deleteCategoryModal.hide();
        }
      });
    }
  }

  public updateCategory(){
    let category=this.selectedCategoryForm.value;
    console.log(category);
    
    if(this.selectedCategory != null){
      this.categoriesService.updateCategory(category).subscribe(data=>{
        if(data.process){
          console.log("actualizacion exitosa ..");
          this.getActiveCategories();
          this.editCategoryModal.hide();
        }
      });
    }
  }

  public setSelectedCategory(category: Category){
    this.selectedCategory = category;
  }

  // methods label
  public addLabels(){
    let label=<Label>this.labelsForm.value;
    console.log('agregando etiquetas ...',label);
    this.labelsService.addLabel(label).subscribe(data =>{
      if(data.process){
        swal.fire({
          text: 'Etiqueta guardada exitosamente!!',
          showCancelButton: false,
          confirmButtonClass: 'btn btn-success',
          confirmButtonText: 'aceptar',
          buttonsStyling: false
        });
        this.addLabelModal.hide();
        this.getActiveCategories();
      }else{
        swal.fire({
          text: 'Error al guardar la etiqueta',
          showCancelButton: false,
          confirmButtonClass: 'btn btn-danger',
          confirmButtonText: 'aceptar',
          buttonsStyling: false

        });
      }
    });
  }

  findByCategory(category: Category){
    this.labelsService.getLabelsByCategory(category.id).subscribe(data=>{
      if(data.process){
        this.labelsCategory = data.responseList;
      }
    });
  }

}
