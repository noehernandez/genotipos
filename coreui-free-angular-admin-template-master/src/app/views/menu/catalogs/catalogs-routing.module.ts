import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogsComponent } from './catalogs.component';
import { LabelsComponent } from './labels/labels.component';
import { CategoriesComponent } from './categories/categories.component';
import { SourcesComponent } from './sources/sources.component';
import { MarkersComponent } from './markers/markers.component';
import { FrequenciesComponent } from './frequencies/frequencies.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Catalogos'
    },
    children: [
      {
        path: 'categories',
        component: CategoriesComponent,
      },
      {
        path: 'frequencies',
        component: FrequenciesComponent,
        data: {
          title: 'Agregar'
        },
      },
      {
        path: 'markers',
        component: MarkersComponent,
      },
      {
        path: 'sources',
        component: SourcesComponent,
      },
      {
        path: 'labels',
        component: LabelsComponent,
      },
      {
        path: '**',
        redirectTo: 'search'
        
      },]
  }];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogsRoutingModule { }
