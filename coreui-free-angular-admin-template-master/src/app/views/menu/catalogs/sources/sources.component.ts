import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, NgForm, Validators, FormGroup } from '@angular/forms';
import {ModalDirective} from 'ngx-bootstrap/modal';

import { SourcesService } from '../../../../_services/catalogs/sources.service';
import { Source } from '../../../../_model/catalogos/source.model';

import swal from 'sweetalert2';

@Component({
  selector: 'app-sources',
  templateUrl: './sources.component.html',
})
export class SourcesComponent implements OnInit {

  @ViewChild('addSourceModal', {static: false}) public addSourceModal: ModalDirective;
  @ViewChild('editSourceModal', {static: false}) public editSourceModal: ModalDirective;
  @ViewChild('deleteSourceModal', {static: false}) public deleteSourceModal: ModalDirective;

  // declare forms control
  filter = new FormControl('');
  sources$data: Source[] = [];
  sources$: Source[] = [];
  page = 1;
  pageSize = 100;
  collectionSize = 1;
  get sources(): Source[] {
    return this.sources$
      .map((source, i) => ({ id: i + 1, ...source }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  //forms
  sourceForm: FormGroup;
  detailSourceForm: FormGroup;
  //variables
  selectedSource: Source = {} as Source;
  detailSource: Source ={} as Source;
  constructor(
    private sourcesService: SourcesService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.getActiveSources();
    this.restartDetailSourceForm();
    this.filter.valueChanges.subscribe(value => {
      this.sources$ = this.sources$data.filter(source => {
        return source.name.includes(value)
          || source.internalId.includes(value)
          || source.externalId.includes(value)
          || source.contactPhone.includes(value)
          || source.contactName.includes(value)
          || source.contactEmail.includes(value)
      });
      this.collectionSize = this.sources$.length;
    });
    this.restartSourceForm();
  }
  public restartSelectedSource(){
    this.selectedSource = {} as Source;
  }
  public restartSourceForm() {
    console.log("reestableciendo fform categorias..");
    this.sourceForm = this.formBuilder.group({
      id: [this.selectedSource.id],
      name: [this.selectedSource.name, Validators.required],
      internalId: [this.selectedSource.internalId, Validators.required],
      externalId: [this.selectedSource.externalId],
      contactName: [this.selectedSource.contactName],
      contactEmail: [this.selectedSource.contactEmail],
      contactPhone: [this.selectedSource.contactPhone],
      otherContactPhone: [this.selectedSource.otherContactPhone],
    });
  }
  public restartDetailSourceForm() {
    console.log("reestableciendo fform categorias..");
    this.detailSourceForm = this.formBuilder.group({
      id: [this.detailSource.id],
      name: [this.detailSource.name, Validators.required],
      internalId: [this.detailSource.internalId, Validators.required],
      externalId: [this.detailSource.externalId],
      contactName: [this.detailSource.contactName],
      contactEmail: [this.detailSource.contactEmail],
      contactPhone: [this.detailSource.contactPhone],
      otherContactPhone: [this.detailSource.otherContactPhone],
    });
  }

  // methods
  public getActiveSources() {
    this.sourcesService.getActiveSources().subscribe(data => {
      console.log(data);
      this.sources$data = data.responseList;
      this.sources$ = this.sources$data
      this.collectionSize = this.sources$data.length;
    });
  }

  public addSource() {
    let source = <Source> this.sourceForm.value;
    console.log("adding source ....",source);
    this.sourcesService.addSource(source).subscribe(data=>{
      if(data.process){
        swal.fire({
          text: 'Fuente guardada exitosamente!!',
          showCancelButton: false,
          confirmButtonClass: 'btn btn-success',
          confirmButtonText: 'aceptar',
          buttonsStyling: false
        });
        this.addSourceModal.hide();
        this.getActiveSources();
      }else{
        swal.fire({
          text: 'Error al guardar la fuente',
          showCancelButton: false,
          confirmButtonClass: 'btn btn-danger',
          confirmButtonText: 'aceptar',
          buttonsStyling: false

        });
      }
    });
  }
  public deleteSource(){
    if(this.selectedSource != null){
      console.log(this.selectedSource);
      this.sourcesService.deleteSource(this.selectedSource).subscribe(data=>{
        if(data.process){
          this.getActiveSources();
        }
        this.deleteSourceModal.hide();
      });
    }
  }
  public updateSource(){
    let source = <Source>this.sourceForm.value;
    console.log('source a editar ...',source);
    this.sourcesService.editSource(source).subscribe(data=>{
      if(data.process){
        this.getActiveSources();
        this.editSourceModal.hide();
      }
    });
    
  }
  public setSelectedSource(source: Source){
    this.selectedSource=source;
  }
  public setDetailSource(source: Source){
    this.detailSource=source;
    this.restartDetailSourceForm();
  }

}
