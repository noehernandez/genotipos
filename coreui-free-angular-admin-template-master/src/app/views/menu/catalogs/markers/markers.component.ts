import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import {ModalDirective} from 'ngx-bootstrap/modal';

// service
import { MarkersService } from '../../../../_services/catalogs/markers.service';
import { Marker } from '../../../../_model/catalogos/marker.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-markers',
  templateUrl: './markers.component.html',
})
export class MarkersComponent implements OnInit {

  @ViewChild('addMarkerModal', { static: false }) public addMarkerModal: ModalDirective;
  @ViewChild('editMarkerModal', { static: false }) public editMarkerModal: ModalDirective;
  @ViewChild('deleteMarkerModal', { static: false }) public deleteMarkerModal: ModalDirective;


  // declare forms control
  filter = new FormControl('');
  markers$data: Marker[] = [];
  markers$: Marker[] = [];
  page = 1;
  pageSize = 100;
  collectionSize = 1;
  get markers(): Marker[] {
    return this.markers$
      .map((marker, i) => ({ id: i + 1, ...marker }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  constructor(
    private markersService: MarkersService,
    private formBuilder: FormBuilder,
  ) { }

  // Agrego forms
markerForm: FormGroup;
//Agrego variables
 selectedMarker: Marker = {} as Marker;

  ngOnInit() {
    this.getActiveMarkers();

    this.filter.valueChanges.subscribe(value => {
      console.log("filtrando ", value);

      this.markers$ = this.markers$data.filter(marker => {
        return marker.name.includes(value)
          || marker.type.includes(value)
          || marker.userWhoCreated.includes(value)
          || marker.date.includes(value);
      });
      this.collectionSize = this.markers$.length;
    });
    this.restartMarkerForm();
  }
  public restartMarkerForm() {
    //Se Modifico
    console.log("reestableciendo Catcategorias..........");
    this.markerForm = this.formBuilder.group({
      id: [this.selectedMarker.id],
      name: [this.selectedMarker.name, Validators.required],
      type: [this.selectedMarker.type, Validators.required],
      typeId: [this.selectedMarker.typeId, Validators.required],
    //  userWhoCreated: [this.selectedMarker.userWhoCreated],
     // userWhoEdited: [this.selectedMarker.userWhoEdited],
        
           // contactEmail: [this.selectedSource.contactEmail],
      // contactPhone: [this.selectedSource.contactPhone],
      // otherContactPhone: [this.selectedSource.otherContactPhone],
    });
  }

  // methods
  getActiveMarkers() {
    this.markersService.getActiveMarkers().subscribe(data => {
      console.log(data);
      this.markers$data = data.responseList;
      this.markers$ = this.markers$data
      this.collectionSize = this.markers$data.length;
    });
  }
   //Se agrego 
   public addMarker() {
    let marker = <Marker> this.markerForm.value;
    console.log(this.markerForm.value);
    console.log("adding Marke ....",marker);
    this.markersService.addMarker(marker).subscribe(data=>{
      if(data.process){
        Swal.fire({
          text: 'Marcador guardado exitosamente!!',
          showCancelButton: false,
          confirmButtonClass: 'btn btn-success',
          confirmButtonText: 'aceptar',
          buttonsStyling: false
        });
        this.addMarkerModal.hide();
        this.getActiveMarkers();
      }else{
        Swal.fire({
          text: 'Error al guardar el marcador',
          showCancelButton: false,
          confirmButtonClass: 'btn btn-danger',
          confirmButtonText: 'aceptar',
          buttonsStyling: false

        });
      }
    });
  }

  //Se agrego
  public deleteMarker(){
    if(this.selectedMarker != null){
      console.log(this.selectedMarker);
      this.markersService.deleteMarker(this.selectedMarker).subscribe(data=>{
        if(data.process){
          this.getActiveMarkers();
        }
        this.deleteMarkerModal.hide();
      });
    }
  }

  //Se Agrego
  public updateMarker(){
    let marker= this.markerForm.value;
    console.log(marker);
    
    if(this.setSelectedMarker != null){
      this.markersService.editMarker(marker).subscribe(data=>{
        if(data.process){
          console.log("actualizacion exitosa ..");
          this.getActiveMarkers();
          this.editMarkerModal.hide();
        }
      });
    }
    
  }

  // Se agrego
  public setSelectedMarker(marker: Marker){
    this.selectedMarker=marker;
  }
}
