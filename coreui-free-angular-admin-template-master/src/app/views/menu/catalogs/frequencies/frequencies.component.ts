import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, NgForm, Validators, FormGroup } from '@angular/forms';
import {ModalDirective} from 'ngx-bootstrap/modal';
// services
import { FrequenciesService } from '../../../../_services/catalogs/frequencies.service';
import { FrequencyImport } from '../../../../_model/catalogos/frequency-import.model';

import swal from 'sweetalert2';
@Component({
  selector: 'app-frequencies',
  templateUrl: './frequencies.component.html',
})
export class FrequenciesComponent implements OnInit {

  @ViewChild('addFrequencyModal', {static: false}) public addFrequencyModal: ModalDirective;
  @ViewChild('deleteFrequencyModal', {static: false}) public deleteFrequencyModal: ModalDirective;
  @ViewChild('defaultFrequencyModal', {static: false}) public defaultFrequencyModal: ModalDirective;

  // declare forms control
  filter = new FormControl('');
  frequencies$data: FrequencyImport[]=[];
  frequencies$: FrequencyImport[]=[];
  page = 1;
  pageSize = 100;
  collectionSize = 1;
  get frequencies(): FrequencyImport[] {
    return this.frequencies$
      .map((category, i) => ({id: i + 1, ...category}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  //forms
  selectedFile: File;
  importForm: FormGroup;
  //variables
  selectedFrequency: FrequencyImport = {} as FrequencyImport;

  constructor(
    private frequenciesService: FrequenciesService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.getActiveFrequencies();

    this.filter.valueChanges.subscribe( value =>{
      this.frequencies$ = this.frequencies$data.filter(frequency => {
        return frequency.importId.includes(value) 
        || frequency.name.includes(value)
        || frequency.fileName.includes(value)
        || frequency.user.includes(value)
        || frequency.date.includes(value)
      });
      this.collectionSize = this.frequencies$.length;
    });
    this.restartFrequencyForm();
  }
  public restartFrequencyForm(){
    console.log("reestableciendo fform ..");
    this.importForm = this.formBuilder.group({
      name: [null, Validators.required],
      file: [null, Validators.required],
    });
  }

  // methods
  public onFileSelect(files: FileList) {
    this.selectedFile = files[0];
  }

  getActiveFrequencies(){
    this.frequenciesService.getActiveFrequencies().subscribe(data =>{
      console.log(data);
      this.frequencies$data=data.responseList;
      this.frequencies$=this.frequencies$data
      this.collectionSize = this.frequencies$data.length;
    });
  }

  public startImport(){
    let importFrequency = <FrequencyImport> this.importForm.value;
    console.log("starting import ...",importFrequency);
    this.frequenciesService.importFrequencyTable(this.selectedFile,importFrequency.name).subscribe(data=>{
      if(data.process){
        swal.fire({
          text: 'Tabla de frecuencias guardada exitosamente!!',
          showCancelButton: false,
          confirmButtonClass: 'btn btn-success',
          confirmButtonText: 'aceptar',
          buttonsStyling: false
        });
        this.addFrequencyModal.hide();
        this.getActiveFrequencies();
      }else{
        swal.fire({
          text: 'Error al guardar la tabla de frecuencias',
          showCancelButton: false,
          confirmButtonClass: 'btn btn-danger',
          confirmButtonText: 'aceptar',
          buttonsStyling: false

        });
      }
    });

  }

  public setSelectedFrequency(frequency: FrequencyImport){
    this.selectedFrequency = frequency;
  }
  public assignByDefault(){
    console.log('tabla de frecuencias : ',this.selectedFrequency.id);
    this.frequenciesService.assignByDefault(this.selectedFrequency).subscribe(data=>{
      if(data.process){
        this.getActiveFrequencies();
        this.defaultFrequencyModal.hide();
      }
    });
  }
  public deleteFrequency(){
    console.log('tabla de frecuencias : ',this.selectedFrequency.id);
    this.frequenciesService.deleteFrequency(this.selectedFrequency).subscribe(data=>{
      if(data.process){
        this.getActiveFrequencies();
        this.deleteFrequencyModal.hide();
      }
    });
  }
  public getFrequencyFile(frequency: FrequencyImport){
    console.log('tabla de frecuencias : ',frequency.id);
    this.frequenciesService.getFrequencyFile(frequency).subscribe(data=>{
      var url = window.URL.createObjectURL(new Blob([data]));
      var a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = url;
      a.download = frequency.fileName;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
    });
  }
}
