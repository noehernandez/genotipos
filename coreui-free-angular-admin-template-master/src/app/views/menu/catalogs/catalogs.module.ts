import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterModule } from '../../master/master.module';
import { ModalModule } from 'ngx-bootstrap/modal';
// import { ModalsComponent } from '../../notifications/modals.component';

// routing
import { CatalogsRoutingModule } from './catalogs-routing.module';

// componets
import { CatalogsComponent } from './catalogs.component';
import { LabelsComponent } from './labels/labels.component';
import { CategoriesComponent } from './categories/categories.component';
import { SourcesComponent } from './sources/sources.component';
import { MarkersComponent } from './markers/markers.component';
import { FrequenciesComponent } from './frequencies/frequencies.component';
// import { ModalsComponent } from '../../notifications/modals.component';


@NgModule({
  declarations: [
    CatalogsComponent, 
    LabelsComponent, 
    CategoriesComponent, 
    SourcesComponent, 
    MarkersComponent, 
    FrequenciesComponent,
    // ModalsComponent,
    // ModalsComponent
  ],
  imports: [
    CommonModule,
    CatalogsRoutingModule,
    MasterModule,
    ModalModule.forRoot(),
    // FormsModule,
    // ReactiveFormsModule,
  ]
})
export class CatalogsModule { }
