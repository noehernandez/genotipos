import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditComponent } from './audit.component';

//routing
import { AuditRoutingModule } from './audit-routing.module'

@NgModule({
  declarations: [AuditComponent],
  imports: [
    CommonModule,
    AuditRoutingModule,
  ]
})
export class AuditModule { }
