import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  // BARCHART
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = ['Etiqueta A', 
  'Etiqueta B', 'Etiqueta C', 
  'Etiqueta D', 'Etiqueta E', 'Etiqueta F', 'Etiqueta G'];
  public barChartType = 'bar';
  public barChartLegend = true;

  public barChartData: any[] = [
    {data: ['30'], label: 'Categoria 1'},
    {data: ['15'], label: 'Categoria 2'},
  ];

}
