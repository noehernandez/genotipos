import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalsComponent } from '../notifications/modals.component';
// Tabs Component
import { TabsModule } from 'ngx-bootstrap/tabs';

@NgModule({
  
  imports: [
    CommonModule,
    // ModalModule.forRoot(),
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    NgbPaginationModule,
    NgbAlertModule,
    NgSelectModule,
    TabsModule,
    // ModalsComponent
  ],
  declarations: [
    // ModalsComponent
  ]
})
export class MasterModule { }
