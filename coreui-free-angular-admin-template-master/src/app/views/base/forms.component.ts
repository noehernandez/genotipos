import { Component, OnInit } from '@angular/core';


@Component({
  templateUrl: 'forms.component.html'
})
export class FormsComponent {

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  constructor() { }

  ngOnInit() {
    this.dropdownList = [
      { "id": 1, "itemName": "Violaciones" },
      { "id": 2, "itemName": "Asesinatos" },
      { "id": 3, "itemName": "Desaparecidos" },
      { "id": 4, "itemName": "La Garita" },
      { "id": 5, "itemName": "Suicidios" }
    ];
    
    this.dropdownSettings = {
      singleSelection: false,
      text: "Seleccione las etiquetas",
      selectAllText: 'Seleccionar todo',
      unSelectAllText: 'Deseleccionar todo',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      badgeShowLimit: 3
    };
  }

  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

}
