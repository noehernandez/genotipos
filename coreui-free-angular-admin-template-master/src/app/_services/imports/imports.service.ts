import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { Cookie } from 'ng2-cookies';

import { APIResponse } from '../../_model/api-response.model';
import { API_REST_IMPORTS } from '../config';
import { Import } from '../../_model/imports/import.model';

@Injectable({
  providedIn: 'root'
})
export class ImportsService {

  constructor(private http: HttpClient) { }

  api_imports: string = `${API_REST_IMPORTS}import`;

  import$: Import;

  getImport():Import{
    return this.import$;
  }
  setImport(import$: Import){
    this.import$ = import$;
  }

  getAllImports(): Observable<APIResponse> {
    return this.http.get<APIResponse>(this.api_imports);
  }

  importProfiles(file: File,carga: string): Observable<APIResponse> {
    const formdata: FormData = new FormData();
      formdata.append('file', file);
      formdata.append('import', carga);
      return this.http.post<APIResponse>(`${API_REST_IMPORTS}import/1`, formdata, {reportProgress: true});
  }
}
