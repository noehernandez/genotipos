import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { Cookie } from 'ng2-cookies';

import { APIResponse } from '../../_model/api-response.model';
import { API_REST_SEARCHES,API_REST_SEARCHES_SAVE } from '../config';
import { FrequencyImport } from '../../_model/catalogos/frequency-import.model';
import { Search } from '../../_model/searches/search.model';
import { SaveSearch } from '../../_model/searches/save-search.model';

@Injectable({
  providedIn: 'root'
})
export class SearchesService {

  private searchId:number;
  private compatibleResultId:number;

  public getCompatibleResultId():number{
    return this.compatibleResultId;
  }
  public setCompatibleResultId(compatibleResultId: number){
    this.compatibleResultId=compatibleResultId;
  }
  public getSearchId():number{
    return this.searchId;
  }
  public setSearchId(searchId: number){
    this.searchId=searchId;
  }

  constructor(private http: HttpClient) { }

  searchByProfile(search: Search):Observable<any> {
    return this.http.post<any>(API_REST_SEARCHES+'busqueda_Por_Perfil',search);
  }

  searchByLabels(search: Search):Observable<any> {
    return this.http.post<any>(API_REST_SEARCHES+'busqueda_Por_Etiquetas',search);
  }
  
  saveSearch(saveSearch: SaveSearch):Observable<APIResponse>{
    return this.http.post<APIResponse>(API_REST_SEARCHES_SAVE+'api/searches',saveSearch);
  }

  getActiveSearchs(userId: number):Observable<APIResponse>{
    return this.http.get<APIResponse>(API_REST_SEARCHES_SAVE+'api/searches/active/'+userId);
  }

  getSearchResult(searchId: number):Observable<APIResponse>{
    return this.http.get<APIResponse>(API_REST_SEARCHES_SAVE+'api/searches/'+searchId);
  }

  getCompatibleMarkers(id: number):Observable<APIResponse>{
    return this.http.get<APIResponse>(API_REST_SEARCHES_SAVE+'api/searches/compatible-profile/'+id);
  }

}
