import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { Cookie } from 'ng2-cookies';

import { APIResponse } from '../../_model/api-response.model';
import { API_REST_IMPORTS, API_REST_PROFILES } from '../config';
import { Import } from '../../_model/imports/import.model';
import { Profile } from '../../_model/profiles/profile.model';

@Injectable({
  providedIn: 'root'
})
export class ProfilesService {

  constructor(private http: HttpClient) { }

  api_profiles: string = `${API_REST_PROFILES}profiles`;
  profile: Profile;

  getProfile():Profile{
    return this.profile;
  }
  setProfile(profile: Profile){
    this.profile = profile;
  }

  getImportedProfiles(importId: number): Observable<APIResponse> {
    return this.http.get<APIResponse>(this.api_profiles+'/'+importId);
  }

  getProfileDetail(profileId: number): Observable<APIResponse> {
    return this.http.get<APIResponse>(this.api_profiles+'/detail/'+profileId);
  }

  getProfileItems(userId: number): Observable<APIResponse> {
    return this.http.get<APIResponse>(this.api_profiles+'/active/'+userId);
  }

}
