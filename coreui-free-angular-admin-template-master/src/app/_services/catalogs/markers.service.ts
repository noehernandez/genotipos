import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { Cookie } from 'ng2-cookies';

import { APIResponse } from '../../_model/api-response.model';
import { API_REST_GENOTIPOS } from '../config';
import { Marker } from '../../_model/catalogos/marker.model';

@Injectable({
  providedIn: 'root'
})
export class MarkersService {

  constructor(private http: HttpClient) { }

  api_marker: string = `${API_REST_GENOTIPOS}marker`;

  getActiveMarkers(): Observable<APIResponse> {
    // let token = Cookie.get('access_token');
    // this.headers = new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   'Authorization': 'Bearer ' + token
    // });
    // console.log("insert " + categoria.nombre);
    return this.http.get<APIResponse>(this.api_marker+'/active/1');
  }

  addMarker(marker: Marker): Observable<APIResponse> {
    return this.http.post<APIResponse>(this.api_marker+'/1',marker);
  }

  deleteMarker(marker: Marker): Observable<APIResponse> {
    return this.http.delete<APIResponse>(this.api_marker+'/'+marker.id);
  }

  editMarker(marker: Marker): Observable<APIResponse> {
    return this.http.put<APIResponse>(this.api_marker,marker);
  }

}
