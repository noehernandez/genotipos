import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { Cookie } from 'ng2-cookies';

import { APIResponse } from '../../_model/api-response.model';
import { API_REST_GENOTIPOS } from '../config';
import { Label } from '../../_model/catalogos/label.model';


@Injectable({
  providedIn: 'root'
})
export class LabelsService {

  constructor(private http: HttpClient) { }

// ] categorias = {API_REST_GENOTIPOS}
  api_label: string = `${API_REST_GENOTIPOS}label`;

  getActiveLabels(): Observable<APIResponse> {
    return this.http.get<APIResponse>(this.api_label+'/active/1');
  }

  addLabel(label: Label): Observable<APIResponse> {
    return this.http.post<APIResponse>(this.api_label+'/1',label);
  }

  getLabelsByCategory(idCategory: number): Observable<APIResponse>{
    return this.http.get<APIResponse>(this.api_label+'/category/'+idCategory);
  }

}
