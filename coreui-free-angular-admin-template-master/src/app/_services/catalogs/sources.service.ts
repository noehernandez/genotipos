import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { Cookie } from 'ng2-cookies';

import { APIResponse } from '../../_model/api-response.model';
import { API_REST_GENOTIPOS } from '../config';
import { Source } from '../../_model/catalogos/source.model';

@Injectable({
  providedIn: 'root'
})
export class SourcesService {

  constructor(private http: HttpClient) { }

  api_source: string = `${API_REST_GENOTIPOS}source`;

  getActiveSources(): Observable<APIResponse> {
    return this.http.get<APIResponse>(this.api_source+'/active/1');
  }

  addSource(source: Source): Observable<APIResponse> {
    return this.http.post<APIResponse>(this.api_source+'/1',source);
  }

  deleteSource(source: Source): Observable<APIResponse> {
    return this.http.delete<APIResponse>(this.api_source+'/'+source.id);
  }

  editSource(source: Source): Observable<APIResponse> {
    return this.http.put<APIResponse>(this.api_source,source);
  }

}
