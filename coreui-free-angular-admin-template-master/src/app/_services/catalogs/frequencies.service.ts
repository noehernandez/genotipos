import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { Cookie } from 'ng2-cookies';

import { APIResponse } from '../../_model/api-response.model';
import { API_REST_GENOTIPOS } from '../config';
import { FrequencyImport } from '../../_model/catalogos/frequency-import.model';

@Injectable({
  providedIn: 'root'
})
export class FrequenciesService {

  constructor(private http: HttpClient) { }

// ] categorias = {API_REST_GENOTIPOS}
  api_frequency: string = `${API_REST_GENOTIPOS}frequency`;

  getActiveFrequencies(): Observable<APIResponse> {
    return this.http.get<APIResponse>(this.api_frequency+'/active/1');
  }

  importFrequencyTable(file: File,name: string): Observable<APIResponse> {
    const formdata: FormData = new FormData();
      formdata.append('file', file);
      formdata.append('name', name);
      return this.http.post<APIResponse>(this.api_frequency+'/1', formdata, {reportProgress: true});
  }

  assignByDefault(frequency: FrequencyImport): Observable<APIResponse> {
    return this.http.put<APIResponse>(this.api_frequency+'/default/'+frequency.id,null);
  }
  deleteFrequency(frequency: FrequencyImport): Observable<APIResponse> {
    return this.http.delete<APIResponse>(this.api_frequency+'/'+frequency.id);
  }
  getFrequencyFile(frequency: FrequencyImport): Observable<Blob> {
    const headers = new HttpHeaders({
      'Content-Type':'application/octet-stream',
      'Accept': 'application/octet-stream'
    });
    return this.http.get(this.api_frequency+'/file/'+frequency.id,{ headers: headers, responseType:"blob"});
  }
}
