import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { Cookie } from 'ng2-cookies';

import { APIResponse } from '../../_model/api-response.model';
import { API_REST_GENOTIPOS } from '../config';
import { Category } from '../../_model/catalogos/category.model';


@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) { }

// ] categorias = {API_REST_GENOTIPOS}
  api_category: string = `${API_REST_GENOTIPOS}category`;

  getActiveCategories(): Observable<APIResponse> {
    return this.http.get<APIResponse>(this.api_category+'/active/1');
  }

  addCategory(category: Category): Observable<APIResponse> {
    return this.http.post<APIResponse>(this.api_category+'/1',category);
  }

  deleteCategory(category: Category): Observable<APIResponse> {
    return this.http.delete<APIResponse>(this.api_category+'/'+category.id);
  }

  updateCategory(category: Category): Observable<APIResponse> {
    return this.http.put<APIResponse>(this.api_category, category);
  }
  // getAllCategories(): Observable<APIResponse> {
  //   // let token = Cookie.get('access_token');
  //   // this.headers = new HttpHeaders({
  //   //   'Content-Type': 'application/json',
  //   //   'Authorization': 'Bearer ' + token
  //   // });
  //   // console.log("insert " + categoria.nombre);
  //   return this.http.post<APIResponse>(this.endpointCategorias, categoria, { headers: this.headers });
  // }
}
