package mx.com.genotipos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BusquedaGenotiposApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusquedaGenotiposApplication.class, args);
	}

}
