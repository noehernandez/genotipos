package mx.com.genotipos.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

@Configuration
public class DbConfig {

	@Autowired
	private Environment env;
	
	@Bean(name = "barDataSource")
	public DataSource barDataSource() {
		return DataSourceBuilder.create().url(env.getProperty("spring.url.prueba"))
				.username(env.getProperty("spring.username.prueba"))
				.password(env.getProperty("spring.password.prueba"))
				.driverClassName(env.getProperty("spring.driverClassName.prueba")).build();
	}

	@Bean(name = "barEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean barEntityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("barDataSource") DataSource dataSource) {
		return
		builder.dataSource(dataSource).packages("mx.com.genotipos.model").build();
	}

}