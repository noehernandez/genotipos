package mx.com.genotipos;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.util.function.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mx.com.genotipos.model.AlelosFrecuencia;
import mx.com.genotipos.model.EtiquetaPerfil;
import mx.com.genotipos.model.UniversoPerfilGenetico;
import mx.com.genotipos.repository.IRepositoryConsulta;

@Component
public class HelperBusquetaEtiqueta {

//	@Autowired
	private IRepositoryConsulta repositoryConsulta;

    public IRepositoryConsulta getRepositoryConsulta() {
        return repositoryConsulta;
    }

    public void setRepositoryConsulta(IRepositoryConsulta repositoryConsulta) {
        this.repositoryConsulta = repositoryConsulta;
    }

        
        
        

	@SuppressWarnings("unchecked")
	public UniversoPerfilGenetico objetoListaPerfil(List<Object[]> listEtiquetaPrueba, int idFrecuencia) {
		UniversoPerfilGenetico universoPG = new UniversoPerfilGenetico();
		boolean agrupacion = false;
		int valor = 0;

		List<List<EtiquetaPerfil>> listaPerfilesAgrupado = new ArrayList<List<EtiquetaPerfil>>();
		List<EtiquetaPerfil> listEtiquetaOrigen = null;

		/* Valor de las frecuencias */

		List<AlelosFrecuencia> listaUniversoAlelosFrecuencia = repositoryConsulta.seleccionAlelos_Frecuencia(idFrecuencia);

		for (Object[] objeto : listEtiquetaPrueba) {
			EtiquetaPerfil ePerfil = new EtiquetaPerfil();
			ePerfil.setNombreEtiqueta((String) objeto[0]);
			ePerfil.setPerfilGenetico((Integer) objeto[1]);
			ePerfil.setIdMarcador((Integer) objeto[2]);

			/* agregar validacion de la frecuencia */

			List<AlelosFrecuencia> listaFrecuencias = listaUniversoAlelosFrecuencia.stream()
					.filter(x -> x.getCat_mar_id().equals(ePerfil.getIdMarcador())).collect(Collectors.toList());

			ePerfil.setListaFrecuencias(listaFrecuencias);

			ePerfil.setAlelo_X((String) objeto[3]);
			ePerfil.setAlelo_Y((String) objeto[4]);
			ePerfil.setNombreMarcador((String) objeto[5]);
			ePerfil.setIdentificador((String) objeto[6]);
			ePerfil.setEdoAbreviacion((String) objeto[7]);

			if (valor != 0 && valor != (Integer) objeto[1])
				agrupacion = false;

			if (agrupacion == false) {
				if (listEtiquetaOrigen != null) {
					listaPerfilesAgrupado.add(listEtiquetaOrigen);
				}
				listEtiquetaOrigen = new ArrayList<EtiquetaPerfil>();
				listEtiquetaOrigen.add(ePerfil);
				agrupacion = true;
				valor = (Integer) objeto[1];
			} else {
				listEtiquetaOrigen.add(ePerfil);
			}
		}
		
		listaPerfilesAgrupado.add(listEtiquetaOrigen);
		
		/*Elimina marcadores duplicados */
		
		List<List<EtiquetaPerfil>> listaPerfilesAgrupadoAux = new ArrayList<List<EtiquetaPerfil>>();

		for (List<EtiquetaPerfil> listEtiquetaFilter : listaPerfilesAgrupado) {
			List<EtiquetaPerfil> listGuardarEtiquetaPerfil = listEtiquetaFilter.stream()
					.filter(distinctByKeys(EtiquetaPerfil::getIdMarcador)).collect(Collectors.toList());
			listaPerfilesAgrupadoAux.add(listGuardarEtiquetaPerfil);
		}
		
		universoPG.setListaPerfilesAgrupado(listaPerfilesAgrupadoAux);
		return universoPG;
	}

	
	@SuppressWarnings("unchecked")
	public UniversoPerfilGenetico objetoPerfil(List<Object[]> listEtiquetaPrueba , int idFrecuencia) {
		UniversoPerfilGenetico universoPG = new UniversoPerfilGenetico();
		boolean agrupacion = false;
		int valor = 0;

		List<List<EtiquetaPerfil>> listaPerfilesAgrupado = new ArrayList<List<EtiquetaPerfil>>();
		List<EtiquetaPerfil> listEtiquetaOrigen = null;

		/* Valor de las frecuencias */

		List<AlelosFrecuencia> listaUniversoAlelosFrecuencia = repositoryConsulta.seleccionAlelos_Frecuencia(idFrecuencia);

		for (Object[] objeto : listEtiquetaPrueba) {
			EtiquetaPerfil ePerfil = new EtiquetaPerfil();
			ePerfil.setPerfilGenetico((Integer) objeto[0]);
			ePerfil.setIdMarcador((Integer) objeto[1]);

			/* agregar validacion de la frecuencia */

			List<AlelosFrecuencia> listaFrecuencias = listaUniversoAlelosFrecuencia.stream()
					.filter(x -> x.getCat_mar_id().equals(ePerfil.getIdMarcador())).collect(Collectors.toList());

			ePerfil.setListaFrecuencias(listaFrecuencias);

			ePerfil.setAlelo_X((String) objeto[2]);
			ePerfil.setAlelo_Y((String) objeto[3]);
			ePerfil.setNombreMarcador((String) objeto[4]);
			ePerfil.setIdentificador((String) objeto[5]);
			ePerfil.setEdoAbreviacion((String) objeto[6]);

			if (valor != 0 && valor != (Integer) objeto[0])
				agrupacion = false;

			if (agrupacion == false) {
				if (listEtiquetaOrigen != null) {
					listaPerfilesAgrupado.add(listEtiquetaOrigen);
				}
				listEtiquetaOrigen = new ArrayList<EtiquetaPerfil>();
				listEtiquetaOrigen.add(ePerfil);
				agrupacion = true;
				valor = (Integer) objeto[0];
			} else {
				listEtiquetaOrigen.add(ePerfil);
			}
		}

		
		listaPerfilesAgrupado.add(listEtiquetaOrigen);
		
		/*Elimina marcadores duplicados */
		
		List<List<EtiquetaPerfil>> listaPerfilesAgrupadoAux = new ArrayList<List<EtiquetaPerfil>>();

		for (List<EtiquetaPerfil> listEtiquetaFilter : listaPerfilesAgrupado) {
			List<EtiquetaPerfil> listGuardarEtiquetaPerfil = listEtiquetaFilter.stream()
					.filter(distinctByKeys(EtiquetaPerfil::getIdMarcador)).collect(Collectors.toList());
			listaPerfilesAgrupadoAux.add(listGuardarEtiquetaPerfil);
		}
		
		universoPG.setListaPerfilesAgrupado(listaPerfilesAgrupadoAux);
		return universoPG;
	}

	
	@SuppressWarnings("unchecked")
	private static <T> Predicate<T> distinctByKeys(Function<? super T, ?>... keyExtractors) {
		final Map<List<?>, Boolean> seen = new ConcurrentHashMap<>();
		return t -> {
			final List<?> keys = Arrays.stream(keyExtractors).map(ke -> ke.apply(t)).collect(Collectors.toList());

			return seen.putIfAbsent(keys, Boolean.TRUE) == null;
		};
	}

}
