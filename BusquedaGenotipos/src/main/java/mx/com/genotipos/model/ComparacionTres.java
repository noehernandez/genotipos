package mx.com.genotipos.model;

public class ComparacionTres {
	
	
	private String aleloXProgenitor;
	private String valorXProgenitor;
	private String aleloYProgenitor;
	private String valorYProgenitor;
	
	private String aleloXSeguro;
	private String valorXSeguro;
	private String aleloYSeguro;
	private String valorYSeguro;
	
	
	private String aleloXHijo;
	private String valorXHijo;
	private String aleloYHijo;
	private String valorYHijo;
	
	
	
	public String getAleloXProgenitor() {
		return aleloXProgenitor;
	}
	public String getValorXProgenitor() {
		return valorXProgenitor;
	}
	public String getAleloYProgenitor() {
		return aleloYProgenitor;
	}
	public String getValorYProgenitor() {
		return valorYProgenitor;
	}
	public String getAleloXSeguro() {
		return aleloXSeguro;
	}
	public String getValorXSeguro() {
		return valorXSeguro;
	}
	public String getAleloYSeguro() {
		return aleloYSeguro;
	}
	public String getValorYSeguro() {
		return valorYSeguro;
	}
	public String getAleloXHijo() {
		return aleloXHijo;
	}
	public String getValorXHijo() {
		return valorXHijo;
	}
	public String getAleloYHijo() {
		return aleloYHijo;
	}
	public String getValorYHijo() {
		return valorYHijo;
	}
	public void setAleloXProgenitor(String aleloXProgenitor) {
		this.aleloXProgenitor = aleloXProgenitor;
	}
	public void setValorXProgenitor(String valorXProgenitor) {
		this.valorXProgenitor = valorXProgenitor;
	}
	public void setAleloYProgenitor(String aleloYProgenitor) {
		this.aleloYProgenitor = aleloYProgenitor;
	}
	public void setValorYProgenitor(String valorYProgenitor) {
		this.valorYProgenitor = valorYProgenitor;
	}
	public void setAleloXSeguro(String aleloXSeguro) {
		this.aleloXSeguro = aleloXSeguro;
	}
	public void setValorXSeguro(String valorXSeguro) {
		this.valorXSeguro = valorXSeguro;
	}
	public void setAleloYSeguro(String aleloYSeguro) {
		this.aleloYSeguro = aleloYSeguro;
	}
	public void setValorYSeguro(String valorYSeguro) {
		this.valorYSeguro = valorYSeguro;
	}
	public void setAleloXHijo(String aleloXHijo) {
		this.aleloXHijo = aleloXHijo;
	}
	public void setValorXHijo(String valorXHijo) {
		this.valorXHijo = valorXHijo;
	}
	public void setAleloYHijo(String aleloYHijo) {
		this.aleloYHijo = aleloYHijo;
	}
	public void setValorYHijo(String valorYHijo) {
		this.valorYHijo = valorYHijo;
	}
	

}
