package mx.com.genotipos.model;

public class Objetivo {

	private String alelo1;
	private String alelo2;
	
	public String getAlelo1() {
		return alelo1;
	}
	public void setAlelo1(String alelo1) {
		this.alelo1 = alelo1;
	}
	public String getAlelo2() {
		return alelo2;
	}
	public void setAlelo2(String alelo2) {
		this.alelo2 = alelo2;
	}
	
	
	
}
