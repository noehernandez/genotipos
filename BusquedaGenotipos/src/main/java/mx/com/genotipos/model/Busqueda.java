package mx.com.genotipos.model;

import java.util.List;

public class Busqueda {
	
	private Resultado resultado;
	private List<Marcador> comparativoMarcadores;

	public Resultado getResultado() {
		return resultado;
	}

	public void setResultado(Resultado resultado) {
		this.resultado = resultado;
	}

	public List<Marcador> getComparativoMarcadores() {
		return comparativoMarcadores;
	}

	public void setComparativoMarcadores(List<Marcador> comparativoMarcadores) {
		this.comparativoMarcadores = comparativoMarcadores;
	}



}
