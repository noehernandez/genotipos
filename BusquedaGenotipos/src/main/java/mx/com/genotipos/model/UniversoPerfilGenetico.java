package mx.com.genotipos.model;

import java.util.List;

public class UniversoPerfilGenetico {

	private List<List<EtiquetaPerfil>> listaPerfilesAgrupado ;

	public List<List<EtiquetaPerfil>> getListaPerfilesAgrupado() {
		return listaPerfilesAgrupado;
	}

	public void setListaPerfilesAgrupado(List<List<EtiquetaPerfil>> listaPerfilesAgrupado) {
		this.listaPerfilesAgrupado = listaPerfilesAgrupado;
	}


}
