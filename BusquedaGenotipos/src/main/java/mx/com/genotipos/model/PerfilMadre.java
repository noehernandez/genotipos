package mx.com.genotipos.model;

public class PerfilMadre {
	
	private String alelo1;
	private Integer coincideAlelo1;
	private String alelo2;
	private Integer coincideAlelo2;
	
	public String getAlelo1() {
		return alelo1;
	}
	public void setAlelo1(String alelo1) {
		this.alelo1 = alelo1;
	}

	public String getAlelo2() {
		return alelo2;
	}
	public void setAlelo2(String alelo2) {
		this.alelo2 = alelo2;
	}
	
	public Integer getCoincideAlelo1() {
		return coincideAlelo1;
	}
	public Integer getCoincideAlelo2() {
		return coincideAlelo2;
	}
	public void setCoincideAlelo1(Integer coincideAlelo1) {
		this.coincideAlelo1 = coincideAlelo1;
	}
	public void setCoincideAlelo2(Integer coincideAlelo2) {
		this.coincideAlelo2 = coincideAlelo2;
	}
	
}
