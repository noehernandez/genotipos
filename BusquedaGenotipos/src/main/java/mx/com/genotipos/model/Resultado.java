package mx.com.genotipos.model;

public class Resultado {
	
	private String genotipoObjetivo;
	private String genotipoCompatible;
	private String perfilPadre;
	private String perfilMadre;
	private String perfilObjetivo;
	private String estado;
	private String amel;
	private String ip;
	private String pp;
	
	public String getGenotipoObjetivo() {
		return genotipoObjetivo;
	}
	public void setGenotipoObjetivo(String genotipoObjetivo) {
		this.genotipoObjetivo = genotipoObjetivo;
	}
	public String getGenotipoCompatible() {
		return genotipoCompatible;
	}
	public void setGenotipoCompatible(String genotipoCompatible) {
		this.genotipoCompatible = genotipoCompatible;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getAmel() {
		return amel;
	}
	public void setAmel(String amel) {
		this.amel = amel;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getPp() {
		return pp;
	}
	public void setPp(String pp) {
		this.pp = pp;
	}
	public String getPerfilPadre() {
		return perfilPadre;
	}
	public String getPerfilMadre() {
		return perfilMadre;
	}
	public String getPerfilObjetivo() {
		return perfilObjetivo;
	}
	public void setPerfilPadre(String perfilPadre) {
		this.perfilPadre = perfilPadre;
	}
	public void setPerfilMadre(String perfilMadre) {
		this.perfilMadre = perfilMadre;
	}
	public void setPerfilObjetivo(String perfilObjetivo) {
		this.perfilObjetivo = perfilObjetivo;
	}
	

}
