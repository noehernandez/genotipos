package mx.com.genotipos.model;

import java.util.List;

public class Marcador {
	
	private String marcador;
	private Integer idMarcador;
	private Objetivo objetivo;
	private Compatible compatible;
	private PerfilPadre perfilPadre;
	private PerfilMadre perfilMadre;
	
	private Integer numeroFormula;
	private String a;
	private String b;
	private String c;
	
	private List<AlelosFrecuencia> listaFrecuencias;
	
	public String getMarcador() {
		return marcador;
	}
	public Integer getIdMarcador() {
		return idMarcador;
	}
	public Objetivo getObjetivo() {
		return objetivo;
	}
	public Compatible getCompatible() {
		return compatible;
	}
	public Integer getNumeroFormula() {
		return numeroFormula;
	}
	public String getA() {
		return a;
	}
	public String getB() {
		return b;
	}
	public void setMarcador(String marcador) {
		this.marcador = marcador;
	}
	public void setIdMarcador(Integer idMarcador) {
		this.idMarcador = idMarcador;
	}
	public void setObjetivo(Objetivo objetivo) {
		this.objetivo = objetivo;
	}
	public void setCompatible(Compatible compatible) {
		this.compatible = compatible;
	}
	public void setNumeroFormula(Integer numeroFormula) {
		this.numeroFormula = numeroFormula;
	}
	public void setA(String a) {
		this.a = a;
	}
	public void setB(String b) {
		this.b = b;
	}
	public List<AlelosFrecuencia> getListaFrecuencias() {
		return listaFrecuencias;
	}
	public void setListaFrecuencias(List<AlelosFrecuencia> listaFrecuencias) {
		this.listaFrecuencias = listaFrecuencias;
	}
	public PerfilPadre getPerfilPadre() {
		return perfilPadre;
	}
	public void setPerfilPadre(PerfilPadre perfilPadre) {
		this.perfilPadre = perfilPadre;
	}
	public PerfilMadre getPerfilMadre() {
		return perfilMadre;
	}
	public void setPerfilMadre(PerfilMadre perfilMadre) {
		this.perfilMadre = perfilMadre;
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	
	//----------------------------//

	
}
