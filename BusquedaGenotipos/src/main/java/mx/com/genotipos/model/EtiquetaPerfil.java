package mx.com.genotipos.model;

import java.util.List;

public class EtiquetaPerfil {

	private String nombreEtiqueta;
	private Integer perfilGenetico;
	private Integer idMarcador;
	private String alelo_X;
	private String alelo_Y;
	private String nombreMarcador;
	private String identificador;
	private String edoAbreviacion;
	
	private List<AlelosFrecuencia> listaFrecuencias;

	public String getNombreEtiqueta() {
		return nombreEtiqueta;
	}

	public void setNombreEtiqueta(String nombreEtiqueta) {
		this.nombreEtiqueta = nombreEtiqueta;
	}

	public Integer getPerfilGenetico() {
		return perfilGenetico;
	}

	public void setPerfilGenetico(Integer perfilGenetico) {
		this.perfilGenetico = perfilGenetico;
	}

	public Integer getIdMarcador() {
		return idMarcador;
	}

	public void setIdMarcador(Integer idMarcador) {
		this.idMarcador = idMarcador;
	}

	public String getAlelo_X() {
		return alelo_X;
	}

	public void setAlelo_X(String alelo_X) {
		this.alelo_X = alelo_X;
	}

	public String getAlelo_Y() {
		return alelo_Y;
	}

	public void setAlelo_Y(String alelo_Y) {
		this.alelo_Y = alelo_Y;
	}

	public List<AlelosFrecuencia> getListaFrecuencias() {
		return listaFrecuencias;
	}

	public void setListaFrecuencias(List<AlelosFrecuencia> listaFrecuencias) {
		this.listaFrecuencias = listaFrecuencias;
	}

	public String getNombreMarcador() {
		return nombreMarcador;
	}

	public void setNombreMarcador(String nombreMarcador) {
		this.nombreMarcador = nombreMarcador;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getEdoAbreviacion() {
		return edoAbreviacion;
	}

	public void setEdoAbreviacion(String edoAbreviacion) {
		this.edoAbreviacion = edoAbreviacion;
	}

	

}
