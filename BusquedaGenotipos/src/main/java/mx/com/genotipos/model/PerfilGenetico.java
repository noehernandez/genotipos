package mx.com.genotipos.model;

import java.util.ArrayList;
import java.util.List;

public class PerfilGenetico {
	
	private boolean marcadorValido;
	private List<Marcador> listMarcador = new ArrayList<Marcador>();
	
	public boolean isMarcadorValido() {
		return marcadorValido;
	}
	public void setMarcadorValido(boolean marcadorValido) {
		this.marcadorValido = marcadorValido;
	}
	public List<Marcador> getListMarcador() {
		return listMarcador;
	}
	public void setListMarcador(List<Marcador> listMarcador) {
		this.listMarcador = listMarcador;
	}

}
