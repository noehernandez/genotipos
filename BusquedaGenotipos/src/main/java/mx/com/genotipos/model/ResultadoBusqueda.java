package mx.com.genotipos.model;

import java.util.List;

public class ResultadoBusqueda {
	
	private List<Busqueda> busquedas;

	public List<Busqueda> getBusquedas() {
		return busquedas;
	}

	public void setBusquedas(List<Busqueda> busquedas) {
		this.busquedas = busquedas;
	}

}
