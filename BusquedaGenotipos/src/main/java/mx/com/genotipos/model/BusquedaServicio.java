package mx.com.genotipos.model;

import java.util.List;

public class BusquedaServicio {
	
	private Integer idPerfil;
	private Integer idPerfilProgenitor;
	private Integer idPerfilSeguro;
	private Integer idPerfilHijo;
	private List<Integer> listaIdEntiquetasOrigen;
	private List<Integer> listaIdEntiquetasProgenitor;
	private Integer numeroExclusiones;
	private Integer numeroMarcadores;
	private Integer idTablaFrecuencia;
	private boolean descartarPerfilesRevision;
	
	public List<Integer> getListaIdEntiquetasOrigen() {
		return listaIdEntiquetasOrigen;
	}
	public List<Integer> getListaIdEntiquetasProgenitor() {
		return listaIdEntiquetasProgenitor;
	}
	public Integer getNumeroExclusiones() {
		return numeroExclusiones;
	}
	public Integer getNumeroMarcadores() {
		return numeroMarcadores;
	}
	public Integer getIdTablaFrecuencia() {
		return idTablaFrecuencia;
	}
	public void setListaIdEntiquetasOrigen(List<Integer> listaIdEntiquetasOrigen) {
		this.listaIdEntiquetasOrigen = listaIdEntiquetasOrigen;
	}
	public void setListaIdEntiquetasProgenitor(List<Integer> listaIdEntiquetasProgenitor) {
		this.listaIdEntiquetasProgenitor = listaIdEntiquetasProgenitor;
	}
	public void setNumeroExclusiones(Integer numeroExclusiones) {
		this.numeroExclusiones = numeroExclusiones;
	}
	public void setNumeroMarcadores(Integer numeroMarcadores) {
		this.numeroMarcadores = numeroMarcadores;
	}
	public void setIdTablaFrecuencia(Integer idTablaFrecuencia) {
		this.idTablaFrecuencia = idTablaFrecuencia;
	}
	public Integer getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(Integer idPerfil) {
		this.idPerfil = idPerfil;
	}
	public Integer getIdPerfilProgenitor() {
		return idPerfilProgenitor;
	}
	public Integer getIdPerfilSeguro() {
		return idPerfilSeguro;
	}
	public Integer getIdPerfilHijo() {
		return idPerfilHijo;
	}
	public void setIdPerfilProgenitor(Integer idPerfilProgenitor) {
		this.idPerfilProgenitor = idPerfilProgenitor;
	}
	public void setIdPerfilSeguro(Integer idPerfilSeguro) {
		this.idPerfilSeguro = idPerfilSeguro;
	}
	public void setIdPerfilHijo(Integer idPerfilHijo) {
		this.idPerfilHijo = idPerfilHijo;
	}
	public boolean isDescartarPerfilesRevision() {
		return descartarPerfilesRevision;
	}
	public void setDescartarPerfilesRevision(boolean descartarPerfilesRevision) {
		this.descartarPerfilesRevision = descartarPerfilesRevision;
	}

	
	
	
}
