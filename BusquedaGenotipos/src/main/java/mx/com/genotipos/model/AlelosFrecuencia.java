package mx.com.genotipos.model;

public class AlelosFrecuencia {
	
	private Integer cat_fre_id;
	private Double  valor_alelo;
	private Integer cat_mar_id;
	private String  cat_ale_descripcion;
	
	public String getCat_ale_descripcion() {
		return cat_ale_descripcion;
	}
	public void setCat_ale_descripcion(String cat_ale_descripcion) {
		this.cat_ale_descripcion = cat_ale_descripcion;
	}
	public Integer getCat_fre_id() {
		return cat_fre_id;
	}
	public void setCat_fre_id(Integer cat_fre_id) {
		this.cat_fre_id = cat_fre_id;
	}

	public Double getValor_alelo() {
		return valor_alelo;
	}
	public void setValor_alelo(Double valor_alelo) {
		this.valor_alelo = valor_alelo;
	}
	public Integer getCat_mar_id() {
		return cat_mar_id;
	}
	public void setCat_mar_id(Integer cat_mar_id) {
		this.cat_mar_id = cat_mar_id;
	}
	
	

}
