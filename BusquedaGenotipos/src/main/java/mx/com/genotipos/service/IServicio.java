package mx.com.genotipos.service;
import mx.com.genotipos.model.BusquedaServicio;
import mx.com.genotipos.model.ResultadoBusqueda;

public interface IServicio {
	
	ResultadoBusqueda busquedaEtiquetaPerfil(BusquedaServicio busquedaServicio);
	
	ResultadoBusqueda busquedaPerfil(BusquedaServicio busquedaServicio);
	
	ResultadoBusqueda busquedaTresParticipantes(BusquedaServicio busquedaServicio);

}
