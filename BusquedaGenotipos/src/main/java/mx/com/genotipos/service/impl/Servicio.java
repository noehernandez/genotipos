package mx.com.genotipos.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import mx.com.genotipos.model.AlelosFrecuencia;
import mx.com.genotipos.model.Busqueda;
import mx.com.genotipos.model.BusquedaServicio;
import mx.com.genotipos.model.ComparacionTres;
import mx.com.genotipos.model.Compatible;
import mx.com.genotipos.model.EtiquetaPerfil;
import mx.com.genotipos.model.Marcador;
import mx.com.genotipos.model.Objetivo;
import mx.com.genotipos.model.PerfilMadre;
import mx.com.genotipos.model.PerfilPadre;
import mx.com.genotipos.model.Resultado;
import mx.com.genotipos.model.ResultadoBusqueda;
import mx.com.genotipos.model.UniversoPerfilGenetico;
import mx.com.genotipos.repository.IRepositoryConsulta;
import mx.com.genotipos.service.IServicio;

@Service
@Scope("prototype")
public class Servicio implements IServicio {

	@Autowired
	private IRepositoryConsulta repositoryConsulta;

	private List<List<EtiquetaPerfil>> matrizEtiquetaProgenitor;

	private List<List<EtiquetaPerfil>> matrizEtiquetaOrigen;

	private List<List<EtiquetaPerfil>> matrizEtiquetaSeguro;

	private List<List<EtiquetaPerfil>> matrizEtiquetaHijo;

	private String identificadorOrigen;

	private String identificadorProgenitor;

	private boolean compatibilidad;

	/* Inicializacion */

	private int numeroExclusiones = 0;

	private int numeroMarcadores_Validar = 0;

	private int contadorCompatibilidad = 0;

	private List<Marcador> listMarcador = new ArrayList<Marcador>();

	private boolean banderaPerfilIncompatible = false;

	private boolean perfilGeneticoValido = false;

	private List<Busqueda> listaBusqueda = new ArrayList<Busqueda>();

	private String estado = "";

	private List<AlelosFrecuencia> listaFrecuencias;

	private List<AlelosFrecuencia> listaFrecuenciasB;

	private AlelosFrecuencia aleloFrecuencia;

	private AlelosFrecuencia alelosFrecuenciaB;

	private static Integer maximoBusquedasVista = 100; /* Numero maximo de resultados en la vista */

	private static Integer numeroBloquesAbuscar = 1000; /* divide la busqueda en bloques de mil */

	private boolean maximoBusquedas = false;

	private Double a;

	private Double b;

	private Double c;

	private Double IP;

	private Double IPGlobal;

	private Double PP;

	/* Variables a nivel de clase para formula de 3 perfiles */

	private List<EtiquetaPerfil> listEtiquetaProgenitor;

	private List<EtiquetaPerfil> listEtiquetaSeguro;

	private List<EtiquetaPerfil> listEtiquetaHijo;

	@Override
	public ResultadoBusqueda busquedaEtiquetaPerfil(BusquedaServicio busquedaServicio) {

		/* Inicio Variables */

		a = 0.0;
		b = 0.0;
		IP = 0.0;
		IPGlobal = 0.0;
		PP = 0.0;

		maximoBusquedas = false;

		contadorCompatibilidad = 0;

		listMarcador = new ArrayList<Marcador>();

		banderaPerfilIncompatible = false;

		perfilGeneticoValido = false;

		estado = "";

		listaBusqueda = new ArrayList<Busqueda>();

		numeroExclusiones = busquedaServicio.getNumeroExclusiones();

		numeroMarcadores_Validar = busquedaServicio.getNumeroMarcadores();

		/* Obtener etiquetas Origen */

		String listaBusquedaOrigen = "";
		List<Integer> listaIdEntiquetasOrigen = busquedaServicio.getListaIdEntiquetasOrigen();
		for (Integer idEntiquetasOrigen : listaIdEntiquetasOrigen) {
			listaBusquedaOrigen += "" + idEntiquetasOrigen + ",";
		}

		listaBusquedaOrigen = listaBusquedaOrigen.substring(0, listaBusquedaOrigen.length() - 1);

		/* Obtener etiquetas Progenitor */

		String listaBusquedaProgenitor = "";
		List<Integer> listaIdEntiquetaProgenitor = busquedaServicio.getListaIdEntiquetasProgenitor();
		for (Integer idEntiquetasProgenitor : listaIdEntiquetaProgenitor) {
			listaBusquedaProgenitor += "" + idEntiquetasProgenitor + ",";
		}

		listaBusquedaProgenitor = listaBusquedaProgenitor.substring(0, listaBusquedaProgenitor.length() - 1);

		/* Validacion para buscar perfiles en revision */

		String perfilesRevicion = "";
		if (busquedaServicio.isDescartarPerfilesRevision() == true)
			perfilesRevicion = "1";
		else
			perfilesRevicion = "1,3";

		/* Obtener Universo de perfiles Geneticos */
                UniversoPerfilGenetico universoPG_Origen =null;
                UniversoPerfilGenetico universoPG_Progenitor=null;
                System.out.println("buscando datos");
                try{
                     universoPG_Origen = repositoryConsulta.busquedaEtiquetaPerfil(listaBusquedaOrigen,
				perfilesRevicion, busquedaServicio.getIdTablaFrecuencia());

		 universoPG_Progenitor = repositoryConsulta.busquedaEtiquetaPerfil(
				listaBusquedaProgenitor, perfilesRevicion, busquedaServicio.getIdTablaFrecuencia());
                }catch(Exception e){
                    e.printStackTrace();
                }
		
                System.out.println("");
		if (universoPG_Origen != null && universoPG_Progenitor != null) {

			matrizEtiquetaOrigen = universoPG_Origen.getListaPerfilesAgrupado();
			matrizEtiquetaProgenitor = universoPG_Progenitor.getListaPerfilesAgrupado();

			System.out.println("matrizEtiquetaOrigen " + matrizEtiquetaOrigen.size());
			System.out.println("matrizEtiquetaProgenitor " + matrizEtiquetaProgenitor.size());

			if (matrizEtiquetaProgenitor.size() >= numeroBloquesAbuscar
					&& matrizEtiquetaOrigen.size() <= numeroBloquesAbuscar) {

				List<List<List<EtiquetaPerfil>>> listaEtiqueta = new ArrayList<>();

				int numeroBloques = matrizEtiquetaProgenitor.size() / numeroBloquesAbuscar;
				int numeroReciduo = matrizEtiquetaProgenitor.size() % numeroBloquesAbuscar;
				int incre = matrizEtiquetaProgenitor.size() - 1;

				for (int j = 0; j < numeroBloques; j++) {
					List<List<EtiquetaPerfil>> listaEtiquetaPerfil = new ArrayList<>();
					for (int i = incre; i >= incre - numeroBloquesAbuscar; i--) {
						if (i >= 0)
							listaEtiquetaPerfil.add(matrizEtiquetaProgenitor.get(i));
					}
					listaEtiqueta.add(listaEtiquetaPerfil);
					incre -= numeroBloquesAbuscar;
				}

				List<List<EtiquetaPerfil>> listaEtiquetaPerfil = new ArrayList<>();
				for (int i = incre; i >= incre - numeroReciduo; i--) {
					if (i >= 0)
						listaEtiquetaPerfil.add(matrizEtiquetaProgenitor.get(i));
				}

				listaEtiqueta.add(listaEtiquetaPerfil);

				for (int i = 0; i < listaEtiqueta.size(); i++) {
					matrizEtiquetaProgenitor = listaEtiqueta.get(i);
					int porsicion = matrizEtiquetaOrigen.size() - 1;
					buscarMatrizOrigen(matrizEtiquetaOrigen.get(porsicion), porsicion);
				}

			} else if (matrizEtiquetaProgenitor.size() >= numeroBloquesAbuscar
					&& matrizEtiquetaOrigen.size() >= numeroBloquesAbuscar) {

				/* Separar en bloques de busqueda PROGENITOR */

				System.out.println("Particion");

				List<List<List<EtiquetaPerfil>>> listaEtiquetaProgenitor = new ArrayList<>();

				int numeroBloques = matrizEtiquetaProgenitor.size() / numeroBloquesAbuscar;
				int numeroReciduo = matrizEtiquetaProgenitor.size() % numeroBloquesAbuscar;
				int incre = matrizEtiquetaProgenitor.size() - 1;

				for (int j = 0; j < numeroBloques; j++) {
					List<List<EtiquetaPerfil>> listaEtiquetaPerfil = new ArrayList<>();
					for (int i = incre; i >= incre - numeroBloquesAbuscar; i--) {
						if (i >= 0)
							listaEtiquetaPerfil.add(matrizEtiquetaProgenitor.get(i));
					}
					listaEtiquetaProgenitor.add(listaEtiquetaPerfil);
					incre -= numeroBloquesAbuscar;
				}

				List<List<EtiquetaPerfil>> listaEtiquetaPerfil = new ArrayList<>();
				for (int i = incre; i >= incre - numeroReciduo; i--) {
					if (i >= 0)
						listaEtiquetaPerfil.add(matrizEtiquetaProgenitor.get(i));
				}

				listaEtiquetaProgenitor.add(listaEtiquetaPerfil);

				/****************************************************************/

				/* Separar matriz ORIGEN */

				List<List<List<EtiquetaPerfil>>> listaEtiquetaOrigen = new ArrayList<>();

				int numeroBloquesOrigen = matrizEtiquetaOrigen.size() / numeroBloquesAbuscar;
				int numeroReciduoOrigen = matrizEtiquetaOrigen.size() % numeroBloquesAbuscar;
				int increOrigen = matrizEtiquetaOrigen.size() - 1;

				for (int j = 0; j < numeroBloquesOrigen; j++) {
					List<List<EtiquetaPerfil>> listaEtiquetaPerfilOrigen = new ArrayList<>();
					for (int i = increOrigen; i >= increOrigen - numeroBloquesAbuscar; i--) {
						if (i >= 0) {
							listaEtiquetaPerfilOrigen.add(matrizEtiquetaOrigen.get(i));
						}
					}

					List<List<EtiquetaPerfil>> listaEtiquetaPerfilOrigenAux = new ArrayList<>();
					for (int i = listaEtiquetaPerfilOrigen.size() - 1; i >= 0; i--) {
						if (i >= 0) {
							listaEtiquetaPerfilOrigenAux.add(listaEtiquetaPerfilOrigen.get(i));
						}
					}

					listaEtiquetaOrigen.add(listaEtiquetaPerfilOrigenAux);
					increOrigen -= numeroBloquesAbuscar;
				}

				/* Iteracion del reciduo */

				List<List<EtiquetaPerfil>> listaEtiquetaPerfilOrigen = new ArrayList<>();
				for (int i = increOrigen; i >= increOrigen - numeroReciduoOrigen; i--) {
					if (i >= 0)
						listaEtiquetaPerfilOrigen.add(matrizEtiquetaOrigen.get(i));
				}

				List<List<EtiquetaPerfil>> listaEtiquetaPerfilOrigenAux = new ArrayList<>();
				for (int i = listaEtiquetaPerfilOrigen.size() - 1; i >= 0; i--) {
					if (i >= 0) {
						listaEtiquetaPerfilOrigenAux.add(listaEtiquetaPerfilOrigen.get(i));
					}
				}

				listaEtiquetaOrigen.add(listaEtiquetaPerfilOrigenAux);

				/****************************************************************/

				for (int j = 0; j < listaEtiquetaOrigen.size(); j++) {
					matrizEtiquetaOrigen = listaEtiquetaOrigen.get(j);
					for (int i = 0; i < listaEtiquetaProgenitor.size(); i++) {
						matrizEtiquetaProgenitor = listaEtiquetaProgenitor.get(i);
						int porsicion = matrizEtiquetaOrigen.size() - 1;
						buscarMatrizOrigen(matrizEtiquetaOrigen.get(porsicion), porsicion);
					}
				}

				/******************************************/
			} else {

				System.out.println("NO Particion ejecuta completo");
				System.out.println(matrizEtiquetaProgenitor.size());
				int porsicion = matrizEtiquetaOrigen.size() - 1;
				buscarMatrizOrigen(matrizEtiquetaOrigen.get(porsicion), porsicion);
			}

		}

		listaBusqueda = eliminarFrecuencias(listaBusqueda);
		ResultadoBusqueda resultadoBusqueda = new ResultadoBusqueda();
		resultadoBusqueda.setBusquedas(listaBusqueda);
		System.out.println("[fin]" + listaBusqueda.size());
		return resultadoBusqueda;
	}

	@Override
	public ResultadoBusqueda busquedaPerfil(BusquedaServicio busquedaServicio) {

		/* Inicio Variables */

		a = 0.0;
		b = 0.0;
		IP = 0.0;
		IPGlobal = 0.0;
		PP = 0.0;

		maximoBusquedas = false;

		contadorCompatibilidad = 0;

		listMarcador = new ArrayList<Marcador>();

		banderaPerfilIncompatible = false;

		perfilGeneticoValido = false;

		estado = "";

		listaBusqueda = new ArrayList<Busqueda>();

		numeroExclusiones = busquedaServicio.getNumeroExclusiones();

		numeroMarcadores_Validar = busquedaServicio.getNumeroMarcadores();

		/* Obtener etiquetas Origen */

		String busquedaOrigen = "" + busquedaServicio.getIdPerfil();

		/* Obtener etiquetas Progenitor */

		String listaBusquedaProgenitor = "";
		List<Integer> listaIdEntiquetaProgenitor = busquedaServicio.getListaIdEntiquetasProgenitor();
		for (Integer idEntiquetasProgenitor : listaIdEntiquetaProgenitor) {
			listaBusquedaProgenitor += "" + idEntiquetasProgenitor + ",";
		}

		listaBusquedaProgenitor = listaBusquedaProgenitor.substring(0, listaBusquedaProgenitor.length() - 1);

		/* Validacion para buscar perfiles en revision */

		String perfilesRevicion = "";
		if (busquedaServicio.isDescartarPerfilesRevision() == true)
			perfilesRevicion = "1";
		else
			perfilesRevicion = "1,3";

		/* Obtener Universo de perfiles Geneticos */

		UniversoPerfilGenetico universoPG_Origen = repositoryConsulta.busquedaPerfil(busquedaOrigen,
				busquedaServicio.getIdTablaFrecuencia());

		UniversoPerfilGenetico universoPG_Progenitor = repositoryConsulta.busquedaEtiquetaPerfil(
				listaBusquedaProgenitor, perfilesRevicion, busquedaServicio.getIdTablaFrecuencia());

		if (universoPG_Origen != null && universoPG_Progenitor != null) {

			matrizEtiquetaOrigen = universoPG_Origen.getListaPerfilesAgrupado();

			matrizEtiquetaProgenitor = universoPG_Progenitor.getListaPerfilesAgrupado();

			System.out.println("matrizEtiquetaOrigen " + matrizEtiquetaOrigen.size());

			System.out.println("matrizEtiquetaProgenitor " + matrizEtiquetaProgenitor.size());

			if (matrizEtiquetaProgenitor.size() >= numeroBloquesAbuscar) {

				List<List<List<EtiquetaPerfil>>> listaEtiqueta = new ArrayList<>();

				int numeroBloques = matrizEtiquetaProgenitor.size() / numeroBloquesAbuscar;
				int numeroReciduo = matrizEtiquetaProgenitor.size() % numeroBloquesAbuscar;
				int incre = matrizEtiquetaProgenitor.size() - 1;

				for (int j = 0; j < numeroBloques; j++) {
					List<List<EtiquetaPerfil>> listaEtiquetaPerfil = new ArrayList<>();
					for (int i = incre; i >= incre - numeroBloquesAbuscar; i--) {
						if (i >= 0)
							listaEtiquetaPerfil.add(matrizEtiquetaProgenitor.get(i));
					}
					listaEtiqueta.add(listaEtiquetaPerfil);
					incre -= numeroBloquesAbuscar;
				}

				List<List<EtiquetaPerfil>> listaEtiquetaPerfil = new ArrayList<>();
				for (int i = incre; i >= incre - numeroReciduo; i--) {
					if (i >= 0)
						listaEtiquetaPerfil.add(matrizEtiquetaProgenitor.get(i));
				}

				listaEtiqueta.add(listaEtiquetaPerfil);

				for (int i = 0; i < listaEtiqueta.size(); i++) {
					matrizEtiquetaProgenitor = listaEtiqueta.get(i);
					int porsicion = matrizEtiquetaOrigen.size() - 1;
					buscarMatrizOrigen(matrizEtiquetaOrigen.get(porsicion), porsicion);
				}

			} else {
				int porsicion = matrizEtiquetaOrigen.size() - 1;
				buscarMatrizOrigen(matrizEtiquetaOrigen.get(porsicion), porsicion);
			}

			System.out.println("iniciaBusqueda");
		}

		listaBusqueda = eliminarFrecuencias(listaBusqueda);
		ResultadoBusqueda resultadoBusqueda = new ResultadoBusqueda();
		resultadoBusqueda.setBusquedas(listaBusqueda);
		System.out.println("[fin]" + listaBusqueda.size());
		return resultadoBusqueda;
	}

	/* Elimina la lista de las frecuencias para que no se muestren en la vista */

	private List<Busqueda> eliminarFrecuencias(List<Busqueda> listBusqueda) {
		for (Busqueda busqueda : listBusqueda) {
			for (Marcador marcador : busqueda.getComparativoMarcadores()) {
				marcador.setListaFrecuencias(null);
			}
		}
		return listBusqueda;
	}

	@Override
	public ResultadoBusqueda busquedaTresParticipantes(BusquedaServicio busquedaServicio) {

		UniversoPerfilGenetico universoPG_Progenitor = repositoryConsulta.busquedaPerfil(
				busquedaServicio.getIdPerfilProgenitor().toString(), busquedaServicio.getIdTablaFrecuencia());

		UniversoPerfilGenetico universoPG_Seguro = repositoryConsulta.busquedaPerfil(
				busquedaServicio.getIdPerfilSeguro().toString(), busquedaServicio.getIdTablaFrecuencia());

		UniversoPerfilGenetico universoPG_Hijo = repositoryConsulta
				.busquedaPerfil(busquedaServicio.getIdPerfilHijo().toString(), busquedaServicio.getIdTablaFrecuencia());

		/* Lectura de Informacion */

		String idEtiquetaProgenitor = "";

		String idEtiquetaHijo = "";

		String idEtiquetaSeguro = "";

		IPGlobal = 0.0;

		IP = 0.0;

		PP = 0.0;

		boolean banderaExclusiones = false;

		int numeroExclusiones = 0;

		ResultadoBusqueda resultadoBusqueda = new ResultadoBusqueda();
		resultadoBusqueda.setBusquedas(new ArrayList<>());

		if (universoPG_Progenitor != null && universoPG_Seguro != null && universoPG_Hijo != null) {

			inicializacionListaTresPerfiles(universoPG_Progenitor, universoPG_Seguro, universoPG_Hijo);

			List<Marcador> listaMarcadores = new ArrayList<>();

			for (EtiquetaPerfil etiquetaProgenitor : this.listEtiquetaProgenitor) {

				System.out.println(etiquetaProgenitor.getIdMarcador());

				idEtiquetaProgenitor = String.valueOf(etiquetaProgenitor.getPerfilGenetico());

				estado = etiquetaProgenitor.getEdoAbreviacion();

				EtiquetaPerfil etiquetaHijo = this.listEtiquetaHijo.stream()
						.filter(x -> x.getIdMarcador().equals(etiquetaProgenitor.getIdMarcador())).findAny()
						.orElse(null);

				EtiquetaPerfil etiquetaSeguro = this.listEtiquetaSeguro.stream()
						.filter(x -> x.getIdMarcador().equals(etiquetaProgenitor.getIdMarcador())).findAny()
						.orElse(null);

				if (etiquetaHijo != null && etiquetaSeguro != null) {

					idEtiquetaSeguro = String.valueOf(etiquetaSeguro.getPerfilGenetico());

					idEtiquetaHijo = String.valueOf(etiquetaHijo.getPerfilGenetico());

					if (etiquetaProgenitor.getIdMarcador() != 18) {

						IP = 0.0;

						ComparacionTres comparacionTres = comparacionTresFrecuencias(etiquetaProgenitor, etiquetaSeguro,
								etiquetaHijo);

						Marcador marcador = validacionFormulasTresPerfiles(comparacionTres);

						System.out.println("Formula -> " + marcador.getNumeroFormula());

						Objetivo objetivo = new Objetivo();
						objetivo.setAlelo1(comparacionTres.getAleloXHijo());
						objetivo.setAlelo2(comparacionTres.getAleloYHijo());

						PerfilPadre perfilPadre = new PerfilPadre();
						perfilPadre.setAlelo1(comparacionTres.getAleloXProgenitor());
						perfilPadre.setAlelo2(comparacionTres.getAleloYProgenitor());

						PerfilMadre perfilMadre = new PerfilMadre();
						perfilMadre.setAlelo1(comparacionTres.getAleloXSeguro());
						perfilMadre.setAlelo2(comparacionTres.getAleloYSeguro());

						if (marcador.getNumeroFormula() != null) { /* No existe compatibilidad en los alelos */

							if (marcador.getNumeroFormula() == 1) {
								AlelosFrecuencia alelosFrecuencia = obtenerAlelosFrecuencia(marcador.getA(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuencia != null) {
									a = alelosFrecuencia.getValor_alelo();
									IP = 1 / a;
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
								perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);

								perfilMadre.setCoincideAlelo1(resultadoTablaFrecuencia);
								perfilMadre.setCoincideAlelo2(resultadoTablaFrecuencia);

							} else if (marcador.getNumeroFormula() == 2) {

								AlelosFrecuencia alelosFrecuencia = obtenerAlelosFrecuencia(marcador.getA(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuencia != null) {
									a = alelosFrecuencia.getValor_alelo();
									IP = 1 / a;
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
								perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);

								perfilMadre.setCoincideAlelo1(2);
								perfilMadre.setCoincideAlelo2(2);

							} else if (marcador.getNumeroFormula() == 3) {

								AlelosFrecuencia alelosFrecuencia = obtenerAlelosFrecuencia(marcador.getA(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuencia != null) {
									a = alelosFrecuencia.getValor_alelo();
									IP = 1 / a;
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
								perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);

								if (perfilMadre.getAlelo1().equals(marcador.getA())) {
									perfilMadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilMadre.setCoincideAlelo2(2);
								} else {
									perfilMadre.setCoincideAlelo1(2);
									perfilMadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

							} else if (marcador.getNumeroFormula() == 4) {

								AlelosFrecuencia alelosFrecuenciaA = obtenerAlelosFrecuencia(marcador.getA(),
										etiquetaSeguro.getListaFrecuencias());

								AlelosFrecuencia alelosFrecuenciaB = obtenerAlelosFrecuencia(marcador.getB(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuenciaA != null && alelosFrecuenciaB != null) {
									a = alelosFrecuenciaA.getValor_alelo();
									b = alelosFrecuenciaB.getValor_alelo();
									IP = 1 / (a + b);
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
								perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);

								if (perfilMadre.getAlelo1().equals(marcador.getA())) {
									perfilMadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilMadre.setCoincideAlelo2(2);
								} else {
									perfilMadre.setCoincideAlelo1(2);
									perfilMadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

							} else if (marcador.getNumeroFormula() == 5) {

								AlelosFrecuencia alelosFrecuencia = obtenerAlelosFrecuencia(marcador.getA(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuencia != null) {
									a = alelosFrecuencia.getValor_alelo();
									IP = 1 / a;
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
								perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);

								perfilMadre.setCoincideAlelo1(2);
								perfilMadre.setCoincideAlelo2(2);

							} else if (marcador.getNumeroFormula() == 6) {

								AlelosFrecuencia alelosFrecuencia = obtenerAlelosFrecuencia(marcador.getA(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuencia != null) {
									a = alelosFrecuencia.getValor_alelo();
									IP = 1 / (2 * a);
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								if (perfilPadre.getAlelo1().equals(marcador.getA())) {
									perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilPadre.setCoincideAlelo2(2);
								} else {
									perfilPadre.setCoincideAlelo1(2);
									perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

								perfilMadre.setCoincideAlelo1(resultadoTablaFrecuencia);
								perfilMadre.setCoincideAlelo2(resultadoTablaFrecuencia);

							}

							else if (marcador.getNumeroFormula() == 7) {

								AlelosFrecuencia alelosFrecuencia = obtenerAlelosFrecuencia(marcador.getB(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuencia != null) {
									b = alelosFrecuencia.getValor_alelo();
									IP = 1 / (2 * b);
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								if (perfilPadre.getAlelo1().equals(marcador.getA())) {
									perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilPadre.setCoincideAlelo2(2);
								} else {
									perfilPadre.setCoincideAlelo1(2);
									perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

								perfilMadre.setCoincideAlelo1(resultadoTablaFrecuencia);
								perfilMadre.setCoincideAlelo2(resultadoTablaFrecuencia);

							} else if (marcador.getNumeroFormula() == 8) {

								AlelosFrecuencia alelosFrecuencia = obtenerAlelosFrecuencia(marcador.getA(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuencia != null) {
									a = alelosFrecuencia.getValor_alelo();
									IP = 1 / (2 * a);
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								/* Validacion de compatibilidad */

								if (perfilPadre.getAlelo1().equals(marcador.getA())) {
									perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilPadre.setCoincideAlelo2(2);
								} else {
									perfilPadre.setCoincideAlelo1(2);
									perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

								perfilMadre.setCoincideAlelo1(2);
								perfilMadre.setCoincideAlelo2(2);

							} else if (marcador.getNumeroFormula() == 9) {

								AlelosFrecuencia alelosFrecuencia = obtenerAlelosFrecuencia(marcador.getA(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuencia != null) {
									a = alelosFrecuencia.getValor_alelo();
									IP = 1 / (2 * a);
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								/* Validacion de compatibilidad */

								if (perfilPadre.getAlelo1().equals(marcador.getA())) {
									perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilPadre.setCoincideAlelo2(2);
								} else {
									perfilPadre.setCoincideAlelo1(2);
									perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

								if (perfilMadre.getAlelo1().equals(marcador.getA())) {
									perfilMadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilMadre.setCoincideAlelo2(2);
								} else {
									perfilMadre.setCoincideAlelo1(2);
									perfilMadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

							} else if (marcador.getNumeroFormula() == 10) {

								AlelosFrecuencia alelosFrecuenciaA = obtenerAlelosFrecuencia(marcador.getA(),
										etiquetaSeguro.getListaFrecuencias());

								AlelosFrecuencia alelosFrecuenciaB = obtenerAlelosFrecuencia(marcador.getB(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuenciaA != null && alelosFrecuenciaB != null) {
									a = alelosFrecuenciaA.getValor_alelo();
									b = alelosFrecuenciaB.getValor_alelo();
									IP = 1 / (a + b);
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								/* Validacion de compatibilidad */

								if (perfilPadre.getAlelo1().equals(marcador.getA())) {
									perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilPadre.setCoincideAlelo2(2);
								} else {
									perfilPadre.setCoincideAlelo1(2);
									perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

								if (perfilMadre.getAlelo1().equals(marcador.getA())) {
									perfilMadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilMadre.setCoincideAlelo2(2);
								} else {
									perfilMadre.setCoincideAlelo1(2);
									perfilMadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

							} else if (marcador.getNumeroFormula() == 11) {

								AlelosFrecuencia alelosFrecuencia = obtenerAlelosFrecuencia(marcador.getA(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuencia != null) {
									a = alelosFrecuencia.getValor_alelo();
									IP = 1 / (2 * a);
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								if (perfilPadre.getAlelo1().equals(marcador.getA())) {
									perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilPadre.setCoincideAlelo2(2);
								} else {
									perfilPadre.setCoincideAlelo1(2);
									perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

								if (perfilMadre.getAlelo1().equals(marcador.getA())) {
									perfilMadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilMadre.setCoincideAlelo2(2);
								} else {
									perfilMadre.setCoincideAlelo1(2);
									perfilMadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

							} else if (marcador.getNumeroFormula() == 12) {

								AlelosFrecuencia alelosFrecuenciaA = obtenerAlelosFrecuencia(marcador.getA(),
										etiquetaSeguro.getListaFrecuencias());

								AlelosFrecuencia alelosFrecuenciaC = obtenerAlelosFrecuencia(marcador.getC(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuenciaA != null && alelosFrecuenciaC != null) {
									a = alelosFrecuenciaA.getValor_alelo();
									c = alelosFrecuenciaC.getValor_alelo();
									IP = 1 / (2 * (a + c));
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								if (perfilPadre.getAlelo1().equals(marcador.getA())) {
									perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilPadre.setCoincideAlelo2(2);
								} else {
									perfilPadre.setCoincideAlelo1(2);
									perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

								if (perfilMadre.getAlelo1().equals(marcador.getA())) {
									perfilMadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilMadre.setCoincideAlelo2(2);
								} else {
									perfilMadre.setCoincideAlelo1(2);
									perfilMadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

							} else if (marcador.getNumeroFormula() == 13) {

								AlelosFrecuencia alelosFrecuenciaB = obtenerAlelosFrecuencia(marcador.getB(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuenciaB != null) {
									b = alelosFrecuenciaB.getValor_alelo();
									IP = 1 / (2 * b);
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								if (perfilPadre.getAlelo1().equals(marcador.getA())) {
									perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilPadre.setCoincideAlelo2(2);
								} else {
									perfilPadre.setCoincideAlelo1(2);
									perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

								if (perfilMadre.getAlelo1().equals(marcador.getA())) {
									perfilMadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilMadre.setCoincideAlelo2(2);
								} else {
									perfilMadre.setCoincideAlelo1(2);
									perfilMadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

							} else if (marcador.getNumeroFormula() == 14) {

								AlelosFrecuencia alelosFrecuenciaB = obtenerAlelosFrecuencia(marcador.getB(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuenciaB != null) {
									b = alelosFrecuenciaB.getValor_alelo();
									IP = 1 / (2 * b);
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								if (perfilPadre.getAlelo1().equals(marcador.getA())) {
									perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilPadre.setCoincideAlelo2(2);
								} else {
									perfilPadre.setCoincideAlelo1(2);
									perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

								if (perfilMadre.getAlelo1().equals(marcador.getA())) {
									perfilMadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilMadre.setCoincideAlelo2(2);
								} else {
									perfilMadre.setCoincideAlelo1(2);
									perfilMadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

							} else if (marcador.getNumeroFormula() == 15) {

								AlelosFrecuencia alelosFrecuenciaA = obtenerAlelosFrecuencia(marcador.getA(),
										etiquetaSeguro.getListaFrecuencias());

								int resultadoTablaFrecuencia = 0;
								if (alelosFrecuenciaA != null) {
									a = alelosFrecuenciaA.getValor_alelo();
									IP = 1 / (2 * a);
									resultadoTablaFrecuencia = 1;
								} else {
									resultadoTablaFrecuencia = 3;
								}

								if (perfilPadre.getAlelo1().equals(marcador.getA())) {
									perfilPadre.setCoincideAlelo1(resultadoTablaFrecuencia);
									perfilPadre.setCoincideAlelo2(2);
								} else {
									perfilPadre.setCoincideAlelo1(2);
									perfilPadre.setCoincideAlelo2(resultadoTablaFrecuencia);
								}

								perfilMadre.setCoincideAlelo1(2);
								perfilMadre.setCoincideAlelo2(2);
							}

						} else {
							System.out.println("-- EXCLUSION --" + etiquetaProgenitor.getIdMarcador() + " -- ");
							perfilPadre.setCoincideAlelo1(0);
							perfilPadre.setCoincideAlelo2(0);

							perfilMadre.setCoincideAlelo1(0);
							perfilMadre.setCoincideAlelo2(0);
							numeroExclusiones++;
							banderaExclusiones = true;
							IPGlobal = 0.0;
						}

						marcador.setMarcador(etiquetaSeguro.getNombreMarcador());
						marcador.setIdMarcador(etiquetaSeguro.getIdMarcador());
						marcador.setObjetivo(objetivo);
						marcador.setPerfilPadre(perfilPadre);
						marcador.setPerfilMadre(perfilMadre);

						listaMarcadores.add(marcador);

						if (banderaExclusiones == false) {
							if (IPGlobal != 0.0) {
								if (IP != 0.0) {
									IPGlobal = IPGlobal * IP;
								}
							} else {
								IPGlobal = IP;
							}
						} else {
							IPGlobal = 0.0;
						}
					}
				}
			}

			List<Busqueda> listaBusquedas = new ArrayList<>();
			if (numeroExclusiones > busquedaServicio.getNumeroExclusiones()) {
				listaBusquedas = new ArrayList<>();
			} else {

				if (IPGlobal != 0.0) {
					PP = (IPGlobal / (IPGlobal + 1)) * 100;
				}

				Resultado resultado = new Resultado();
				resultado.setPerfilPadre(idEtiquetaProgenitor);
				resultado.setPerfilMadre(idEtiquetaSeguro);
				resultado.setPerfilObjetivo(idEtiquetaHijo);
				resultado.setEstado(estado);
				resultado.setAmel("AMEL");
				if (IPGlobal != 0.0) {
					resultado.setIp(String.format("%6.2E", IPGlobal));
					resultado.setPp(String.format("%.8f", PP) + "%");
				} else {
					resultado.setIp("0");
					resultado.setPp("0");
				}
				Busqueda busqueda = new Busqueda();
				busqueda.setResultado(resultado);
				busqueda.setComparativoMarcadores(listaMarcadores);
				listaBusquedas = new ArrayList<>();
				listaBusquedas.add(busqueda);
				resultadoBusqueda.setBusquedas(listaBusquedas);
			}
		}

		return resultadoBusqueda;
	}

	private AlelosFrecuencia obtenerAlelosFrecuencia(String valorAlelo, List<AlelosFrecuencia> listaFrecuencias) {
		AlelosFrecuencia alelosFrecuencia = listaFrecuencias.stream()
				.filter(x -> x.getCat_ale_descripcion().equals(valorAlelo)).findAny().orElse(null);
		return alelosFrecuencia;
	}

	private Marcador validacionFormulasTresPerfiles(ComparacionTres comparacionTres) {

		Marcador marcador = new Marcador();

		try {

			/* Genotipo presunto progenitor */

			if (comparacionTres.getValorXProgenitor().equals("A")
					&& comparacionTres.getValorYProgenitor().equals("A")) {

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(1);
						marcador.setA(comparacionTres.getAleloXHijo());
					}
				}

				/* 2 */

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						marcador.setNumeroFormula(2);
						marcador.setA(comparacionTres.getAleloXHijo());
					}
				}

				if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						marcador.setNumeroFormula(2);
						marcador.setA(comparacionTres.getAleloYHijo());
					}
				}

				/* 3 */

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						marcador.setNumeroFormula(3);
						marcador.setA(comparacionTres.getAleloXHijo());
					}
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(3);
						marcador.setA(comparacionTres.getAleloXHijo());
					}
				}

				/* 4 */

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						marcador.setNumeroFormula(4);
						marcador.setA(comparacionTres.getAleloXHijo());
						marcador.setB(comparacionTres.getAleloYHijo());
					}
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(4);
						marcador.setA(comparacionTres.getAleloXHijo());
						marcador.setB(comparacionTres.getAleloYHijo());
					}
				}
				if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						marcador.setNumeroFormula(4);
						marcador.setA(comparacionTres.getAleloYHijo());
						marcador.setB(comparacionTres.getAleloXHijo());
					}
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(4);
						marcador.setA(comparacionTres.getAleloYHijo());
						marcador.setB(comparacionTres.getAleloXHijo());
					}
				}
				/* 5 */
				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						marcador.setNumeroFormula(5);
						marcador.setA(comparacionTres.getAleloXHijo());
					}
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						marcador.setNumeroFormula(5);
						marcador.setA(comparacionTres.getAleloXHijo());
					}
				}
				if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						marcador.setNumeroFormula(5);
						marcador.setA(comparacionTres.getAleloYHijo());
					}
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						marcador.setNumeroFormula(5);
						marcador.setA(comparacionTres.getAleloYHijo());
					}
				}
			}

			/* A B */

			if (comparacionTres.getValorXProgenitor().equals("A") && comparacionTres.getValorYProgenitor().equals("B")
					|| comparacionTres.getValorXProgenitor().equals("B")
							&& comparacionTres.getValorYProgenitor().equals("A")) {

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(6);
						marcador.setA(comparacionTres.getAleloXHijo());
					}
				}

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(7);
						marcador.setB(comparacionTres.getAleloYHijo());
					}
				}

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("C")) {
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						marcador.setNumeroFormula(8);
						marcador.setA(comparacionTres.getAleloXHijo());
					}
				}

				/* 8 */
				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("C")) {
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						marcador.setNumeroFormula(8);
						marcador.setA(comparacionTres.getAleloXHijo());
					}
				}
				if (comparacionTres.getValorXHijo().equals("C") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						marcador.setNumeroFormula(8);
						marcador.setA(comparacionTres.getAleloYHijo());
					}
				}

				/* 9 */

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						marcador.setNumeroFormula(9);
						marcador.setA(comparacionTres.getAleloXHijo());
					}
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(9);
						marcador.setA(comparacionTres.getAleloYHijo());
					}
				}

				/* 10 */

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						marcador.setNumeroFormula(10);
						marcador.setA(comparacionTres.getAleloXHijo());
						marcador.setB(comparacionTres.getAleloYHijo());
					}
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(10);
						marcador.setA(comparacionTres.getAleloXHijo());
						marcador.setB(comparacionTres.getAleloYHijo());
					}
				}

				if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						marcador.setNumeroFormula(10);
						marcador.setA(comparacionTres.getAleloYHijo());
						marcador.setB(comparacionTres.getAleloXHijo());
					}
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(10);
						marcador.setA(comparacionTres.getAleloYHijo());
						marcador.setB(comparacionTres.getAleloXHijo());
					}
				}

				/* Formula 11 */

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						marcador.setNumeroFormula(11);
						marcador.setA(comparacionTres.getAleloXHijo());
					}
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(11);
						marcador.setA(comparacionTres.getAleloYHijo());
					}
				}

				/* Formula 12 */

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("C")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						marcador.setNumeroFormula(12);
						marcador.setA(comparacionTres.getAleloXHijo());
						marcador.setC(comparacionTres.getAleloYHijo());
					}
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(12);
						marcador.setA(comparacionTres.getAleloXHijo());
						marcador.setC(comparacionTres.getAleloYHijo());
					}
				}

				if (comparacionTres.getValorXHijo().equals("C") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						marcador.setNumeroFormula(12);
						marcador.setA(comparacionTres.getAleloYHijo());
						marcador.setC(comparacionTres.getAleloXHijo());
					}
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(12);
						marcador.setA(comparacionTres.getAleloYHijo());
						marcador.setC(comparacionTres.getAleloXHijo());
					}
				}

				/* Formula 13 */

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						marcador.setNumeroFormula(13);
						marcador.setB(comparacionTres.getAleloYHijo());
					} else if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(13);
						marcador.setB(comparacionTres.getAleloYHijo());
					}
				}
				if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						marcador.setNumeroFormula(13);
						marcador.setB(comparacionTres.getAleloXHijo());
					} else if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(13);
						marcador.setB(comparacionTres.getAleloXHijo());
					}
				}

				/* Formula 14 */

				if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("C")) {
					if (comparacionTres.getValorXSeguro().equals("A") && comparacionTres.getValorYSeguro().equals("C")
							|| comparacionTres.getValorXSeguro().equals("C")
									&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(14);
						marcador.setB(comparacionTres.getAleloXHijo());
					}
				} else if (comparacionTres.getValorXHijo().equals("C") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("A") && comparacionTres.getValorYSeguro().equals("C")
							|| comparacionTres.getValorXSeguro().equals("C")
									&& comparacionTres.getValorYSeguro().equals("A")) {
						marcador.setNumeroFormula(14);
						marcador.setB(comparacionTres.getAleloYHijo());
					}
				}

				/* Formula 15 */

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("C")) {
					if (comparacionTres.getValorXSeguro().equals("C") && comparacionTres.getValorYSeguro().equals("D")
							|| comparacionTres.getValorXSeguro().equals("D")
									&& comparacionTres.getValorYSeguro().equals("C")) {
						marcador.setNumeroFormula(15);
						marcador.setA(comparacionTres.getAleloXHijo());
					}
				} else if (comparacionTres.getValorXHijo().equals("C") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("C") && comparacionTres.getValorYSeguro().equals("D")
							|| comparacionTres.getValorXSeguro().equals("D")
									&& comparacionTres.getValorYSeguro().equals("C")) {
						marcador.setNumeroFormula(15);
						marcador.setA(comparacionTres.getAleloYHijo());
					}
				}
			}

			return marcador;
		} catch (Exception e) {
			return new Marcador();
		}
	}

	private ComparacionTres comparacionTresFrecuencias(EtiquetaPerfil etiquetaProgenitor, EtiquetaPerfil etiquetaSeguro,
			EtiquetaPerfil etiquetaHijo) {

		try {

			ComparacionTres comparacionTres = new ComparacionTres();

			comparacionTres.setAleloXProgenitor(etiquetaProgenitor.getAlelo_X());
			comparacionTres.setValorXProgenitor("A");

			comparacionTres.setAleloYProgenitor(etiquetaProgenitor.getAlelo_Y());
			comparacionTres.setValorYProgenitor("B");

			if (etiquetaProgenitor.getAlelo_Y().equals(etiquetaProgenitor.getAlelo_X())) {
				comparacionTres.setAleloYProgenitor(etiquetaProgenitor.getAlelo_Y());
				comparacionTres.setValorYProgenitor("A");
			}

			/* Asignacion Progenitor x */

			if (comparacionTres.getAleloXProgenitor().equals(etiquetaHijo.getAlelo_X())) {
				comparacionTres.setAleloXHijo(etiquetaHijo.getAlelo_X());
				comparacionTres.setValorXHijo(comparacionTres.getValorXProgenitor());
			}

			if (comparacionTres.getAleloXProgenitor().equals(etiquetaSeguro.getAlelo_X())) {
				comparacionTres.setAleloXSeguro(etiquetaSeguro.getAlelo_X());
				comparacionTres.setValorXSeguro(comparacionTres.getValorXProgenitor());
			}

			if (comparacionTres.getAleloYProgenitor().equals(etiquetaHijo.getAlelo_X())) {
				comparacionTres.setAleloXHijo(etiquetaHijo.getAlelo_X());
				comparacionTres.setValorXHijo(comparacionTres.getValorYProgenitor());
			}

			if (comparacionTres.getAleloYProgenitor().equals(etiquetaSeguro.getAlelo_X())) {
				comparacionTres.setAleloXSeguro(etiquetaSeguro.getAlelo_X());
				comparacionTres.setValorXSeguro(comparacionTres.getValorYProgenitor());
			}

			/* Asignacion Progenitor Y */

			if (comparacionTres.getAleloYProgenitor().equals(etiquetaHijo.getAlelo_Y())) {
				comparacionTres.setAleloYHijo(etiquetaHijo.getAlelo_Y());
				comparacionTres.setValorYHijo(comparacionTres.getValorYProgenitor());
			}

			if (comparacionTres.getAleloYProgenitor().equals(etiquetaSeguro.getAlelo_Y())) {
				comparacionTres.setAleloYSeguro(etiquetaSeguro.getAlelo_Y());
				comparacionTres.setValorYSeguro(comparacionTres.getValorYProgenitor());
			}

			if (comparacionTres.getAleloXProgenitor().equals(etiquetaHijo.getAlelo_Y())) {
				comparacionTres.setAleloYHijo(etiquetaHijo.getAlelo_Y());
				comparacionTres.setValorYHijo(comparacionTres.getValorXProgenitor());
			}

			if (comparacionTres.getAleloXProgenitor().equals(etiquetaSeguro.getAlelo_Y())) {
				comparacionTres.setAleloYSeguro(etiquetaSeguro.getAlelo_Y());
				comparacionTres.setValorYSeguro(comparacionTres.getValorXProgenitor());
			}

			if (comparacionTres.getAleloXHijo() != null
					&& comparacionTres.getAleloXHijo().equals(etiquetaSeguro.getAlelo_X())) {
				comparacionTres.setAleloXSeguro(etiquetaSeguro.getAlelo_X());
				comparacionTres.setValorXSeguro(comparacionTres.getValorXHijo());
			}

			if (comparacionTres.getAleloXHijo() != null
					&& comparacionTres.getAleloXHijo().equals(etiquetaSeguro.getAlelo_Y())) {
				comparacionTres.setAleloYSeguro(etiquetaSeguro.getAlelo_Y());
				comparacionTres.setValorYSeguro(comparacionTres.getValorXHijo());
			}

			if (comparacionTres.getAleloYHijo() != null
					&& comparacionTres.getAleloYHijo().equals(etiquetaSeguro.getAlelo_X())) {
				comparacionTres.setAleloXSeguro(etiquetaSeguro.getAlelo_X());
				comparacionTres.setValorXSeguro(comparacionTres.getValorYHijo());
			}

			if (comparacionTres.getAleloYHijo() != null
					&& comparacionTres.getAleloYHijo().equals(etiquetaSeguro.getAlelo_Y())) {
				comparacionTres.setAleloYSeguro(etiquetaSeguro.getAlelo_Y());
				comparacionTres.setValorYSeguro(comparacionTres.getValorYHijo());
			}

			/* Validacion para el alelo hijo */

			if (comparacionTres.getAleloXSeguro() == null) {
				comparacionTres.setAleloXSeguro(etiquetaSeguro.getAlelo_X());
			}
			if (comparacionTres.getAleloYSeguro() == null) {
				comparacionTres.setAleloYSeguro(etiquetaSeguro.getAlelo_Y());
			}

			if (comparacionTres.getValorXHijo() == null) {
				comparacionTres.setAleloXHijo(etiquetaHijo.getAlelo_X());
				comparacionTres.setValorXHijo("C");
			}
			if (comparacionTres.getValorYHijo() == null) {
				comparacionTres.setAleloYHijo(etiquetaHijo.getAlelo_Y());
				comparacionTres.setValorYHijo("C");
			}

			if (comparacionTres.getAleloXHijo().equals(comparacionTres.getAleloXSeguro())) {
				comparacionTres.setValorXSeguro(comparacionTres.getValorXHijo());
			}

			if (comparacionTres.getAleloXHijo().equals(comparacionTres.getAleloYSeguro())) {
				comparacionTres.setValorYSeguro(comparacionTres.getValorXHijo());
			}

			if (comparacionTres.getAleloYHijo().equals(comparacionTres.getAleloXSeguro())) {
				comparacionTres.setValorXSeguro(comparacionTres.getValorYHijo());
			}

			if (comparacionTres.getAleloYHijo().equals(comparacionTres.getAleloYSeguro())) {
				comparacionTres.setValorYSeguro(comparacionTres.getValorYHijo());
			}

			/* Validacion seguro */
			if (comparacionTres.getValorXSeguro() == null) {
				comparacionTres.setValorXSeguro("D");
			}
			if (comparacionTres.getValorYSeguro() == null) {
				comparacionTres.setValorYSeguro("D");
			}

			/* Validacion AA */

			if (comparacionTres.getValorXProgenitor().equals("A")
					&& comparacionTres.getValorYProgenitor().equals("A")) {

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("C")) {
					if (comparacionTres.getAleloYHijo().equals(comparacionTres.getAleloXSeguro())) {
						comparacionTres.setValorXSeguro("B");
						comparacionTres.setValorYHijo("B");
					}
					if (comparacionTres.getAleloYHijo().equals(comparacionTres.getAleloYSeguro())) {
						comparacionTres.setValorYSeguro("B");
						comparacionTres.setValorYHijo("B");
					}
				}
				if (comparacionTres.getValorXHijo().equals("C") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getAleloXHijo().equals(comparacionTres.getAleloXSeguro())) {
						comparacionTres.setValorXSeguro("B");
						comparacionTres.setValorXHijo("B");
					}
					if (comparacionTres.getAleloXHijo().equals(comparacionTres.getAleloYSeguro())) {
						comparacionTres.setValorYSeguro("B");
						comparacionTres.setValorXHijo("B");
					}
				}

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("A")) {
					if (!comparacionTres.getAleloXHijo().equals(comparacionTres.getAleloXSeguro())) {
						comparacionTres.setValorXSeguro("B");
					}
					if (!comparacionTres.getAleloXHijo().equals(comparacionTres.getAleloYSeguro())) {
						comparacionTres.setValorYSeguro("B");
					}
				}
			}

			if (comparacionTres.getValorXSeguro().equals("A") && comparacionTres.getValorYSeguro().equals("D")) {
				comparacionTres.setValorYSeguro("C");
			} else if (comparacionTres.getValorXSeguro().equals("D") && comparacionTres.getValorYSeguro().equals("A")) {
				comparacionTres.setValorXSeguro("C");
			} else if (comparacionTres.getValorXSeguro().equals("B") && comparacionTres.getValorYSeguro().equals("D")) {
				comparacionTres.setValorYSeguro("C");
			} else if (comparacionTres.getValorXSeguro().equals("D") && comparacionTres.getValorYSeguro().equals("B")) {
				comparacionTres.setValorXSeguro("C");
			}

			/* 6 */

			if (comparacionTres.getValorXProgenitor().equals("A")
					&& comparacionTres.getValorYProgenitor().equals("B")) {
				if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("B")) {

						if (comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorXHijo())
								&& comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYHijo())
								&& comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorXSeguro())
								&& comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYSeguro())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorXSeguro("A");
							comparacionTres.setValorYSeguro("A");
							comparacionTres.setValorYHijo("A");
							comparacionTres.setValorXHijo("A");

						}
					}
				}
			}
			/* 7 */

			if (comparacionTres.getValorXProgenitor().equals("A")
					&& comparacionTres.getValorYProgenitor().equals("B")) {
				if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("B")) {

						if (comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorXHijo())
								&& comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorXSeguro())
								&& comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYSeguro())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorXSeguro("A");
							comparacionTres.setValorYSeguro("A");

							comparacionTres.setValorYHijo("B");
							comparacionTres.setValorXHijo("A");

						}
					}

				}

			}

			/* 8 */

			if (comparacionTres.getValorXProgenitor().equals("A")
					&& comparacionTres.getValorYProgenitor().equals("B")) {
				if (comparacionTres.getValorXHijo().equals("C") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						if (comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYHijo())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorYHijo("A");
						}
					}
				}
				if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("C")) {
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						if (comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorXHijo())) {
							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorXHijo("A");
						}
					}
				}
			}

			/* 9 */

			if (comparacionTres.getValorXProgenitor().equals("A")
					&& comparacionTres.getValorYProgenitor().equals("B")) {
				if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("A")
							&& comparacionTres.getValorYSeguro().equals("B")) {

						if (comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYHijo())
								&& comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYSeguro())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorYSeguro("A");
							comparacionTres.setValorXSeguro("B");
							comparacionTres.setValorXHijo("A");
							comparacionTres.setValorYHijo("A");
						}
					}

					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("A")) {
						if (comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYHijo())
								&& comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorXSeguro())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorYSeguro("B");
							comparacionTres.setValorXSeguro("A");
							comparacionTres.setValorXHijo("A");
							comparacionTres.setValorYHijo("A");
						}
					}
				}
			}

			/* 11 */

			if (comparacionTres.getValorXProgenitor().equals("A")
					&& comparacionTres.getValorYProgenitor().equals("B")) {
				if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("B")) {

						if (comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYHijo())
								&& comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYSeguro())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorYSeguro("A");
							comparacionTres.setValorXHijo("A");
							comparacionTres.setValorYHijo("A");

						}

					}
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						if (comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYHijo())
								&& comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorXSeguro())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorXSeguro("A");
							comparacionTres.setValorXHijo("A");
							comparacionTres.setValorYHijo("A");

						}
					}
				}

			}

			/* 12 */
			if (comparacionTres.getValorXProgenitor().equals("A")
					&& comparacionTres.getValorYProgenitor().equals("B")) {
				if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("C")) {
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						if (comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorXHijo())
								&& comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYSeguro())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorYSeguro("A");
							comparacionTres.setValorXHijo("A");
						}
					}

					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						if (comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorXHijo())
								&& comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorXSeguro())) {
							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorXSeguro("A");
							comparacionTres.setValorXHijo("A");
						}
					}
				}

				if (comparacionTres.getValorXHijo().equals("C") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						if (comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYHijo())
								&& comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYSeguro())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorYSeguro("A");
							comparacionTres.setValorYHijo("A");
						}
					}

					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						if (comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorYHijo())
								&& comparacionTres.getValorYProgenitor().equals(comparacionTres.getValorXSeguro())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorXSeguro("A");
							comparacionTres.setValorYHijo("A");
						}
					}
				}
			}

			/* 13 */

			if (comparacionTres.getValorXProgenitor().equals("A")
					&& comparacionTres.getValorYProgenitor().equals("B")) {
				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						comparacionTres.setValorYProgenitor("A");
						comparacionTres.setValorXProgenitor("B");
						comparacionTres.setValorYSeguro("A");
						comparacionTres.setValorYHijo("A");
						comparacionTres.setValorXHijo("B");
					}
					if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						comparacionTres.setValorYProgenitor("A");
						comparacionTres.setValorXProgenitor("B");
						comparacionTres.setValorXSeguro("A");
						comparacionTres.setValorYHijo("A");
						comparacionTres.setValorXHijo("B");
					}
				} else if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						comparacionTres.setValorYProgenitor("A");
						comparacionTres.setValorXProgenitor("B");
						comparacionTres.setValorYSeguro("A");
						comparacionTres.setValorYHijo("B");
						comparacionTres.setValorXHijo("A");
					} else if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						comparacionTres.setValorYProgenitor("A");
						comparacionTres.setValorXProgenitor("B");
						comparacionTres.setValorXSeguro("A");
						comparacionTres.setValorYHijo("B");
						comparacionTres.setValorXHijo("A");
					}
				}
			}

			/* 14 */

			if (comparacionTres.getValorXProgenitor().equals("A")
					&& comparacionTres.getValorYProgenitor().equals("B")) {
				if (comparacionTres.getValorXHijo().equals("C") && comparacionTres.getValorYHijo().equals("A")) {
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						if (comparacionTres.getAleloYProgenitor().equals(comparacionTres.getAleloYSeguro())
								&& comparacionTres.getAleloXProgenitor().equals(comparacionTres.getAleloYHijo())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorYSeguro("A");
							comparacionTres.setValorYHijo("B");
						}
					} else if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						if (comparacionTres.getAleloYProgenitor().equals(comparacionTres.getAleloXSeguro())
								&& comparacionTres.getAleloXProgenitor().equals(comparacionTres.getAleloYHijo())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorXSeguro("A");
							comparacionTres.setValorYHijo("B");
						}
					}
				}

				if (comparacionTres.getValorXHijo().equals("A") && comparacionTres.getValorYHijo().equals("C")) {
					if (comparacionTres.getValorXSeguro().equals("C")
							&& comparacionTres.getValorYSeguro().equals("B")) {
						if (comparacionTres.getAleloYProgenitor().equals(comparacionTres.getAleloYSeguro())
								&& comparacionTres.getAleloXProgenitor().equals(comparacionTres.getAleloYHijo())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorYSeguro("A");
							comparacionTres.setValorYHijo("B");

						} else if (comparacionTres.getAleloYProgenitor().equals(comparacionTres.getAleloYSeguro())
								&& comparacionTres.getAleloXProgenitor().equals(comparacionTres.getAleloXHijo())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorYSeguro("A");
							comparacionTres.setValorXHijo("B");
						}
					} else if (comparacionTres.getValorXSeguro().equals("B")
							&& comparacionTres.getValorYSeguro().equals("C")) {
						if (comparacionTres.getAleloYProgenitor().equals(comparacionTres.getAleloXSeguro())
								&& comparacionTres.getAleloXProgenitor().equals(comparacionTres.getAleloXHijo())) {

							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorXSeguro("A");
							comparacionTres.setValorXHijo("B");
						}
					}
				}
			}

			/* 15 */

			if (comparacionTres.getValorXProgenitor().equals("A")
					&& comparacionTres.getValorYProgenitor().equals("B")) {
				if (comparacionTres.getValorXHijo().equals("C") && comparacionTres.getValorYHijo().equals("B")) {
					if (comparacionTres.getValorXSeguro().equals("C") && comparacionTres.getValorYSeguro().equals("D")
							|| comparacionTres.getValorXSeguro().equals("D")
									&& comparacionTres.getValorYSeguro().equals("C")) {
						if (comparacionTres.getAleloYProgenitor().equals(comparacionTres.getAleloYHijo())) {
							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorYHijo("A");
						}
					}
				} else if (comparacionTres.getValorXHijo().equals("B") && comparacionTres.getValorYHijo().equals("C")) {
					if (comparacionTres.getValorXSeguro().equals("C") && comparacionTres.getValorYSeguro().equals("D")
							|| comparacionTres.getValorXSeguro().equals("D")
									&& comparacionTres.getValorYSeguro().equals("C")) {
						if (comparacionTres.getAleloYProgenitor().equals(comparacionTres.getAleloXHijo())) {
							comparacionTres.setValorYProgenitor("A");
							comparacionTres.setValorXProgenitor("B");
							comparacionTres.setValorXHijo("A");
						}
					}
				}
			}

			System.out.println("" + comparacionTres.getAleloXProgenitor() + "->" + comparacionTres.getValorXProgenitor()
					+ ", " + comparacionTres.getAleloYProgenitor() + "->" + comparacionTres.getValorYProgenitor());

			System.out.println("" + comparacionTres.getAleloXSeguro() + "->" + comparacionTres.getValorXSeguro() + ", "
					+ comparacionTres.getAleloYSeguro() + "->" + comparacionTres.getValorYSeguro());

			System.out.println("" + comparacionTres.getAleloXHijo() + "->" + comparacionTres.getValorXHijo() + ", "
					+ comparacionTres.getAleloYHijo() + "->" + comparacionTres.getValorYHijo());

			System.out.println("---------------------------------------------------------------------");

			return comparacionTres;

		} catch (Exception e) {

			ComparacionTres comparacionTres = new ComparacionTres();
			comparacionTres.setAleloXProgenitor(etiquetaProgenitor.getAlelo_X());
			comparacionTres.setAleloYProgenitor(etiquetaProgenitor.getAlelo_Y());

			comparacionTres.setAleloXHijo(etiquetaHijo.getAlelo_X());
			comparacionTres.setAleloYHijo(etiquetaHijo.getAlelo_Y());

			comparacionTres.setAleloXSeguro(etiquetaSeguro.getAlelo_X());
			comparacionTres.setAleloYSeguro(etiquetaSeguro.getAlelo_Y());

			return comparacionTres;
		}
	}

	private void inicializacionListaTresPerfiles(UniversoPerfilGenetico universoPG_Progenitor,
			UniversoPerfilGenetico universoPG_Seguro, UniversoPerfilGenetico universoPG_Hijo) {
		matrizEtiquetaProgenitor = universoPG_Progenitor.getListaPerfilesAgrupado();
		matrizEtiquetaSeguro = universoPG_Seguro.getListaPerfilesAgrupado();
		matrizEtiquetaHijo = universoPG_Hijo.getListaPerfilesAgrupado();
		this.listEtiquetaProgenitor = matrizEtiquetaProgenitor.get(0);
		this.listEtiquetaSeguro = matrizEtiquetaSeguro.get(0);
		this.listEtiquetaHijo = matrizEtiquetaHijo.get(0);
	}

	private void buscarMatrizOrigen(List<EtiquetaPerfil> listEtiquetaPerfilOrigen, int decrementoOrigen) {

		int porsicion = matrizEtiquetaProgenitor.size() - 1;
		buscarMatrizProgenitor(listEtiquetaPerfilOrigen, matrizEtiquetaProgenitor.get(porsicion), porsicion);

		int vDecrementoOrigen = decrementoOrigen - 1;
		if (vDecrementoOrigen >= 0) {
			buscarMatrizOrigen(matrizEtiquetaOrigen.get(vDecrementoOrigen), vDecrementoOrigen);
		}
	}

	private void buscarMatrizProgenitor(List<EtiquetaPerfil> listEtiquetaPerfilOrigen,
			List<EtiquetaPerfil> listEtiquetaPerfilProgenitor, int decremento) {

		int porsicionEtiqueta = listEtiquetaPerfilOrigen.size() - 1;
		buscarEtiquetaPerfilOrigen(listEtiquetaPerfilOrigen.get(porsicionEtiqueta), listEtiquetaPerfilOrigen,
				listEtiquetaPerfilProgenitor, porsicionEtiqueta);

		contadorCompatibilidad = 0;

		if (perfilGeneticoValido) {
			if (listMarcador.size() >= numeroMarcadores_Validar) {
				if (listaBusqueda.size() < maximoBusquedasVista) {

					/* Obtener Indice de Parentesto */

					IP = 0.0;
					IPGlobal = 0.0;
					PP = 0.0;

					int porsicionMarcador = this.listMarcador.size() - 1;
					iterarListaMarcadores(this.listMarcador.get(porsicionMarcador), porsicionMarcador);

					/*
					 * for (Marcador marcador : listMarcador) if (marcador.getIdMarcador() != 18) {
					 * // es el marcador AMEL } }
					 */

					if (IPGlobal != 0.0) {
						PP = (IPGlobal / (IPGlobal + 1)) * 100;
					}

					Resultado resultado = new Resultado();
					resultado.setGenotipoCompatible(identificadorOrigen);
					resultado.setGenotipoObjetivo(identificadorProgenitor);
					resultado.setEstado(estado);
					resultado.setAmel("AMEL");
					resultado.setIp(String.format("%6.2E", IPGlobal));
					resultado.setPp(String.format("%.8f", PP) + "%");

					Busqueda busqueda = new Busqueda();
					busqueda.setResultado(resultado);
					busqueda.setComparativoMarcadores(listMarcador);

					listaBusqueda.add(busqueda);

				} else {
					maximoBusquedas = true;
				}
			}
		}

		listMarcador = new ArrayList<Marcador>();

		/* Cambio de id de progenitor */

		if (!maximoBusquedas) {
			int vDecremento = decremento - 1;
			if (vDecremento >= 0) {
				buscarMatrizProgenitor(listEtiquetaPerfilOrigen, matrizEtiquetaProgenitor.get(vDecremento),
						vDecremento);
			}
		}
	}

	private void iterarListaMarcadores(Marcador marcador, int decremento) {

		if (marcador.getIdMarcador() != 18) { // es el marcador AMEL

			if (marcador.getNumeroFormula() != null) { // exclusion

				this.aleloFrecuencia = null;
				this.alelosFrecuenciaB = null;

				listaFrecuencias = marcador.getListaFrecuencias();
				int porsicion = listaFrecuencias.size() - 1;
				if (porsicion >= 0)
					buscarListaFrecuencia(listaFrecuencias.get(porsicion), marcador.getA(), porsicion);

				if (this.aleloFrecuencia != null && this.aleloFrecuencia.getValor_alelo() != null) {

					if (marcador.getNumeroFormula() == 1) {
						a = this.aleloFrecuencia.getValor_alelo();
						IP = 1 / a;

					} else if (marcador.getNumeroFormula() == 2) {
						a = this.aleloFrecuencia.getValor_alelo();
						IP = 1 / (2 * a);

					} else if (marcador.getNumeroFormula() == 3) {
						a = this.aleloFrecuencia.getValor_alelo();
						IP = 1 / (2 * a);

					} else if (marcador.getNumeroFormula() == 4) {

						listaFrecuenciasB = marcador.getListaFrecuencias();
						int porsicionB = listaFrecuenciasB.size() - 1;
						buscarListaFrecuenciaB(listaFrecuenciasB.get(porsicionB), marcador.getB(), porsicionB);

						a = this.aleloFrecuencia.getValor_alelo();

						if (this.alelosFrecuenciaB != null && this.alelosFrecuenciaB.getValor_alelo() != null) {
							b = this.alelosFrecuenciaB.getValor_alelo();
							IP = (a + b) / (4 * (a * b));
						} else {
							// No existe valor en la tabla de frecuencia
							if (marcador.getCompatible().getCoincideAlelo2() == 1)
								marcador.getCompatible().setCoincideAlelo2(3);
						}

					} else if (marcador.getNumeroFormula() == 5) {
						a = this.aleloFrecuencia.getValor_alelo();
						IP = 1 / (4 * a);
					}

					if (IPGlobal != 0.0) {
						if (IP != 0.0) {
							IPGlobal = IPGlobal * IP;
						}
					} else {
						IPGlobal = IP;
					}
				} else {
					// No existe valor en la tabla de frecuencia
					marcador = validarAleloExistenteTablaFrecuencia(marcador);
				}
			} else {
				IP = 0.0;
				IPGlobal = 0.0;
			}
		}

		int vDecremento = decremento - 1;
		if (vDecremento >= 0) {
			iterarListaMarcadores(this.listMarcador.get(vDecremento), vDecremento);
		}
	}

	private Marcador validarAleloExistenteTablaFrecuencia(Marcador marcador) {
		if (marcador.getCompatible().getCoincideAlelo1() == 1)
			marcador.getCompatible().setCoincideAlelo1(3);// No existe valor en la tabla de frecuencia
		if (marcador.getCompatible().getCoincideAlelo2() == 1)
			marcador.getCompatible().setCoincideAlelo2(3);// No existe valor en la tabla de frecuencia
		return marcador;
	}

	private void buscarListaFrecuencia(AlelosFrecuencia aleloFrecuencia, String marcadorA, int decremento) {

		if (!aleloFrecuencia.getCat_ale_descripcion().equals(marcadorA)) {
			int vDecremento = decremento - 1;
			if (vDecremento >= 0) {
				buscarListaFrecuencia(this.listaFrecuencias.get(vDecremento), marcadorA, vDecremento);
			}
		} else {
			this.aleloFrecuencia = aleloFrecuencia;
		}
	}

	private void buscarListaFrecuenciaB(AlelosFrecuencia aleloFrecuenciaB, String marcadorB, int decremento) {

		if (!aleloFrecuenciaB.getCat_ale_descripcion().equals(marcadorB)) {
			int vDecremento = decremento - 1;
			if (vDecremento >= 0) {
				buscarListaFrecuenciaB(this.listaFrecuenciasB.get(vDecremento), marcadorB, vDecremento);
			}
		} else {
			this.alelosFrecuenciaB = aleloFrecuenciaB;
		}
	}

	private void buscarEtiquetaPerfilOrigen(EtiquetaPerfil etiquetaPerfilOrigen,
			List<EtiquetaPerfil> listEtiquetaPerfilOrigen, List<EtiquetaPerfil> listEtiquetaPerfilProgenitor,
			int decremento) {

		int porsicionEtiquetaProgenitor = listEtiquetaPerfilProgenitor.size() - 1;
		buscarEtiquetaPerfilProgenitor(etiquetaPerfilOrigen,
				listEtiquetaPerfilProgenitor.get(porsicionEtiquetaProgenitor), listEtiquetaPerfilProgenitor,
				porsicionEtiquetaProgenitor);

		if (!banderaPerfilIncompatible) {
			int vDecremento = decremento - 1;
			if (vDecremento >= 0) {
				buscarEtiquetaPerfilOrigen(listEtiquetaPerfilOrigen.get(vDecremento), listEtiquetaPerfilOrigen,
						listEtiquetaPerfilProgenitor, vDecremento);
			}
			banderaPerfilIncompatible = false;
		} else {
			perfilGeneticoValido = false;
			banderaPerfilIncompatible = false;
		}

	}

	private void buscarEtiquetaPerfilProgenitor(EtiquetaPerfil etiquetaPerfilOrigen,
			EtiquetaPerfil etiquetaPerfilProgenitor, List<EtiquetaPerfil> listEtiquetaPerfilProgenitor,
			int decremento) {

		if (etiquetaPerfilOrigen.getIdMarcador() == etiquetaPerfilProgenitor.getIdMarcador()) {

			// ------------------------ //
			// System.out.println("--");

			// System.out.println("" + etiquetaPerfilOrigen.getPerfilGenetico());
			// System.out.println("" + etiquetaPerfilOrigen.getIdMarcador());

			// System.out.println("" + etiquetaPerfilProgenitor.getPerfilGenetico());
			// System.out.println("" + etiquetaPerfilProgenitor.getIdMarcador());

			identificadorOrigen = String.valueOf(etiquetaPerfilOrigen.getPerfilGenetico());
			identificadorProgenitor = String.valueOf(etiquetaPerfilProgenitor.getPerfilGenetico());
			estado = etiquetaPerfilOrigen.getEdoAbreviacion();

			compatibilidad = false;
			String aleloXOrigen = etiquetaPerfilOrigen.getAlelo_X();
			String aleloYOrigen = etiquetaPerfilOrigen.getAlelo_Y();

			String aleloXProgenitor = etiquetaPerfilProgenitor.getAlelo_X();
			String aleloYProgenitor = etiquetaPerfilProgenitor.getAlelo_Y();

			Objetivo objetivo = new Objetivo();
			objetivo.setAlelo1(aleloXOrigen);
			objetivo.setAlelo2(aleloYOrigen);

			/* Valores de compatibilidad */
			/* 1.- si hay compatibilidad */
			/* 2.- no hay compatibilidad */
			/*
			 * 3.- si hay compatibilidad pero el valor no existe en la tabla de frecuencia
			 */

			Compatible compatible = new Compatible();
			compatible.setAlelo1(aleloXProgenitor);
			compatible.setCoincideAlelo1(2);
			compatible.setAlelo2(aleloYProgenitor);
			compatible.setCoincideAlelo2(2);

			Marcador marcador = new Marcador();
			marcador.setMarcador(etiquetaPerfilProgenitor.getNombreMarcador());
			marcador.setObjetivo(objetivo);
			marcador.setCompatible(compatible);
			marcador.setIdMarcador(etiquetaPerfilProgenitor.getIdMarcador());
			marcador.setListaFrecuencias(etiquetaPerfilProgenitor.getListaFrecuencias());

			if (aleloXOrigen != null && aleloXProgenitor != null) {
				if (aleloXOrigen.equals(aleloXProgenitor)) {
					compatible.setCoincideAlelo1(1);
					compatibilidad = true;
				}
			}

			if (aleloXOrigen != null && aleloYProgenitor != null) {
				if (aleloXOrigen.equals(aleloYProgenitor)) {
					compatible.setCoincideAlelo2(1);
					compatibilidad = true;
				}
			}

			if (aleloYOrigen != null && aleloXProgenitor != null) {
				if (aleloYOrigen.equals(aleloXProgenitor)) {
					compatible.setCoincideAlelo1(1);
					compatibilidad = true;
				}
			}

			if (aleloYOrigen != null && aleloYProgenitor != null) {
				if (aleloYOrigen.equals(aleloYProgenitor)) {
					compatible.setCoincideAlelo2(1);
					compatibilidad = true;
				}
			}

			/* Validaciones para formula */

			String aleloA = "";
			String aleloB = "";

			if (aleloXOrigen.equals(aleloYOrigen)) {
				/* AA */
				if (compatible.getCoincideAlelo1() == 1 && compatible.getCoincideAlelo2() == 1) {
					/* AA */
					marcador.setNumeroFormula(1);
					marcador.setA(aleloXProgenitor);
				} else if (compatible.getCoincideAlelo1() == 1 && compatible.getCoincideAlelo2() == 2) {
					aleloA = compatible.getAlelo1();
					aleloB = compatible.getAlelo2();
				} else if (compatible.getCoincideAlelo1() == 2 && compatible.getCoincideAlelo2() == 1) {
					aleloA = compatible.getAlelo2();
					aleloB = compatible.getAlelo1();
				}
				if (!aleloA.equals("")) {
					if (!aleloA.equals(aleloB)) {
						/* AB */
						marcador.setNumeroFormula(2);
						marcador.setA(aleloA);
					}
				}
			} else if (!aleloXOrigen.equals(aleloYOrigen)) {
				/* AB */
				if (compatible.getCoincideAlelo1() == 1 && compatible.getCoincideAlelo2() == 1) {
					if (compatible.getAlelo1().equals(compatible.getAlelo2())) {
						/* AA */
						marcador.setNumeroFormula(3);
						marcador.setA(aleloXProgenitor);
					} else {
						/* AB */
						marcador.setNumeroFormula(4);
						marcador.setA(aleloXProgenitor);
						marcador.setB(aleloYProgenitor);
					}
				} else if (compatible.getCoincideAlelo1() == 1 && compatible.getCoincideAlelo2() == 2) {
					aleloA = compatible.getAlelo1();
					aleloB = compatible.getAlelo2();
				} else if (compatible.getCoincideAlelo1() == 2 && compatible.getCoincideAlelo2() == 1) {
					aleloA = compatible.getAlelo2();
					aleloB = compatible.getAlelo1();
				}
				if (!aleloA.equals("")) {
					if (!aleloA.equals(aleloB)) {
						/* AC */
						marcador.setNumeroFormula(5);
						marcador.setA(aleloA);
					}
				}
			}

			if (!compatibilidad) {
				contadorCompatibilidad++;
			}

			listMarcador.add(marcador);
		}

		if (contadorCompatibilidad <= numeroExclusiones) {
			int vDecremento = decremento - 1;
			if (vDecremento >= 0) {
				buscarEtiquetaPerfilProgenitor(etiquetaPerfilOrigen, listEtiquetaPerfilProgenitor.get(vDecremento),
						listEtiquetaPerfilProgenitor, vDecremento);
				perfilGeneticoValido = true;
			}
		} else {
			listMarcador = new ArrayList<Marcador>();
			banderaPerfilIncompatible = true;
			perfilGeneticoValido = false;
		}
	}
}
