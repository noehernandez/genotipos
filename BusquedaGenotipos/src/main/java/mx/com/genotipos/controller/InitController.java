package mx.com.genotipos.controller;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import mx.com.genotipos.model.BusquedaServicio;
import mx.com.genotipos.model.ResultadoBusqueda;
import mx.com.genotipos.service.IServicio;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = {"http://localhost:4200","http://3.17.128.176:4200","http://18.216.205.13:8080","*"} , allowedHeaders = "*")
public class InitController {

	@Autowired
	private IServicio servicio;

	@RequestMapping(value = "/busqueda_Por_Etiquetas",method = RequestMethod.POST)
	public ResponseEntity<ResultadoBusqueda> busqueda_Por_Etiquetas(@RequestBody BusquedaServicio busquedaServicio) {
		ResultadoBusqueda resultadoBusqueda = servicio.busquedaEtiquetaPerfil(busquedaServicio);
		return new ResponseEntity<ResultadoBusqueda>(resultadoBusqueda, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/busqueda_Por_Perfil",method = RequestMethod.POST)
	public ResponseEntity<ResultadoBusqueda> busqueda_Por_Perfil(@RequestBody BusquedaServicio busquedaServicio) {
		ResultadoBusqueda resultadoBusqueda = servicio.busquedaPerfil(busquedaServicio);
		return new ResponseEntity<ResultadoBusqueda>(resultadoBusqueda, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/busqueda_TresParticipantes",method = RequestMethod.POST)
	public ResponseEntity<ResultadoBusqueda> busqueda_TresParticipantes(@RequestBody BusquedaServicio busquedaServicio) {
		ResultadoBusqueda resultadoBusqueda = servicio.busquedaTresParticipantes(busquedaServicio);
		return new ResponseEntity<ResultadoBusqueda>(resultadoBusqueda, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/prueba",method = RequestMethod.POST)
	public ResponseEntity<BusquedaServicio> prueba() {
		BusquedaServicio busquedaServicio = new BusquedaServicio();
		busquedaServicio.setIdPerfil(3);
		List<Integer> listBusquedaPrueba = new ArrayList<Integer>();
		listBusquedaPrueba.add(132);
		listBusquedaPrueba.add(135);
		listBusquedaPrueba.add(105);
		busquedaServicio.setListaIdEntiquetasOrigen(listBusquedaPrueba);
		busquedaServicio.setListaIdEntiquetasProgenitor(listBusquedaPrueba);
		busquedaServicio.setNumeroExclusiones(2);
		busquedaServicio.setNumeroMarcadores(15);
		busquedaServicio.setIdTablaFrecuencia(12);
		return new ResponseEntity<BusquedaServicio>(busquedaServicio, HttpStatus.OK);
	}

}
