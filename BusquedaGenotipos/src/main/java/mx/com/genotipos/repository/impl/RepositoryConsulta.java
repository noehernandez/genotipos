package mx.com.genotipos.repository.impl;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import mx.com.genotipos.HelperBusquetaEtiqueta;
import mx.com.genotipos.model.AlelosFrecuencia;
import mx.com.genotipos.model.UniversoPerfilGenetico;
import mx.com.genotipos.repository.IRepositoryConsulta;

@Repository
public class RepositoryConsulta implements IRepositoryConsulta {

    @PersistenceContext(unitName = "barEntityManagerFactory")
    private EntityManager barEntityManagerFactory;

    @Autowired
    private HelperBusquetaEtiqueta helperBusquetaEtiqueta;

    @PostConstruct
    public void init() {
        helperBusquetaEtiqueta.setRepositoryConsulta(this);
    }

    @SuppressWarnings("unchecked")
    @Override
    public UniversoPerfilGenetico busquedaEtiquetaPerfil(String listaEtiquetas, String perfilesRevicion, int idFrecuencia) {
        try {
            System.out.println("buscando .." + listaEtiquetas);
//                    System.out.println(listaEtiquetas);
            StringBuffer sbQuery = new StringBuffer();
            sbQuery.append(" DECLARE @listaEtiquetas varchar(max) ");
            sbQuery.append(" SELECT @listaEtiquetas = '" + listaEtiquetas + "'");

            sbQuery.append(" DECLARE @listaPerfiles varchar(max) ");
            sbQuery.append(" SELECT @listaPerfiles = '" + perfilesRevicion + "'");

            sbQuery.append(" EXEC SP_BusquedaEtiquetaPerfil  @listaEtiquetas,@listaPerfiles  ");
            Query query = barEntityManagerFactory.createNativeQuery(sbQuery.toString());
            List<Object[]> listBusquedaEtiqueta = (List<Object[]>) query.getResultList();
            UniversoPerfilGenetico universoPG = helperBusquetaEtiqueta.objetoListaPerfil(listBusquedaEtiqueta, idFrecuencia);
            return universoPG;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public UniversoPerfilGenetico busquedaPerfil(String listaEtiquetas, int idFrecuencia) {
        try {
            StringBuffer sbQuery = new StringBuffer();
            sbQuery.append(" DECLARE @listaEtiquetas varchar(max) ");
            sbQuery.append(" SELECT @listaEtiquetas = '" + listaEtiquetas + "'");
            sbQuery.append(" EXEC SP_BusquedaPerfil  @listaEtiquetas ");
            Query query = barEntityManagerFactory.createNativeQuery(sbQuery.toString());
            List<Object[]> listBusquedaEtiqueta = (List<Object[]>) query.getResultList();
            UniversoPerfilGenetico universoPG = helperBusquetaEtiqueta.objetoPerfil(listBusquedaEtiqueta, idFrecuencia);
            return universoPG;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    /*Metodo para obtener el tipo de frecuencia  */
    @SuppressWarnings("unchecked")
    @Override
    public List<AlelosFrecuencia> seleccionAlelos_Frecuencia(int idFrecuencia) {
        try {
            StringBuffer sbQuery = new StringBuffer();
            sbQuery.append(" SELECT ayf_fre_id  ");
            sbQuery.append(" ,ayf_valor_alelo,ayf_mar_id ");
            sbQuery.append(" ,catAle.ale_descripcion   ");
            sbQuery.append(" FROM ALELOS_FRECUENCIA ayf ");
            sbQuery.append(" INNER JOIN CAT_ALELOS  catAle  ON catAle.ale_id = ayf.ayf_ale_id  ");
            sbQuery.append(" where ayf_fre_id =  " + idFrecuencia + " ");
            sbQuery.append(" order by ayf_mar_id ");

            Query query = barEntityManagerFactory.createNativeQuery(sbQuery.toString());
            List<Object[]> listSeleccionALELOS_FRECUENCIA = (List<Object[]>) query.getResultList();
            List<AlelosFrecuencia> listAlelosFrecuencia = new ArrayList<AlelosFrecuencia>();

            for (Object[] objeto : listSeleccionALELOS_FRECUENCIA) {
                AlelosFrecuencia alelosFrecuencia = new AlelosFrecuencia();
                alelosFrecuencia.setCat_fre_id((Integer) objeto[0]);
                alelosFrecuencia.setValor_alelo((Double) objeto[1]);
                alelosFrecuencia.setCat_mar_id((Integer) objeto[2]);
                alelosFrecuencia.setCat_ale_descripcion((String) objeto[3]);
                listAlelosFrecuencia.add(alelosFrecuencia);
            }

            return listAlelosFrecuencia;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

}
