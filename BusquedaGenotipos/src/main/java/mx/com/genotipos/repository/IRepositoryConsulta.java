package mx.com.genotipos.repository;
import java.util.List;

import mx.com.genotipos.model.AlelosFrecuencia;
import mx.com.genotipos.model.UniversoPerfilGenetico;

public interface IRepositoryConsulta {
	
	UniversoPerfilGenetico busquedaEtiquetaPerfil(String listaEtiquetas,String perfilesRevicion, int idFrecuencia);
	
	UniversoPerfilGenetico busquedaPerfil(String listaEtiquetas,  int idFrecuencia);
	
	/*Metodos para obtener valor frecuencia  */
	
	List<AlelosFrecuencia>  seleccionAlelos_Frecuencia(int idFrecuencia);
	
	
}
