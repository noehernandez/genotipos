/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author NS-448
 */
@Service
public class ExcelManager {
    
    public String getCellValue(Cell cell) {
        if (cell != null) {
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_STRING:
                    return cell.getStringCellValue().toUpperCase().replaceAll(" ", "").replaceAll("\n", "");
                case Cell.CELL_TYPE_NUMERIC:
                    return String.valueOf(cell.toString()).replace(".0", "");
                default:
                    return cell.toString().toUpperCase().replaceAll(" ", "").replaceAll("\n", "");
            }
        }
        return "";
    }
    
    public Sheet getSheet(MultipartFile file, int sheetIndex) throws IOException {
        InputStream stream = new ByteArrayInputStream(file.getBytes());
        Workbook workbook = null;
        if (file.getOriginalFilename().endsWith("xls")) {
            workbook = new HSSFWorkbook(stream);
        } else if (file.getOriginalFilename().endsWith("xlsx")) {
            workbook = new XSSFWorkbook(stream);
        }
        Sheet sheet = workbook.getSheetAt(sheetIndex);
        return sheet;
    }
}
