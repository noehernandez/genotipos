/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author NS-448
 */
@Service
public class FileManager {
    
    
    public String saveFile(MultipartFile file) {
        String fileName = new SimpleDateFormat("ddMMyy-hhmmss").format(new Date()) + "-" + file.getOriginalFilename();
        String destination = "C:/Genotipos/";
        String filePath = destination + fileName;
        new File(destination).mkdirs();
        try (FileOutputStream fos = new FileOutputStream(filePath)) {
            fos.write(file.getBytes());
            return filePath;
        } catch (FileNotFoundException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }
    
    public ResponseEntity<Resource> getResponseEntityFile(String path) throws MalformedURLException, IOException{
        File file = new File(path);
        Path filePath = Paths.get(file.getAbsolutePath());
        Resource resource = new UrlResource(filePath.toUri());
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(new ByteArrayResource(Files.readAllBytes(filePath)));
    }
    
    public File getFile(String path) throws MalformedURLException, IOException{
        return new File(path);
    }
    
    public Path getFilePath(String path) throws MalformedURLException, IOException{
        File file = new File(path);
        return Paths.get(file.getAbsolutePath());
    }
    
}
