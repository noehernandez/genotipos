/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao;

import com.mx.genotipos.dao.model.CatFrecuencias;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author NS-448
 */
@Transactional
@Repository
public interface FrequencyDAO extends JpaRepository<CatFrecuencias,Integer>{
    
    @Query("SELECT f FROM CatFrecuencias f WHERE f.freBajaLogica = :lowLogic")
    public List<CatFrecuencias> findByBajaLogica(@Param("lowLogic") Integer lowLogic);
    
    @Modifying
    @Query("UPDATE CatFrecuencias f SET f.freDefault = false")
    public int updateDefaultAll();
    
    @Modifying
    @Query("UPDATE CatFrecuencias f SET f.freDefault = :isDefault WHERE f.freId = :frequencyId")
    public int updateDefault(@Param("isDefault") Boolean isDefault, @Param("frequencyId") Integer frequencyId);
    
    @Modifying
    @Query("UPDATE CatFrecuencias f SET f.freBajaLogica = :lowLogic WHERE f.freId = :frequencyId")
    public int updateFreBajaLogica(@Param("lowLogic") Integer lowLogic, @Param("frequencyId") Integer frequencyId);
}
