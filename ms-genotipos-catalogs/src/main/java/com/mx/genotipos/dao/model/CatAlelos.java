/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "CAT_ALELOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatAlelos.findAll", query = "SELECT c FROM CatAlelos c")
    , @NamedQuery(name = "CatAlelos.findByAleId", query = "SELECT c FROM CatAlelos c WHERE c.aleId = :aleId")
    , @NamedQuery(name = "CatAlelos.findByAleDescripcion", query = "SELECT c FROM CatAlelos c WHERE c.aleDescripcion = :aleDescripcion")
    , @NamedQuery(name = "CatAlelos.findByAleBajaLogica", query = "SELECT c FROM CatAlelos c WHERE c.aleBajaLogica = :aleBajaLogica")
    , @NamedQuery(name = "CatAlelos.findByAleFechaRegistro", query = "SELECT c FROM CatAlelos c WHERE c.aleFechaRegistro = :aleFechaRegistro")})
public class CatAlelos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ale_id")
    private Integer aleId;
    @Column(name = "ale_descripcion")
    private String aleDescripcion;
    @Column(name = "ale_baja_logica")
    private Integer aleBajaLogica;
    @Column(name = "ale_fecha_registro")
    @Temporal(TemporalType.DATE)
    private Date aleFechaRegistro;
    @OneToMany(mappedBy = "ayfAleId", fetch = FetchType.LAZY)
    private List<AlelosFrecuencia> alelosFrecuenciaList;

    public CatAlelos() {
    }

    public CatAlelos(Integer aleId) {
        this.aleId = aleId;
    }

    public Integer getAleId() {
        return aleId;
    }

    public void setAleId(Integer aleId) {
        this.aleId = aleId;
    }

    public String getAleDescripcion() {
        return aleDescripcion;
    }

    public void setAleDescripcion(String aleDescripcion) {
        this.aleDescripcion = aleDescripcion;
    }

    public Integer getAleBajaLogica() {
        return aleBajaLogica;
    }

    public void setAleBajaLogica(Integer aleBajaLogica) {
        this.aleBajaLogica = aleBajaLogica;
    }

    public Date getAleFechaRegistro() {
        return aleFechaRegistro;
    }

    public void setAleFechaRegistro(Date aleFechaRegistro) {
        this.aleFechaRegistro = aleFechaRegistro;
    }

    @XmlTransient
    public List<AlelosFrecuencia> getAlelosFrecuenciaList() {
        return alelosFrecuenciaList;
    }

    public void setAlelosFrecuenciaList(List<AlelosFrecuencia> alelosFrecuenciaList) {
        this.alelosFrecuenciaList = alelosFrecuenciaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (aleId != null ? aleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatAlelos)) {
            return false;
        }
        CatAlelos other = (CatAlelos) object;
        if ((this.aleId == null && other.aleId != null) || (this.aleId != null && !this.aleId.equals(other.aleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mx.genotipos.dao.model.CatAlelos[ aleId=" + aleId + " ]";
    }
    
}
