/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "ALELOS_FRECUENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AlelosFrecuencia.findAll", query = "SELECT a FROM AlelosFrecuencia a")
    , @NamedQuery(name = "AlelosFrecuencia.findByAyfId", query = "SELECT a FROM AlelosFrecuencia a WHERE a.ayfId = :ayfId")
    , @NamedQuery(name = "AlelosFrecuencia.findByAyfDescripcion", query = "SELECT a FROM AlelosFrecuencia a WHERE a.ayfDescripcion = :ayfDescripcion")
    , @NamedQuery(name = "AlelosFrecuencia.findByAyfValorAlelo", query = "SELECT a FROM AlelosFrecuencia a WHERE a.ayfValorAlelo = :ayfValorAlelo")})
public class AlelosFrecuencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name = "ayf_id")
    private Integer ayfId;
    @Column(name = "ayf_descripcion")
    private String ayfDescripcion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ayf_valor_alelo")
    private Double ayfValorAlelo;
    @JoinColumn(name = "ayf_ale_id", referencedColumnName = "ale_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CatAlelos ayfAleId;
    @JoinColumn(name = "ayf_fre_id", referencedColumnName = "fre_id")
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private CatFrecuencias ayfFreId;
    @JoinColumn(name = "ayf_mar_id", referencedColumnName = "mar_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CatMarcadores ayfMarId;

    public AlelosFrecuencia() {
    }

    public AlelosFrecuencia(Double ayfValorAlelo, Integer ayfAleId, Integer ayfMarId, CatFrecuencias ayfFreId) {
        this.ayfValorAlelo = ayfValorAlelo;
        this.ayfAleId = new CatAlelos(ayfAleId);
        this.ayfMarId = new CatMarcadores(ayfMarId);
        this.ayfFreId = ayfFreId;
    }
    
    

    public AlelosFrecuencia(Integer ayfId) {
        this.ayfId = ayfId;
    }

    public Integer getAyfId() {
        return ayfId;
    }

    public void setAyfId(Integer ayfId) {
        this.ayfId = ayfId;
    }

    public String getAyfDescripcion() {
        return ayfDescripcion;
    }

    public void setAyfDescripcion(String ayfDescripcion) {
        this.ayfDescripcion = ayfDescripcion;
    }

    public Double getAyfValorAlelo() {
        return ayfValorAlelo;
    }

    public void setAyfValorAlelo(Double ayfValorAlelo) {
        this.ayfValorAlelo = ayfValorAlelo;
    }

    public CatAlelos getAyfAleId() {
        return ayfAleId;
    }

    public void setAyfAleId(CatAlelos ayfAleId) {
        this.ayfAleId = ayfAleId;
    }

    public CatFrecuencias getAyfFreId() {
        return ayfFreId;
    }

    public void setAyfFreId(CatFrecuencias ayfFreId) {
        this.ayfFreId = ayfFreId;
    }

    public CatMarcadores getAyfMarId() {
        return ayfMarId;
    }

    public void setAyfMarId(CatMarcadores ayfMarId) {
        this.ayfMarId = ayfMarId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ayfId != null ? ayfId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AlelosFrecuencia)) {
            return false;
        }
        AlelosFrecuencia other = (AlelosFrecuencia) object;
        if ((this.ayfId == null && other.ayfId != null) || (this.ayfId != null && !this.ayfId.equals(other.ayfId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mx.genotipos.dao.model.AlelosFrecuencia[ ayfId=" + ayfId + " ]";
    }
    
}
