/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao;

import java.util.List;
import com.mx.genotipos.dao.model.CatAlelos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author NS-448
 */
@Transactional
@Repository
public interface AlleleDAO extends JpaRepository<CatAlelos,Integer>{
    
    @Query("SELECT c FROM CatAlelos c WHERE c.aleBajaLogica = :aleBajaLogica")
    public List<CatAlelos> findByBajaLogica(@Param("aleBajaLogica") Integer aleBajaLogica);
}
