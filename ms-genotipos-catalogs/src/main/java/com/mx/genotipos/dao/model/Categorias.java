/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "CATEGORIAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categorias.findAll", query = "SELECT c FROM Categorias c")
    , @NamedQuery(name = "Categorias.findByCatCategoriaId", query = "SELECT c FROM Categorias c WHERE c.catCategoriaId = :catCategoriaId")
    , @NamedQuery(name = "Categorias.findByCatDescripcion", query = "SELECT c FROM Categorias c WHERE c.catDescripcion = :catDescripcion")
    , @NamedQuery(name = "Categorias.findByCatBajaLogica", query = "SELECT c FROM Categorias c WHERE c.catBajaLogica = :catBajaLogica")
    , @NamedQuery(name = "Categorias.findByCatFechaRegistro", query = "SELECT c FROM Categorias c WHERE c.catFechaRegistro = :catFechaRegistro")
    , @NamedQuery(name = "Categorias.findByCatIdUsuario", query = "SELECT c FROM Categorias c WHERE c.catIdUsuario = :catIdUsuario")})
public class Categorias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cat_categoria_id")
    private Integer catCategoriaId;
    @Column(name = "cat_descripcion")
    private String catDescripcion;
    @Column(name = "cat_baja_logica")
    private Integer catBajaLogica;
    @Column(name = "cat_fecha_registro")
    @Temporal(TemporalType.DATE)
    private Date catFechaRegistro;
    @Column(name = "cat_id_usuario")
    private Integer catIdUsuario;
    @OneToMany(mappedBy = "etiCategoriaId", fetch = FetchType.EAGER)
    private List<Etiquetas> etiquetasList;

    public Categorias() {
    }

    public Categorias(Integer catCategoriaId, String catDescripcion, Integer catIdUsuario) {
        this.catCategoriaId = catCategoriaId;
        this.catDescripcion = catDescripcion;
        this.catIdUsuario = catIdUsuario;
        this.catBajaLogica = 1;
        this.catFechaRegistro = new Date();
    }
    
    

    public Categorias(Integer catCategoriaId) {
        this.catCategoriaId = catCategoriaId;
    }

    public Integer getCatCategoriaId() {
        return catCategoriaId;
    }

    public void setCatCategoriaId(Integer catCategoriaId) {
        this.catCategoriaId = catCategoriaId;
    }

    public String getCatDescripcion() {
        return catDescripcion;
    }

    public void setCatDescripcion(String catDescripcion) {
        this.catDescripcion = catDescripcion;
    }

    public Integer getCatBajaLogica() {
        return catBajaLogica;
    }

    public void setCatBajaLogica(Integer catBajaLogica) {
        this.catBajaLogica = catBajaLogica;
    }

    public Date getCatFechaRegistro() {
        return catFechaRegistro;
    }

    public void setCatFechaRegistro(Date catFechaRegistro) {
        this.catFechaRegistro = catFechaRegistro;
    }

    public Integer getCatIdUsuario() {
        return catIdUsuario;
    }

    public void setCatIdUsuario(Integer catIdUsuario) {
        this.catIdUsuario = catIdUsuario;
    }

    @XmlTransient
    public List<Etiquetas> getEtiquetasList() {
        return etiquetasList;
    }

    public void setEtiquetasList(List<Etiquetas> etiquetasList) {
        this.etiquetasList = etiquetasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catCategoriaId != null ? catCategoriaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categorias)) {
            return false;
        }
        Categorias other = (Categorias) object;
        if ((this.catCategoriaId == null && other.catCategoriaId != null) || (this.catCategoriaId != null && !this.catCategoriaId.equals(other.catCategoriaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mx.genotipos.dao.entities.Categorias[ catCategoriaId=" + catCategoriaId + " ]";
    }
    
}
