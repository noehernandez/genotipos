/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao;

import com.mx.genotipos.dao.model.Categorias;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author NS-448
 */
@Transactional
@Repository
public interface CategoryDAO extends JpaRepository<Categorias,Integer>{
    
    @Modifying
    @Query("UPDATE Categorias c SET c.catBajaLogica = :lowLogic WHERE c.catCategoriaId = :idCategory")
    public int updateBajaLogica(@Param("lowLogic") Integer lowLogic,@Param("idCategory") Integer idCategory);
    
    @Query("SELECT c FROM Categorias c WHERE c.catBajaLogica = :catBajaLogica")
    public List<Categorias> findByBajaLogica(@Param("catBajaLogica") Integer lowLogic);
}
