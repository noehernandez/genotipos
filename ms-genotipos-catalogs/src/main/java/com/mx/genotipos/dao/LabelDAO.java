/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao;

import com.mx.genotipos.dao.model.Categorias;
import com.mx.genotipos.dao.model.Etiquetas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author NS-448
 */
@Transactional
@Repository
public interface LabelDAO extends JpaRepository<Etiquetas,Integer>{
    
    @Query("SELECT e FROM Etiquetas e WHERE e.etiBajaLogica = 1")
    public List<Etiquetas> findByBajaLogica();
    
    @Query("SELECT e FROM Etiquetas e WHERE e.etiCategoriaId.catCategoriaId = :categoryId")
    public List<Etiquetas> findByCategoria(@Param("categoryId") Integer categoryId);
    
//    select * from [dbo].[ETIQUETAS] where eti_categoria_id = 1 and eti_baja_logica=1
    @Query("SELECT count(e) FROM Etiquetas e WHERE e.etiCategoriaId.catCategoriaId = :categoryId and e.etiBajaLogica=1")
    public Integer findByIdCategoria(@Param("categoryId") Integer categoryId);
}
