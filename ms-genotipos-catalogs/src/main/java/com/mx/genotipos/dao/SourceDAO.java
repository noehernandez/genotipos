/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao;

import java.util.List;
import com.mx.genotipos.dao.model.Fuentes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author NS-448
 */
@Transactional
@Repository
public interface SourceDAO extends JpaRepository<Fuentes,Integer>{
    
    @Query("SELECT f FROM Fuentes f WHERE f.fntBajaLogica = :lowLogic")
    public List<Fuentes> findByBajaLogica(@Param("lowLogic") Integer lowLogic);
    
    @Modifying
    @Query("UPDATE Fuentes f SET f.fntBajaLogica = :lowLogic WHERE f.fntId = :idSource")
    public int updateBajaLogica(@Param("lowLogic") Integer lowLogic,@Param("idSource") Integer idSource);
    
}
