/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "CAT_TIPO_MARCADOR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatTipoMarcador.findAll", query = "SELECT c FROM CatTipoMarcador c")
    , @NamedQuery(name = "CatTipoMarcador.findByTmId", query = "SELECT c FROM CatTipoMarcador c WHERE c.tmId = :tmId")
    , @NamedQuery(name = "CatTipoMarcador.findByTmDescripcion", query = "SELECT c FROM CatTipoMarcador c WHERE c.tmDescripcion = :tmDescripcion")
    , @NamedQuery(name = "CatTipoMarcador.findByTmFechaRegistro", query = "SELECT c FROM CatTipoMarcador c WHERE c.tmFechaRegistro = :tmFechaRegistro")
    , @NamedQuery(name = "CatTipoMarcador.findByTmBajaLogica", query = "SELECT c FROM CatTipoMarcador c WHERE c.tmBajaLogica = :tmBajaLogica")})
public class CatTipoMarcador implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name = "tm_id")
    private Integer tmId;
    @Column(name = "tm_descripcion")
    private String tmDescripcion;
    @Column(name = "tm_fecha_registro")
    @Temporal(TemporalType.DATE)
    private Date tmFechaRegistro;
    @Column(name = "tm_baja_logica")
    private Boolean tmBajaLogica;
    @OneToMany(mappedBy = "marTipo", fetch = FetchType.LAZY)
    private List<CatMarcadores> catMarcadoresList;

    public CatTipoMarcador() {
    }

    public CatTipoMarcador(Integer tmId) {
        this.tmId = tmId;
    }

    public Integer getTmId() {
        return tmId;
    }

    public void setTmId(Integer tmId) {
        this.tmId = tmId;
    }

    public String getTmDescripcion() {
        return tmDescripcion;
    }

    public void setTmDescripcion(String tmDescripcion) {
        this.tmDescripcion = tmDescripcion;
    }

    public Date getTmFechaRegistro() {
        return tmFechaRegistro;
    }

    public void setTmFechaRegistro(Date tmFechaRegistro) {
        this.tmFechaRegistro = tmFechaRegistro;
    }

    public Boolean getTmBajaLogica() {
        return tmBajaLogica;
    }

    public void setTmBajaLogica(Boolean tmBajaLogica) {
        this.tmBajaLogica = tmBajaLogica;
    }

    @XmlTransient
    public List<CatMarcadores> getCatMarcadoresList() {
        return catMarcadoresList;
    }

    public void setCatMarcadoresList(List<CatMarcadores> catMarcadoresList) {
        this.catMarcadoresList = catMarcadoresList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tmId != null ? tmId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatTipoMarcador)) {
            return false;
        }
        CatTipoMarcador other = (CatTipoMarcador) object;
        if ((this.tmId == null && other.tmId != null) || (this.tmId != null && !this.tmId.equals(other.tmId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mx.genotipos.dao.entities.CatTipoMarcador[ tmId=" + tmId + " ]";
    }
    
}
