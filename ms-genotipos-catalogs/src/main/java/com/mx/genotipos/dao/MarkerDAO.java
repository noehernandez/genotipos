/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao;

import com.mx.genotipos.dao.model.CatMarcadores;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author NS-448
 */
@Transactional
@Repository
public interface MarkerDAO extends JpaRepository<CatMarcadores,Integer>{
    
    @Query("SELECT c FROM CatMarcadores c WHERE c.marBajaLogica = 1")
    public List<CatMarcadores> findByBajaLogica();
    
    // Se agrego el consulta para eliminar de manera logica
    @Modifying
    @Query("UPDATE CatMarcadores cm SET cm.marBajaLogica = :lowLogic WHERE cm.marId = :idSource")
    public int updateBajaLogica(@Param("lowLogic") Integer lowLogic,@Param("idSource") Integer idSource);

}
