/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "CAT_FRECUENCIAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatFrecuencias.findAll", query = "SELECT c FROM CatFrecuencias c")
    , @NamedQuery(name = "CatFrecuencias.findByFreId", query = "SELECT c FROM CatFrecuencias c WHERE c.freId = :freId")
    , @NamedQuery(name = "CatFrecuencias.findByFreNombre", query = "SELECT c FROM CatFrecuencias c WHERE c.freNombre = :freNombre")
    , @NamedQuery(name = "CatFrecuencias.findByFreNombreArchivo", query = "SELECT c FROM CatFrecuencias c WHERE c.freNombreArchivo = :freNombreArchivo")
    , @NamedQuery(name = "CatFrecuencias.findByFreBajaLogica", query = "SELECT c FROM CatFrecuencias c WHERE c.freBajaLogica = :freBajaLogica")
    , @NamedQuery(name = "CatFrecuencias.findByFreFechaRegistro", query = "SELECT c FROM CatFrecuencias c WHERE c.freFechaRegistro = :freFechaRegistro")
    , @NamedQuery(name = "CatFrecuencias.findByFreRutaArchivo", query = "SELECT c FROM CatFrecuencias c WHERE c.freRutaArchivo = :freRutaArchivo")
    , @NamedQuery(name = "CatFrecuencias.findByFreIdImportacion", query = "SELECT c FROM CatFrecuencias c WHERE c.freIdImportacion = :freIdImportacion")
    , @NamedQuery(name = "CatFrecuencias.findByFreDefault", query = "SELECT c FROM CatFrecuencias c WHERE c.freDefault = :freDefault")})
public class CatFrecuencias implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ayfFreId", fetch = FetchType.LAZY)
    private List<AlelosFrecuencia> alelosFrecuencia;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name = "fre_id")
    private Integer freId;
    @Column(name = "fre_nombre")
    private String freNombre;
    @Column(name = "fre_nombre_archivo")
    private String freNombreArchivo;
    @Column(name = "fre_baja_logica")
    private Integer freBajaLogica;
    @Column(name = "fre_fecha_registro")
    @Temporal(TemporalType.DATE)
    private Date freFechaRegistro;
    @Column(name = "fre_ruta_archivo")
    private String freRutaArchivo;
    @Column(name = "fre_id_importacion", insertable=false, updatable=false)
    private String freIdImportacion;
    @Column(name = "fre_default")
    private Boolean freDefault;
    @Column(name = "fre_usuario")
    private Integer freUsuario;

    public CatFrecuencias() {
    }

    public CatFrecuencias( String freNombre, String freNombreArchivo, String freRutaArchivo, Integer freUsuario) {
        this.freNombre = freNombre;
        this.freNombreArchivo = freNombreArchivo;
        this.freRutaArchivo = freRutaArchivo;
        this.freBajaLogica = 1;
        this.freFechaRegistro = new Date();
        this.freDefault = false;
        this.freUsuario = freUsuario;
    }
    
    

    public CatFrecuencias(Integer freId) {
        this.freId = freId;
    }

    public Integer getFreId() {
        return freId;
    }

    public void setFreId(Integer freId) {
        this.freId = freId;
    }

    public String getFreNombre() {
        return freNombre;
    }

    public void setFreNombre(String freNombre) {
        this.freNombre = freNombre;
    }

    public String getFreNombreArchivo() {
        return freNombreArchivo;
    }

    public void setFreNombreArchivo(String freNombreArchivo) {
        this.freNombreArchivo = freNombreArchivo;
    }

    public Integer getFreBajaLogica() {
        return freBajaLogica;
    }

    public void setFreBajaLogica(Integer freBajaLogica) {
        this.freBajaLogica = freBajaLogica;
    }

    public Date getFreFechaRegistro() {
        return freFechaRegistro;
    }

    public void setFreFechaRegistro(Date freFechaRegistro) {
        this.freFechaRegistro = freFechaRegistro;
    }

    public String getFreRutaArchivo() {
        return freRutaArchivo;
    }

    public void setFreRutaArchivo(String freRutaArchivo) {
        this.freRutaArchivo = freRutaArchivo;
    }

    public String getFreIdImportacion() {
        return freIdImportacion;
    }

    public void setFreIdImportacion(String freIdImportacion) {
        this.freIdImportacion = freIdImportacion;
    }

    public Boolean getFreDefault() {
        return freDefault;
    }

    public void setFreDefault(Boolean freDefault) {
        this.freDefault = freDefault;
    }

    public Integer getFreUsuario() {
        return freUsuario;
    }

    public void setFreUsuario(Integer freUsuario) {
        this.freUsuario = freUsuario;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (freId != null ? freId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatFrecuencias)) {
            return false;
        }
        CatFrecuencias other = (CatFrecuencias) object;
        if ((this.freId == null && other.freId != null) || (this.freId != null && !this.freId.equals(other.freId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mx.genotipos.dao.entities.CatFrecuencias[ freId=" + freId + " ]";
    }

    @XmlTransient
    public List<AlelosFrecuencia> getAlelosFrecuenciaList() {
        return alelosFrecuencia;
    }

    public void setAlelosFrecuenciaList(List<AlelosFrecuencia> alelosFrecuencia) {
        this.alelosFrecuencia = alelosFrecuencia;
    }
    
}
