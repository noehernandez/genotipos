/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "ETIQUETAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Etiquetas.findAll", query = "SELECT e FROM Etiquetas e")
    , @NamedQuery(name = "Etiquetas.findByEtiId", query = "SELECT e FROM Etiquetas e WHERE e.etiId = :etiId")
    , @NamedQuery(name = "Etiquetas.findByEtiDescripcion", query = "SELECT e FROM Etiquetas e WHERE e.etiDescripcion = :etiDescripcion")
    , @NamedQuery(name = "Etiquetas.findByEtiBajaLogica", query = "SELECT e FROM Etiquetas e WHERE e.etiBajaLogica = :etiBajaLogica")
    , @NamedQuery(name = "Etiquetas.findByEtiFechaRegistro", query = "SELECT e FROM Etiquetas e WHERE e.etiFechaRegistro = :etiFechaRegistro")
    , @NamedQuery(name = "Etiquetas.findByEtiIdUsuario", query = "SELECT e FROM Etiquetas e WHERE e.etiIdUsuario = :etiIdUsuario")})
public class Etiquetas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name = "eti_id")
    private Integer etiId;
    @Column(name = "eti_descripcion")
    private String etiDescripcion;
    @Column(name = "eti_baja_logica")
    private Integer etiBajaLogica;
    @Column(name = "eti_fecha_registro")
    @Temporal(TemporalType.DATE)
    private Date etiFechaRegistro;
    @Column(name = "eti_id_usuario")
    private Integer etiIdUsuario;
    @JoinColumn(name = "eti_categoria_id", referencedColumnName = "cat_categoria_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Categorias etiCategoriaId;

    public Etiquetas() {
    }

    public Etiquetas(Integer etiId, String etiDescripcion, Integer etiIdUsuario, Categorias etiCategoriaId) {
        this.etiId = etiId;
        this.etiDescripcion = etiDescripcion;
        this.etiIdUsuario = etiIdUsuario;
        this.etiCategoriaId = etiCategoriaId;
        this.etiFechaRegistro = new Date();
        this.etiBajaLogica = 1;
    }
    
    public Etiquetas(String etiDescripcion, Integer etiIdUsuario, Integer etiCategoriaId) {
        this.etiId = etiId;
        this.etiDescripcion = etiDescripcion;
        this.etiIdUsuario = etiIdUsuario;
        this.etiCategoriaId = new Categorias(etiCategoriaId);
        this.etiFechaRegistro = new Date();
        this.etiBajaLogica = 1;
    }

    public Etiquetas(Integer etiId) {
        this.etiId = etiId;
    }

    public Integer getEtiId() {
        return etiId;
    }

    public void setEtiId(Integer etiId) {
        this.etiId = etiId;
    }

    public String getEtiDescripcion() {
        return etiDescripcion;
    }

    public void setEtiDescripcion(String etiDescripcion) {
        this.etiDescripcion = etiDescripcion;
    }

    public Integer getEtiBajaLogica() {
        return etiBajaLogica;
    }

    public void setEtiBajaLogica(Integer etiBajaLogica) {
        this.etiBajaLogica = etiBajaLogica;
    }

    public Date getEtiFechaRegistro() {
        return etiFechaRegistro;
    }

    public void setEtiFechaRegistro(Date etiFechaRegistro) {
        this.etiFechaRegistro = etiFechaRegistro;
    }

    public Integer getEtiIdUsuario() {
        return etiIdUsuario;
    }

    public void setEtiIdUsuario(Integer etiIdUsuario) {
        this.etiIdUsuario = etiIdUsuario;
    }

    public Categorias getEtiCategoriaId() {
        return etiCategoriaId;
    }

    public void setEtiCategoriaId(Categorias etiCategoriaId) {
        this.etiCategoriaId = etiCategoriaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (etiId != null ? etiId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Etiquetas)) {
            return false;
        }
        Etiquetas other = (Etiquetas) object;
        if ((this.etiId == null && other.etiId != null) || (this.etiId != null && !this.etiId.equals(other.etiId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mx.genotipos.dao.entities.Etiquetas[ etiId=" + etiId + " ]";
    }
    
}
