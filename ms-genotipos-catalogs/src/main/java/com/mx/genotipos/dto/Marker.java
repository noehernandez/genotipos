/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

import lombok.*;

/**
 *
 * @author NS-448
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Marker {
    
    private Integer id;
    private String name;
    private String type;
    private Integer typeId;
    private String userWhoCreated;
    private String userWhoEdited;
    private String date;
}
