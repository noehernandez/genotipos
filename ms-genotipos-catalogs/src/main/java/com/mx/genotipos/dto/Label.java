/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

import lombok.*;

/**
 *
 * @author NS-448
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Label {
    
    private Integer id;
    private String name;
    private Integer categoryId;
    private Integer associatedProfiles;
    private String creationDate;
}
