/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;
import java.util.List;
import lombok.*;

/**
 *
 * @author NS-448
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Source {
    
    private Integer id;
    private String name;
    private String internalId;
    private String externalId;
    private String contactName;
    private String contactEmail;
    private String contactPhone;
    private String otherContactPhone;
    
}
