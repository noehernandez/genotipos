/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

import lombok.*;

/**
 *
 * @author NS-448
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FrequencyImport {
    
    private Integer id;
    private String importId;
    private String name;
    private String fileName;
    private String user;
    private String date;
    private boolean assignedByDefault;
    
}
