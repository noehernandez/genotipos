/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.controller;

import com.mx.genotipos.dto.Label;
import com.mx.genotipos.dto.Response;
import com.mx.genotipos.dto.Source;
import com.mx.genotipos.service.impl.SourceServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author NS-448
 */
@RestController
@RequestMapping("/api/source")
@CrossOrigin(origins = {"http://localhost:4200","http://3.17.128.176:4200","http://18.216.205.13:8080","*"} , allowedHeaders = "*")

public class SourceController {
    
    private SourceServiceImpl sourceServiceImpl;
    
    @Autowired
    public SourceController(SourceServiceImpl sourceServiceImpl) {
        this.sourceServiceImpl = sourceServiceImpl;
    }
    
    @GetMapping("/active/{userId}")
    public Response getActiveSources(@PathVariable Integer userId){
        return sourceServiceImpl.getActiveSources(userId);
    }
    
    @PostMapping("/{userId}")
    public Response addSource(@RequestBody Source source, @PathVariable Integer userId){
        return sourceServiceImpl.addSource(source, userId);
    }
    
    @PutMapping("")
    public Response updateSource(@RequestBody Source source){
        return sourceServiceImpl.updateSource(source);
    }
    
    @DeleteMapping("/{sourceId}")
    public Response deleteSource(@PathVariable Integer sourceId){
        return sourceServiceImpl.deleteSource(sourceId);
    }
    
}
