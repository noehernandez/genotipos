/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.controller;

import com.mx.genotipos.dto.Response;
import com.mx.genotipos.dto.Marker;
import com.mx.genotipos.service.impl.MarkerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author NS-448
 */
@RestController
@RequestMapping("/api/marker")
@CrossOrigin(origins = {"http://localhost:4200","http://3.17.128.176:4200","http://18.216.205.13:8080","*"} , allowedHeaders = "*")

public class MarkerController {
    
    private MarkerServiceImpl markerServiceImpl;

    @Autowired
    public MarkerController(MarkerServiceImpl markerServiceImpl) {
        this.markerServiceImpl = markerServiceImpl;
    }
    
    @GetMapping("/active/{userId}")
    public Response getActiveMarkers(@PathVariable Integer userId){
        return markerServiceImpl.getActiveMarkers(userId);
    }
    
    @PostMapping("/{userId}")
    public Response addMarker(@RequestBody Marker marker, @PathVariable Integer userId){
        return markerServiceImpl.addMarker(marker, userId);
    }
    
    @PutMapping("")
    public Response updateMarker(@RequestBody Marker marker){
        return markerServiceImpl.updateMarker(marker);
    }
    
    @DeleteMapping("/{sourceId}")
    public Response deleteMarker(@PathVariable Integer sourceId){
        return markerServiceImpl.deleteMarker(sourceId);
    }
}
