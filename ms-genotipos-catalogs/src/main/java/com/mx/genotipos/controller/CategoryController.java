/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.controller;

import com.mx.genotipos.dto.Category;
import com.mx.genotipos.dto.Response;
import com.mx.genotipos.service.impl.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author NS-448
 */
@RestController
@RequestMapping("/api/category")
@CrossOrigin(origins = {"http://localhost:4200","http://3.17.128.176:4200","http://18.216.205.13:8080","*"} , allowedHeaders = "*")
public class CategoryController {
    
    private final CategoryServiceImpl categoryServiceImpl;
    
    @Autowired
    public CategoryController(CategoryServiceImpl categoryServiceImpl) {
        this.categoryServiceImpl = categoryServiceImpl;
    }
    
//    @PostMapping("/validarXml")
    @GetMapping("/active/{userId}")
    public Response getActiveCategories(@PathVariable Integer userId){
        return categoryServiceImpl.getActiveCategories(userId);
    }
    
    @PostMapping("/{userId}")
    public Response addCategory(@RequestBody Category category, @PathVariable Integer userId){
        return categoryServiceImpl.addCategory(category,userId);
    }
    
    @PutMapping("")
    public Response updateCategory(@RequestBody Category category){
        return categoryServiceImpl.updateCategory(category);
    }
    
    @DeleteMapping("/{categoryId}")
    public Response deleteCategory(@PathVariable Integer categoryId){
        return categoryServiceImpl.deleteCategory(categoryId);
    }
}
