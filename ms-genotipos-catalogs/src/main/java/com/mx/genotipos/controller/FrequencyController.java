/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.controller;

import com.google.gson.Gson;
import com.mx.genotipos.dto.FrequencyImport;
import com.mx.genotipos.dto.Marker;
import com.mx.genotipos.dto.Response;
import com.mx.genotipos.service.impl.FrequencyServiceImpl;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author NS-448
 */
@RestController
@RequestMapping("/api/frequency")
@CrossOrigin(origins = {"http://localhost:4200","http://3.17.128.176:4200","http://18.216.205.13:8080","*"} , allowedHeaders = "*")
public class FrequencyController {
    
    private FrequencyServiceImpl frequencyServiceImpl;

    @Autowired
    public FrequencyController(FrequencyServiceImpl FrequencyServiceImpl) {
        this.frequencyServiceImpl = FrequencyServiceImpl;
    }
        
    @GetMapping("/active/{userId}")
    public Response getActiveFrequencies(@PathVariable Integer userId){
        return frequencyServiceImpl.getActiveFrequencyImports(userId);
    }
    
    @GetMapping(value="/file/{frequencyImportId}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Resource> getFrequencyFile(@PathVariable Integer frequencyImportId) throws IOException {
        return frequencyServiceImpl.getFrequencyFile(frequencyImportId);
    }
    
    @PostMapping("/{userId}")
    public Response addFrequencyImport(@RequestParam("file") MultipartFile file,@RequestParam("name") String name, @PathVariable Integer userId){
//        Gson gson =  new Gson();
//        System.out.println(strImport);
//        FrequencyImport frequencyImport=gson.fromJson(strImport, FrequencyImport.class);
        return frequencyServiceImpl.addFrequency(file, name, userId);
    }
    
    @PutMapping("/default/{frequencyImportId}")
    public Response assignFrequencyByDefault(@PathVariable Integer frequencyImportId){
        return frequencyServiceImpl.assignFrequencyByDefault(frequencyImportId);
    }
    
    @DeleteMapping("/{frequencyId}")
    public Response deleteFrequency(@PathVariable Integer frequencyId){
        return frequencyServiceImpl.deleteFrequency(frequencyId);
    }
    
//    @PutMapping("")
//    public Response updateFrequency(@RequestBody Marker marker){
//        return markerServiceImpl.updateMarker(marker);
//    }
//    
//    @DeleteMapping("/{sourceId}")
//    public Response deleteFrequency(@PathVariable Integer sourceId){
//        return markerServiceImpl.deleteMarker(sourceId);
//    }
    
}
