/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service;

import com.mx.genotipos.dto.FrequencyImport;
import com.mx.genotipos.dto.Response;
import com.mx.genotipos.dto.Source;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author NS-448
 */
public interface IFrequencyService {
    
    public Response getActiveFrequencyImports(Integer userId);
    public Response addFrequency(MultipartFile file,String name, Integer userId);
//    public Response updateSource(Source source);
    public Response deleteFrequency(Integer frequencyImportId);
    public Response assignFrequencyByDefault(Integer frequencyImportId);
    public ResponseEntity<Resource> getFrequencyFile(Integer frequencyImportId);
}
