/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service;

import com.mx.genotipos.dto.Response;
import com.mx.genotipos.dto.Source;

/**
 *
 * @author NS-448
 */
public interface ISourceService {
    
    public Response getActiveSources(Integer userId);
    public Response addSource(Source source, Integer userId);
    public Response updateSource(Source source);
    public Response deleteSource(Integer sourceId);

}
