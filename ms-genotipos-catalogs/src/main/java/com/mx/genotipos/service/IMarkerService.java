/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service;

import com.mx.genotipos.dto.Marker;
import com.mx.genotipos.dto.Response;
import com.mx.genotipos.dto.Source;

/**
 *
 * @author NS-448
 */
public interface IMarkerService {
    
    public Response getActiveMarkers(Integer userId);
    public Response addMarker(Marker marker, Integer userId);
    public Response updateMarker(Marker marker);
    public Response deleteMarker(Integer markerId);
}
