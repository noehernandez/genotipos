/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service;

import com.mx.genotipos.dto.Label;
import com.mx.genotipos.dto.Response;

/**
 *
 * @author NS-448
 */
public interface ILabelService {
    
    public Response getActiveLabels(Integer userId);
    public Response addLabel(Label label, Integer userId);
    public Response updateLabel(Label label);
    public Response deleteLabel(Integer labelId);
    public Response getLabelsByCategory(Integer categoryId);
}
