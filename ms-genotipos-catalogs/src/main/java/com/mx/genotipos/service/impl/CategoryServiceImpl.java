/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service.impl;

import com.mx.genotipos.dao.CategoryDAO;
import com.mx.genotipos.dao.LabelDAO;
import com.mx.genotipos.dao.model.Categorias;
import com.mx.genotipos.dto.Category;
import com.mx.genotipos.dto.Response;
import com.mx.genotipos.service.ICategoryService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author NS-448
 */
@Service
public class CategoryServiceImpl implements ICategoryService{
    
    @Autowired
    private CategoryDAO categoryDAO;
    
    @Autowired
    private LabelDAO labelDAO;

    @Override
    public Response getActiveCategories(Integer userId) {
        List<Categorias> categories=categoryDAO.findByBajaLogica(1);
        List<Category> categoriesResponse= new ArrayList<>();
        categories.stream()
                .map(this::categoriesToDTO)
                .forEach(categoriesResponse::add);
        
        return new Response(true,0, "Exitoso", categoriesResponse);
    }

    @Override
    public Response addCategory(Category category, Integer userId) {
        Categorias categories= new Categorias(
                category.getId(),
                category.getName(),
                userId
        );
        categoryDAO.save(categories);
        return new Response(true,0, "Exitoso", null);
    }

    @Override
    public Response updateCategory(Category category) {
        System.out.println("updating category");
        if(category.getId()!=null){
            categoryDAO.save(categoriesToEntity(category));
        }
        return new Response(true,0, "Exitoso", null);
    }

    @Override
    public Response deleteCategory(Integer id) {
        System.out.println("deleting category"+id);
        categoryDAO.updateBajaLogica(0,id);
        return new Response(true,0, "Exitoso", null);
    }
    
    private Category categoriesToDTO(Categorias category){
        Integer labels = labelDAO.findByIdCategoria(category.getCatCategoriaId());
        return new Category(category.getCatCategoriaId(),category.getCatDescripcion(),labels);
    }
    
    private Categorias categoriesToEntity(Category category){
        return new Categorias(category.getId(),category.getName(),1);
    }
    
}
