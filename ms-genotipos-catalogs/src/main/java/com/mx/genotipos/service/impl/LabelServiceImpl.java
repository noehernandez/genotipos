/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service.impl;

import com.mx.genotipos.dao.LabelDAO;
import com.mx.genotipos.dao.model.Categorias;
import com.mx.genotipos.dao.model.Etiquetas;
import com.mx.genotipos.dto.Label;
import com.mx.genotipos.dto.Response;
import com.mx.genotipos.service.ILabelService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.springframework.stereotype.Service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author NS-448
 */
@Service
public class LabelServiceImpl implements ILabelService{

    @Autowired
    private LabelDAO labelDAO;
    
    private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-mm-yyyy");
    
    @Override
    public Response getActiveLabels(Integer userId) {
        List<Label> labelsResponse = new ArrayList<>();
        List<Etiquetas> labels = labelDAO.findByBajaLogica();
        labels.stream().map(this::labelToDTO).forEach(labelsResponse::add);
        return new Response(true, 0, "Exito", labelsResponse);
    }

    @Override
    public Response addLabel(Label label, Integer userId) {
        if(label != null){
            String[] labels=label.getName().split(",");
            for(int x=0; x<labels.length; x++){
                labelDAO.save(new Etiquetas(labels[x],userId,label.getCategoryId()));
            }
        }
        return new Response(true, 0, "Exitoso", null);
    }
    
    @Override
    public Response getLabelsByCategory(Integer categoryId) {
        List<Label> labelsResponse = new ArrayList<>();
        List<Etiquetas> labels = labelDAO.findByCategoria(categoryId);
        labels.stream().map(this::labelToDTO).forEach(labelsResponse::add);
        return new Response(true, 0, "Exito", labelsResponse);
    }

    @Override
    public Response updateLabel(Label label) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Response deleteLabel(Integer labelId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
 
    private Label labelToDTO(Etiquetas etiqueta){
        return new Label(
                etiqueta.getEtiId(), 
                etiqueta.getEtiDescripcion(), 
                etiqueta.getEtiCategoriaId().getCatCategoriaId(),
                0,
                DATE_FORMAT.format(etiqueta.getEtiFechaRegistro()));
    }
    
    private Etiquetas labelToEntity(Label label){
        Categorias category = label.getCategoryId()==null?null:new Categorias(label.getCategoryId());
        return new Etiquetas(label.getId(), label.getName(), 1 ,category);
    }

}
