/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service.impl;

import com.mx.genotipos.dao.SourceDAO;
import com.mx.genotipos.dao.model.Fuentes;
import com.mx.genotipos.dto.Response;
import com.mx.genotipos.dto.Source;
import com.mx.genotipos.service.ISourceService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author NS-448
 */
@Service
public class SourceServiceImpl implements ISourceService {

    @Autowired
    private SourceDAO sourceDAO;

    @Override
    public Response getActiveSources(Integer userId) {
        List<Source> sourcesResponse = new ArrayList<>();
        List<Fuentes> sources = sourceDAO.findByBajaLogica(1);
        sources.stream().map(this::sourceToDTO).forEach(sourcesResponse::add);
        return new Response(true, 0, "Exito", sourcesResponse);
    }

    @Override
    public Response addSource(Source source, Integer userId) {
        sourceDAO.save(sourceToEntity(source));
        return new Response(true, 0, "Exito", null);
    }

    @Override
    public Response updateSource(Source source) {
        if(source.getId()!=null){
            System.out.println("updating .."+source.getId());
            sourceDAO.save(sourceToEntity(source));
        } 
        return new Response(true, 0, "", null);
    }

    @Override
    public Response deleteSource(Integer sourceId) {
        System.out.println("deleting .."+sourceId);
        sourceDAO.updateBajaLogica(0, sourceId);
        return new Response(true, 0, "Exito", null);
    }
    
    
    private Source sourceToDTO(Fuentes fuente){
        return new Source(fuente.getFntId(), fuente.getFntNombreFuente(), fuente.getFntIdentificadorInterno(), 
                fuente.getFntIdentificadorExterno(), fuente.getFntNombreContacto(), fuente.getFntCorreoContacto(), 
                fuente.getFntTelefonoContacto(),fuente.getFntOtroTelefonoContacto());
    }
    
    private Fuentes sourceToEntity(Source source){
        return new Fuentes(source.getId(), source.getName(), source.getInternalId(), source.getContactName(), 
                source.getContactEmail(),source.getContactPhone(), source.getOtherContactPhone(), source.getExternalId());
    }

    
    
}
