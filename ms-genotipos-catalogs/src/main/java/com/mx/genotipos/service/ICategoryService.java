/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service;

import com.mx.genotipos.dto.Category;
import com.mx.genotipos.dto.Response;

/**
 *
 * @author NS-448
 */
public interface ICategoryService {
    
    public Response getActiveCategories(Integer userId);
    public Response addCategory(Category category,Integer userId);
    public Response updateCategory(Category category);
    public Response deleteCategory(Integer id);
}
