/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service.impl;

import com.mx.genotipos.dao.MarkerDAO;
import com.mx.genotipos.dao.model.CatMarcadores;
import com.mx.genotipos.dao.model.Fuentes;
import com.mx.genotipos.dto.Marker;
import com.mx.genotipos.dto.Response;
import com.mx.genotipos.dto.Source;
import com.mx.genotipos.service.IMarkerService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author NS-448
 */
@Service
public class MarkerServiceImpl implements IMarkerService{

    @Autowired
    private MarkerDAO markerDAO;

    @Override
    public Response getActiveMarkers(Integer userId) {
        List<Marker> markersResponse = new ArrayList<>();
        List<CatMarcadores> markers = markerDAO.findByBajaLogica();
        markers.stream().map(this::markerToDTO).forEach(markersResponse::add);
        return new Response(true, 0, "Exitoso", markersResponse);
    }

    @Override
    public Response addMarker(Marker marker, Integer userId) {
    	markerDAO.save(markerToEntity(marker));   
        return new Response(true,0, "Exitoso", null);
    }

    @Override
    public Response updateMarker(Marker marker) {
    	  if(marker.getId()!=null){
              System.out.println("updating .."+marker.getId());
              markerDAO.save(markerToEntity(marker));
          } 
          return new Response(true, 0, "", null);
    }

    @Override
    public Response deleteMarker(Integer markerId) {
    	System.out.println("deleting .."+markerId);
    	markerDAO.updateBajaLogica(0, markerId);
        return new Response(true, 0, "Exito", null);
    }
    
    private Marker markerToDTO(CatMarcadores marker){
        return new Marker(marker.getMarId(), marker.getMarNombre(), marker.getMarTipo().getTmDescripcion(), marker.getMarTipo().getTmId(),
                "Admin", null, marker.getMarFechaRegistro().toString());
    }
    
    private CatMarcadores markerToEntity(Marker markers){
        return new CatMarcadores(markers.getId(), markers.getName(), markers.getType(), markers.getUserWhoCreated(), 
        		markers.getUserWhoEdited(), markers.getDate(),markers.getTypeId());   
        
        
    }

}
