/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service.impl;

import com.mx.genotipos.dao.AlleleDAO;
import com.mx.genotipos.dao.FrequencyDAO;
import com.mx.genotipos.dao.MarkerDAO;
import com.mx.genotipos.dao.model.AlelosFrecuencia;
import com.mx.genotipos.dao.model.CatAlelos;
import com.mx.genotipos.dao.model.CatFrecuencias;
import com.mx.genotipos.dao.model.CatMarcadores;
import com.mx.genotipos.dto.FrequencyImport;
import com.mx.genotipos.dto.Response;
import com.mx.genotipos.service.IFrequencyService;
import com.mx.genotipos.utils.ExcelManager;
import com.mx.genotipos.utils.FileManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.StreamSupport;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author NS-448
 */
@Service
public class FrequencyServiceImpl implements IFrequencyService {

    @Autowired
    private FrequencyDAO frequencyDAO;

    @Autowired
    private FileManager fileManager;

    @Autowired
    private ExcelManager excelManager;

    @Autowired
    private AlleleDAO alleleDAO;

    @Autowired
    private MarkerDAO markerDAO;

    //private variables
    private List<Integer> allelesIds;
    private HashMap<String, Integer> markersMap;
    private CatFrecuencias frequency;

    @Override
    public Response getActiveFrequencyImports(Integer userId) {
        List<FrequencyImport> frequencyImportsResponse = new ArrayList<>();
        List<CatFrecuencias> frequencyImports = frequencyDAO.findByBajaLogica(1);
        frequencyImports.stream().map(this::frequencyImportToDTO).forEach(frequencyImportsResponse::add);
        return new Response(true, 0, "Exito", frequencyImportsResponse);
    }

    @Override
    public Response addFrequency(MultipartFile file, String name, Integer userId) {
        try {
            frequency = new CatFrecuencias(name, file.getOriginalFilename(), saveFile(file),1);
            List<AlelosFrecuencia> allelesFrequency = getFrequenciesFromSheet(excelManager.getSheet(file, 0));
            frequency.setAlelosFrecuenciaList(allelesFrequency);
            frequencyDAO.save(frequency);
            return new Response(true, 0, "Exito", null);
        } catch (IOException ex) {
            ex.printStackTrace();
            return new Response(true, 0, "Error al leer el archivo", null);
        }

    }

    @Override
    public Response deleteFrequency(Integer frequencyId) {
        System.out.println("Actualizando frecuencias ... " + frequencyId);
        frequencyDAO.updateFreBajaLogica(0, frequencyId);
        return new Response(true, 0, "Exito", null);
    }

    @Override
    public Response assignFrequencyByDefault(Integer frequencyImportId) {
        System.out.println("asignando por default .." + frequencyImportId);
        frequencyDAO.updateDefaultAll();
        frequencyDAO.updateDefault(true, frequencyImportId);
        return new Response(true, 0, "", null);
    }

    @Override
    public ResponseEntity<Resource> getFrequencyFile(Integer frequencyImportId) {
        CatFrecuencias frequency = frequencyDAO.findById(frequencyImportId).orElse(null);
        try {
            return fileManager.getResponseEntityFile(frequency.getFreRutaArchivo());
        } catch (IOException ex) {
            Logger.getLogger(FrequencyServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private FrequencyImport frequencyImportToDTO(CatFrecuencias frequency) {
        return new FrequencyImport(frequency.getFreId(), frequency.getFreIdImportacion(), frequency.getFreNombre(),
                frequency.getFreNombreArchivo(), "Admin", frequency.getFreFechaRegistro().toString(), frequency.getFreDefault());
    }

    private String saveFile(MultipartFile file) {
        String fileName = new SimpleDateFormat("ddMMyy-hhmmss").format(new Date()) + "-" + file.getOriginalFilename();
        String destination = "C:/Genotipos/";
        String filePath = destination + fileName;
        new File(destination).mkdirs();
        try (FileOutputStream fos = new FileOutputStream(filePath)) {
            fos.write(file.getBytes());
            return filePath;
        } catch (FileNotFoundException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    private List<AlelosFrecuencia> getFrequenciesFromSheet(Sheet sheet) {
        List<AlelosFrecuencia> allelesFrequency = new ArrayList<>();
        Row firstRow = sheet.getRow(0);
        allelesIds = getAllelesFromRow(firstRow);
        System.out.println("alleles ID :"+ allelesIds);
        markersMap = getMarkersMap();
        markersMap.forEach((m,n) -> System.out.println("macador "+m+" id: "+n));
        sheet.removeRow(firstRow);
        StreamSupport.stream(sheet.spliterator(), false)
                .map(this::getFrequenciesFromRow)
                .forEach(allelesFrequency::addAll);
        return allelesFrequency;
    }

    private List<AlelosFrecuencia> getFrequenciesFromRow(Row row) {
        List<AlelosFrecuencia> allelesFrequency = new ArrayList<>();
        Cell firstCell = row.getCell(0);
        String marker = excelManager.getCellValue(firstCell);
        row.removeCell(firstCell);
        if (markersMap.containsKey(marker)) {
            Integer markerId = markersMap.get(marker);
            for (Cell cell : row) {
                allelesFrequency.add(createAlelosFrecuenciaFromCell(cell, markerId));
            }
        } else {
            System.out.println("No existe el marcador : "+marker);
        }
        return allelesFrequency;
    }

    private AlelosFrecuencia createAlelosFrecuenciaFromCell(Cell cell, Integer markerId) {
        String cellValue = excelManager.getCellValue(cell);
        if (!cellValue.equals("")) {
            Double alleleValue = Double.parseDouble(cellValue);
            try{
                return new AlelosFrecuencia(alleleValue, allelesIds.get(cell.getColumnIndex() - 1), markerId,frequency);
            }catch(Exception e){
                System.out.println("Error en "+ alleleValue+ "column "+ cell.getColumnIndex()+" marker "+markerId);
            }
//            return new AlelosFrecuencia(alleleValue, allelesIds.get(cell.getColumnIndex() - 1), markerId,frequency);
        }
        return null;
    }

    private List<Integer> getAllelesFromRow(Row row) {
        List<Integer> allelesIds = new ArrayList();
        HashMap<String, Integer> allelesMap = getAllelesMap();
        for (Cell cell : row) {
            String allele = excelManager.getCellValue(cell);
            if (allelesMap.containsKey(allele)) {
                allelesIds.add(allelesMap.get(allele));
            } else {
                //regresar una exepcion para mostrar el error al usuario
                System.out.println("No existe el alelo en la base de datos : "+allele);
            }
        }
        System.out.println(allelesIds.toString());
        return allelesIds;
    }

    private HashMap<String, Integer> getAllelesMap() {
        HashMap<String, Integer> allelesMap = new HashMap();
        List<CatAlelos> alleles = alleleDAO.findByBajaLogica(1);
        alleles.forEach(catAlelo -> allelesMap.put(catAlelo.getAleDescripcion(), catAlelo.getAleId()));
        return allelesMap;
    }

    private HashMap<String, Integer> getMarkersMap() {
        HashMap<String, Integer> markersMap = new HashMap();
        List<CatMarcadores> markers = markerDAO.findByBajaLogica();
        markers.forEach(catMarcadores -> markersMap.put(catMarcadores.getMarNombre(), catMarcadores.getMarId()));
        return markersMap;
    }

}
