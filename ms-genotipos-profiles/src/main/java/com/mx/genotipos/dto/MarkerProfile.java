/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

import java.util.Date;
import lombok.*;

/**
 *
 * @author NS-448
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarkerProfile {
    
    private Integer id;
    private String alele1;
    private String alele2;
    private String name;

    public MarkerProfile(String alele1, String alele2, String name) {
        this.alele1 = alele1;
        this.alele2 = alele2;
        this.name = name;
    }
    
    

}
