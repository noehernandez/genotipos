/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author NS-448
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Profile {
    
    private Integer id;
    private String internalId;
    private String externalId;
    private Integer markers;
    private Integer homocigotos;
    private String user;
    private String status;
    private String registrationDate;
    private List<String> metadata;
    private List<MarkerProfile> markersProfile;

    public Profile(Integer id, String internalId, String externalId) {
        this.id = id;
        this.internalId = internalId;
        this.externalId = externalId;
    }
    
    
    
}
