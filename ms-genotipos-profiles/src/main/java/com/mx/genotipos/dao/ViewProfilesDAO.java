/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao;

import com.mx.genotipos.dao.model.VistaPerfiles;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author NS-448
 */
@Transactional
@Repository
public interface ViewProfilesDAO extends JpaRepository<VistaPerfiles,Integer>{
    
    @Query("SELECT v FROM VistaPerfiles v WHERE v.perIdImportacion = :perIdImportacion")
    public List<VistaPerfiles> findByIdImportacion(@Param("perIdImportacion") Integer perIdImportacion);
    
    @Query("SELECT v FROM VistaPerfiles v WHERE v.perId = :profileId")
    public VistaPerfiles findByPerfilId(@Param("profileId") Integer profileId);
    
//    falta de actualizar
    @Query("SELECT v FROM VistaPerfiles v WHERE v.perBajaLogica = :perBajaLogica")
    public List<VistaPerfiles> findByUserId(@Param("perBajaLogica") Integer perBajaLogica);
}
