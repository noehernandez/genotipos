/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "VISTA_PERFILES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VistaPerfiles.findAll", query = "SELECT v FROM VistaPerfiles v")
    , @NamedQuery(name = "VistaPerfiles.findByPerId", query = "SELECT v FROM VistaPerfiles v WHERE v.perId = :perId")
    , @NamedQuery(name = "VistaPerfiles.findByPerIdentificador", query = "SELECT v FROM VistaPerfiles v WHERE v.perIdentificador = :perIdentificador")
    , @NamedQuery(name = "VistaPerfiles.findByPerIdentificadorInterno", query = "SELECT v FROM VistaPerfiles v WHERE v.perIdentificadorInterno = :perIdentificadorInterno")
    , @NamedQuery(name = "VistaPerfiles.findByPerBajaLogica", query = "SELECT v FROM VistaPerfiles v WHERE v.perBajaLogica = :perBajaLogica")
    , @NamedQuery(name = "VistaPerfiles.findByPerFechaRegistro", query = "SELECT v FROM VistaPerfiles v WHERE v.perFechaRegistro = :perFechaRegistro")
    , @NamedQuery(name = "VistaPerfiles.findByPerPerfilOriginal", query = "SELECT v FROM VistaPerfiles v WHERE v.perPerfilOriginal = :perPerfilOriginal")
    , @NamedQuery(name = "VistaPerfiles.findByPerIdImportacion", query = "SELECT v FROM VistaPerfiles v WHERE v.perIdImportacion = :perIdImportacion")
    , @NamedQuery(name = "VistaPerfiles.findByMarcadores", query = "SELECT v FROM VistaPerfiles v WHERE v.marcadores = :marcadores")
    , @NamedQuery(name = "VistaPerfiles.findByHomocigotos", query = "SELECT v FROM VistaPerfiles v WHERE v.homocigotos = :homocigotos")
    , @NamedQuery(name = "VistaPerfiles.findByUsuario", query = "SELECT v FROM VistaPerfiles v WHERE v.usuario = :usuario")
    , @NamedQuery(name = "VistaPerfiles.findByTpId", query = "SELECT v FROM VistaPerfiles v WHERE v.tpId = :tpId")
    , @NamedQuery(name = "VistaPerfiles.findByTpDescripcion", query = "SELECT v FROM VistaPerfiles v WHERE v.tpDescripcion = :tpDescripcion")})
public class VistaPerfiles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "per_id")
    @Id
    private int perId;
    @Column(name = "per_identificador")
    private String perIdentificador;
    @Column(name = "per_identificador_interno")
    private String perIdentificadorInterno;
    @Column(name = "per_baja_logica")
    private Integer perBajaLogica;
    @Column(name = "per_fecha_registro")
    private String perFechaRegistro;
    @Column(name = "per_perfil_original")
    private Integer perPerfilOriginal;
    @Column(name = "per_id_importacion")
    private Integer perIdImportacion;
    @Column(name = "marcadores")
    private Integer marcadores;
    @Column(name = "homocigotos")
    private Integer homocigotos;
    @Column(name = "usuario")
    private String usuario;
    @Column(name = "tp_id")
    private Integer tpId;
    @Column(name = "tp_descripcion")
    private String tpDescripcion;

    public VistaPerfiles() {
    }

    public int getPerId() {
        return perId;
    }

    public void setPerId(int perId) {
        this.perId = perId;
    }

    public String getPerIdentificador() {
        return perIdentificador;
    }

    public void setPerIdentificador(String perIdentificador) {
        this.perIdentificador = perIdentificador;
    }

    public String getPerIdentificadorInterno() {
        return perIdentificadorInterno;
    }

    public void setPerIdentificadorInterno(String perIdentificadorInterno) {
        this.perIdentificadorInterno = perIdentificadorInterno;
    }

    public Integer getPerBajaLogica() {
        return perBajaLogica;
    }

    public void setPerBajaLogica(Integer perBajaLogica) {
        this.perBajaLogica = perBajaLogica;
    }

    public String getPerFechaRegistro() {
        return perFechaRegistro;
    }

    public void setPerFechaRegistro(String perFechaRegistro) {
        this.perFechaRegistro = perFechaRegistro;
    }

    public Integer getPerPerfilOriginal() {
        return perPerfilOriginal;
    }

    public void setPerPerfilOriginal(Integer perPerfilOriginal) {
        this.perPerfilOriginal = perPerfilOriginal;
    }

    public Integer getPerIdImportacion() {
        return perIdImportacion;
    }

    public void setPerIdImportacion(Integer perIdImportacion) {
        this.perIdImportacion = perIdImportacion;
    }

    public Integer getMarcadores() {
        return marcadores;
    }

    public void setMarcadores(Integer marcadores) {
        this.marcadores = marcadores;
    }

    public Integer getHomocigotos() {
        return homocigotos;
    }

    public void setHomocigotos(Integer homocigotos) {
        this.homocigotos = homocigotos;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getTpId() {
        return tpId;
    }

    public void setTpId(Integer tpId) {
        this.tpId = tpId;
    }

    public String getTpDescripcion() {
        return tpDescripcion;
    }

    public void setTpDescripcion(String tpDescripcion) {
        this.tpDescripcion = tpDescripcion;
    }
    
}
