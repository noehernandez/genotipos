/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "MARCADORES_PERFILES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MarcadoresPerfiles.findAll", query = "SELECT m FROM MarcadoresPerfiles m")
    , @NamedQuery(name = "MarcadoresPerfiles.findByMpId", query = "SELECT m FROM MarcadoresPerfiles m WHERE m.mpId = :mpId")
    , @NamedQuery(name = "MarcadoresPerfiles.findByMpAleloUno", query = "SELECT m FROM MarcadoresPerfiles m WHERE m.mpAleloUno = :mpAleloUno")
    , @NamedQuery(name = "MarcadoresPerfiles.findByMpAleloDos", query = "SELECT m FROM MarcadoresPerfiles m WHERE m.mpAleloDos = :mpAleloDos")
    , @NamedQuery(name = "MarcadoresPerfiles.findByMpBajaLogica", query = "SELECT m FROM MarcadoresPerfiles m WHERE m.mpBajaLogica = :mpBajaLogica")
    , @NamedQuery(name = "MarcadoresPerfiles.findByMpFechaRegistro", query = "SELECT m FROM MarcadoresPerfiles m WHERE m.mpFechaRegistro = :mpFechaRegistro")})
public class MarcadoresPerfiles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "mp_id")
    private Integer mpId;
    @Column(name = "mp_alelo_uno")
    private String mpAleloUno;
    @Column(name = "mp_alelo_dos")
    private String mpAleloDos;
    @Column(name = "mp_baja_logica")
    private Integer mpBajaLogica;
    @Column(name = "mp_fecha_registro")
    @Temporal(TemporalType.DATE)
    private Date mpFechaRegistro;
    @JoinColumn(name = "mp_id_marcador", referencedColumnName = "mar_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CatMarcadores mpIdMarcador;
    @JoinColumn(name = "mp_id_perfil", referencedColumnName = "per_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Perfiles mpIdPerfil;

    public MarcadoresPerfiles() {
    }

    public MarcadoresPerfiles(Integer mpId) {
        this.mpId = mpId;
    }

    public Integer getMpId() {
        return mpId;
    }

    public void setMpId(Integer mpId) {
        this.mpId = mpId;
    }

    public String getMpAleloUno() {
        return mpAleloUno;
    }

    public void setMpAleloUno(String mpAleloUno) {
        this.mpAleloUno = mpAleloUno;
    }

    public String getMpAleloDos() {
        return mpAleloDos;
    }

    public void setMpAleloDos(String mpAleloDos) {
        this.mpAleloDos = mpAleloDos;
    }

    public Integer getMpBajaLogica() {
        return mpBajaLogica;
    }

    public void setMpBajaLogica(Integer mpBajaLogica) {
        this.mpBajaLogica = mpBajaLogica;
    }

    public Date getMpFechaRegistro() {
        return mpFechaRegistro;
    }

    public void setMpFechaRegistro(Date mpFechaRegistro) {
        this.mpFechaRegistro = mpFechaRegistro;
    }

    public CatMarcadores getMpIdMarcador() {
        return mpIdMarcador;
    }

    public void setMpIdMarcador(CatMarcadores mpIdMarcador) {
        this.mpIdMarcador = mpIdMarcador;
    }

    public Perfiles getMpIdPerfil() {
        return mpIdPerfil;
    }

    public void setMpIdPerfil(Perfiles mpIdPerfil) {
        this.mpIdPerfil = mpIdPerfil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mpId != null ? mpId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MarcadoresPerfiles)) {
            return false;
        }
        MarcadoresPerfiles other = (MarcadoresPerfiles) object;
        if ((this.mpId == null && other.mpId != null) || (this.mpId != null && !this.mpId.equals(other.mpId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mx.genotipos.dao.model.MarcadoresPerfiles[ mpId=" + mpId + " ]";
    }
    
}
