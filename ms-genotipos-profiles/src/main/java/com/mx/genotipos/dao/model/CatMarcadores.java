/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "CAT_MARCADORES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatMarcadores.findAll", query = "SELECT c FROM CatMarcadores c")
    , @NamedQuery(name = "CatMarcadores.findByMarId", query = "SELECT c FROM CatMarcadores c WHERE c.marId = :marId")
    , @NamedQuery(name = "CatMarcadores.findByMarNombre", query = "SELECT c FROM CatMarcadores c WHERE c.marNombre = :marNombre")
    , @NamedQuery(name = "CatMarcadores.findByMarBajaLogica", query = "SELECT c FROM CatMarcadores c WHERE c.marBajaLogica = :marBajaLogica")
    , @NamedQuery(name = "CatMarcadores.findByMarFechaRegistro", query = "SELECT c FROM CatMarcadores c WHERE c.marFechaRegistro = :marFechaRegistro")
    , @NamedQuery(name = "CatMarcadores.findByMarUsuarioCreo", query = "SELECT c FROM CatMarcadores c WHERE c.marUsuarioCreo = :marUsuarioCreo")
    , @NamedQuery(name = "CatMarcadores.findByMarUsuarioEdito", query = "SELECT c FROM CatMarcadores c WHERE c.marUsuarioEdito = :marUsuarioEdito")})
public class CatMarcadores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "mar_id")
    private Integer marId;
    @Column(name = "mar_nombre")
    private String marNombre;
    @Column(name = "mar_baja_logica")
    private Integer marBajaLogica;
    @Column(name = "mar_fecha_registro")
    @Temporal(TemporalType.DATE)
    private Date marFechaRegistro;
    @Column(name = "mar_usuario_creo")
    private Integer marUsuarioCreo;
    @Column(name = "mar_usuario_edito")
    private Integer marUsuarioEdito;
    @JoinColumn(name = "mar_tipo", referencedColumnName = "tm_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CatTipoMarcador marTipo;
    @OneToMany(mappedBy = "mpIdMarcador", fetch = FetchType.LAZY)
    private List<MarcadoresPerfiles> marcadoresPerfilesList;

    public CatMarcadores() {
    }

    public CatMarcadores(Integer marId) {
        this.marId = marId;
    }

    public Integer getMarId() {
        return marId;
    }

    public void setMarId(Integer marId) {
        this.marId = marId;
    }

    public String getMarNombre() {
        return marNombre;
    }

    public void setMarNombre(String marNombre) {
        this.marNombre = marNombre;
    }

    public Integer getMarBajaLogica() {
        return marBajaLogica;
    }

    public void setMarBajaLogica(Integer marBajaLogica) {
        this.marBajaLogica = marBajaLogica;
    }

    public Date getMarFechaRegistro() {
        return marFechaRegistro;
    }

    public void setMarFechaRegistro(Date marFechaRegistro) {
        this.marFechaRegistro = marFechaRegistro;
    }

    public Integer getMarUsuarioCreo() {
        return marUsuarioCreo;
    }

    public void setMarUsuarioCreo(Integer marUsuarioCreo) {
        this.marUsuarioCreo = marUsuarioCreo;
    }

    public Integer getMarUsuarioEdito() {
        return marUsuarioEdito;
    }

    public void setMarUsuarioEdito(Integer marUsuarioEdito) {
        this.marUsuarioEdito = marUsuarioEdito;
    }

    public CatTipoMarcador getMarTipo() {
        return marTipo;
    }

    public void setMarTipo(CatTipoMarcador marTipo) {
        this.marTipo = marTipo;
    }

    @XmlTransient
    public List<MarcadoresPerfiles> getMarcadoresPerfilesList() {
        return marcadoresPerfilesList;
    }

    public void setMarcadoresPerfilesList(List<MarcadoresPerfiles> marcadoresPerfilesList) {
        this.marcadoresPerfilesList = marcadoresPerfilesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (marId != null ? marId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatMarcadores)) {
            return false;
        }
        CatMarcadores other = (CatMarcadores) object;
        if ((this.marId == null && other.marId != null) || (this.marId != null && !this.marId.equals(other.marId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mx.genotipos.dao.model.CatMarcadores[ marId=" + marId + " ]";
    }
    
}
