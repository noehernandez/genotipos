/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service;

import com.mx.genotipos.dto.Response;

/**
 *
 * @author NS-448
 */
public interface IProfilesService {
    
    public Response getImportedProfiles(Integer importId);
    public Response getDetailProfile(Integer profileId);
    public Response getActiveProfiles(Integer userId);
}
