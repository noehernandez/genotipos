/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service.impl;

import com.mx.genotipos.dao.ProfilesDAO;
import com.mx.genotipos.dao.model.VistaPerfiles;
import com.mx.genotipos.dto.Profile;
import com.mx.genotipos.dto.Response;
import com.mx.genotipos.service.IProfilesService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mx.genotipos.dao.ViewProfilesDAO;
import com.mx.genotipos.dao.model.MarcadoresPerfiles;
import com.mx.genotipos.dao.model.Perfiles;
import com.mx.genotipos.dto.MarkerProfile;
import com.mx.genotipos.dto.Item;

/**
 *
 * @author NS-448
 */
@Service
public class ProfilesServiceImpl implements IProfilesService{

    @Autowired
    private ViewProfilesDAO viewProfilesDAO;
    
    @Autowired
    private ProfilesDAO profilesDAO;
    
    @Override
    public Response getImportedProfiles(Integer importId) {
        System.out.println("extract profiles...");
        List<Profile> profilesResponse= new ArrayList<>();
        List<VistaPerfiles> profiles = viewProfilesDAO.findByIdImportacion(importId);
        profiles.stream().map(this::viewProfileToDTO)
                .forEach(profilesResponse::add);
        return new Response(true, 0, "Exito", profilesResponse);
    }
    
    @Override
    public Response getDetailProfile(Integer profileId) {
        System.out.println("buscando perfil ..."+profileId);
        VistaPerfiles viewProfile = viewProfilesDAO.findByPerfilId(profileId);
        Profile profileResponse = viewProfileToDTO(viewProfile);
        Perfiles profile = profilesDAO.findById(profileId).orElse(null);
        return new Response(true, 0, "Exitoso", setMarkersAndLabelsToProfile(profile,profileResponse));
    }
    
    @Override
    public Response getActiveProfiles(Integer userId) {
        System.out.println("buscando perfiles ..."+userId);
        List<VistaPerfiles> viewProfiles = viewProfilesDAO.findByUserId(userId);
        List<Item> profilesResponse = viewProfilesToItems(viewProfiles);
        System.out.println("exito");
        return new Response(true, 0, "Exitoso", profilesResponse);
    }

    private List<Item> viewProfilesToItems(List<VistaPerfiles> viewProfiles){
        List<Item> profiles= new ArrayList<>();
        viewProfiles.stream()
                .map(this::viewProfileToItem)
                .forEach(profiles::add);
        return profiles;
    }
    
    private Profile viewProfileToDTO(VistaPerfiles profile){
        return new Profile(
                profile.getPerId(),
                profile.getPerIdentificadorInterno(), 
                profile.getPerIdentificador(), 
                profile.getMarcadores(), 
                profile.getHomocigotos(), 
                profile.getUsuario(), 
                profile.getTpDescripcion(),
                profile.getPerFechaRegistro(),
                null,
                null
        );
    }
    
    private Item viewProfileToItem(VistaPerfiles profile){
        return new Item(
                profile.getPerId(),
                profile.getPerIdentificador()
        );
    }
    
    private Profile setMarkersAndLabelsToProfile(Perfiles profile, Profile profileResponse){
        List<MarkerProfile> markersProfile = new ArrayList();
        markersProfile = markersToDTO(profile.getMarcadoresPerfilesList());
        profileResponse.setMarkersProfile(markersProfile);
        return profileResponse;
    }
    
    private List<MarkerProfile> markersToDTO(List<MarcadoresPerfiles> markers){
        List<MarkerProfile> markersResponse= new ArrayList<>();
        markers.stream().map(this::markerToDTO)
                .forEach(markersResponse::add);
        return markersResponse;
    }
    
    private MarkerProfile markerToDTO(MarcadoresPerfiles marker){
        return new MarkerProfile(marker.getMpAleloUno(), marker.getMpAleloDos(), marker.getMpIdMarcador().getMarNombre());
    }

}
