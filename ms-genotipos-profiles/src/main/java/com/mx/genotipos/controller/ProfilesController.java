/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.controller;

import com.mx.genotipos.dto.Response;
import com.mx.genotipos.service.IProfilesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author NS-448
 */
@RestController
@RequestMapping("/api/profiles")
@CrossOrigin(origins = {"http://localhost:4200","http://3.17.128.176:4200","*"} , allowedHeaders = "*")
public class ProfilesController {
    
    @Autowired
    private IProfilesService profilesService;
    
    @GetMapping("/{importId}")
    public Response getImportedProfiles(@PathVariable Integer importId){
        return profilesService.getImportedProfiles(importId);
    }
    
    @GetMapping("/detail/{profileId}")
    public Response getDetailProfile(@PathVariable Integer profileId){
        return profilesService.getDetailProfile(profileId);
    }
    
    @GetMapping("/active/{userId}")
    public Response getActiveProfiles(@PathVariable Integer userId){
        return profilesService.getActiveProfiles(userId);
    }
}
