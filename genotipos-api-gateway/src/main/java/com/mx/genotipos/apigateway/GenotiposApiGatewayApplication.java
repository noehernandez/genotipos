package com.mx.genotipos.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class GenotiposApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GenotiposApiGatewayApplication.class, args);
	}

}
