/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author NS-448
 */
@Data
@NoArgsConstructor
public class SearchResult {
    private List<Label> etiquetasObjetivo;
    private List<Label> etiquetasAComparar;
    private Integer idPerfil;
    private Integer idPerfilProgenitor;
    private Integer idPerfilSeguro;
    private Integer idPerfilHijo;
    private String identificadorPerfil;
    private Integer idTablaFrecuencia;
    private Integer idFuente;
    private String tablaFrecuencia;
    private String fuente;
    private String motivo;
    private Integer numeroExclusiones;
    private String descripcion;
    private Integer numeroMarcadores;
    private boolean descartarPerfilesRevision;
    private Integer idUsuario;
    private String usuario;
    private String tipo;
    private String numero;
    private String fecha;
    private String estatus;
    private Integer id;
    private Integer tipoBusqueda;
    private List<Result> busquedas;
//    private List<Result> resultados;
}
