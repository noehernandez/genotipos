/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

/**
 *
 * @author NS-448
 */
public class PerfilCompatible {
    
    private String alelo1;
    private Integer coincideAlelo1;
    private String alelo2;
    private Integer coincideAlelo2;

    public PerfilCompatible() {
    }

    public PerfilCompatible(String alelo1, Integer coincideAlelo1, String alelo2, Integer coincideAlelo2) {
        this.alelo1 = alelo1;
        this.coincideAlelo1 = coincideAlelo1;
        this.alelo2 = alelo2;
        this.coincideAlelo2 = coincideAlelo2;
    }

    public String getAlelo1() {
        return alelo1;
    }

    public void setAlelo1(String alelo1) {
        this.alelo1 = alelo1;
    }

    public Integer getCoincideAlelo1() {
        return coincideAlelo1;
    }

    public void setCoincideAlelo1(Integer coincideAlelo1) {
        this.coincideAlelo1 = coincideAlelo1;
    }

    public String getAlelo2() {
        return alelo2;
    }

    public void setAlelo2(String alelo2) {
        this.alelo2 = alelo2;
    }

    public Integer getCoincideAlelo2() {
        return coincideAlelo2;
    }

    public void setCoincideAlelo2(Integer coincideAlelo2) {
        this.coincideAlelo2 = coincideAlelo2;
    }
}
