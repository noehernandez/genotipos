/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author NS-448
 */
@Data
@NoArgsConstructor
public class Result {
    private PerfilesCompatibles resultado;
    private List<MarcadoresCompatibles> comparativoMarcadores;
}
