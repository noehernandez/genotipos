/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author NS-448
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarcadoresCompatibles {
    private String marcador;
    private Integer idMarcador;
    private Objetivo objetivo;
    private PerfilCompatible compatible;
    private PerfilCompatible perfilPadre;
    private PerfilCompatible perfilMadre;
    private String aleloXOrigen;
    private String aleloYOrigen;
    private String aleloXProgenitor;
    private String aleloYProgenitor;
}
