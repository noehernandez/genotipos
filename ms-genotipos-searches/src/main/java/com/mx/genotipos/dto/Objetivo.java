/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

/**
 *
 * @author NS-448
 */
public class Objetivo {
    
    private String alelo1;
    private String alelo2;

    public Objetivo(String alelo1, String alelo2) {
        this.alelo1 = alelo1;
        this.alelo2 = alelo2;
    }

    public Objetivo() {
    }
    
    

    public String getAlelo1() {
        return alelo1;
    }

    public void setAlelo1(String alelo1) {
        this.alelo1 = alelo1;
    }

    public String getAlelo2() {
        return alelo2;
    }

    public void setAlelo2(String alelo2) {
        this.alelo2 = alelo2;
    }
    
    
}
