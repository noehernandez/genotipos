/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author NS-448
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Busqueda {
    private Integer id;
    private String numero;
    private String fuente;
    private String motivo;
    private String descripcion;
    private String usuario;
    private String tipoBusqueda;
    private String estatus;
    private String fecha;
}
