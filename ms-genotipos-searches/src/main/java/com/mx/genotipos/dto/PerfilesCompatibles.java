/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author NS-448
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PerfilesCompatibles {
    private Integer id;
    private String genotipoObjetivo;
    private String genotipoCompatible;
    private String perfilPadre;
    private String perfilMadre;
    private String perfilObjetivo;
    private String estado;
    private String amel;
    private String ip;
    private String pp;
    private List<Metadato> metadatosObjetivo;
    private List<Metadato> metadatosPadre;
    private List<Metadato> metadatosMadre;
    private List<MarcadoresCompatibles> marcadoresCompatibles;

    public PerfilesCompatibles(Integer id, String genotipoObjetivo, String genotipoCompatible, String perfilPadre, String perfilMadre, String perfilObjetivo, String estado, String amel, String ip, String pp) {
        this.id = id;
        this.genotipoObjetivo = genotipoObjetivo;
        this.genotipoCompatible = genotipoCompatible;
        this.perfilPadre = perfilPadre;
        this.perfilMadre = perfilMadre;
        this.perfilObjetivo = perfilObjetivo;
        this.estado = estado;
        this.amel = amel;
        this.ip = ip;
        this.pp = pp;
        this.metadatosMadre= new ArrayList<>();
        this.metadatosObjetivo = new ArrayList<>();
        this.metadatosPadre = new ArrayList<>();
    }
    
    
}
