/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author NS-448
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DetalleBusqueda extends Busqueda{
    private String conclusiones;
    private String marcadores;
    private String exclusiones;
    private String tablaFrecuencias;
    private Integer descartarPerfilesRevision;
    private List<String> etiquetasObjetivo;
    private List<String> etiquetasAComparar;
    private List<PerfilesCompatibles> perfilesCompatibles;

    public DetalleBusqueda(String conclusiones, String marcadores, String exclusiones, String tablaFrecuencias, Integer descartarPerfilesRevision, List<String> etiquetasObjetivo, List<String> etiquetasAComparar, List<PerfilesCompatibles> perfilesCompatibles, Integer id, String numero, String fuente, String motivo, String descripcion, String usuario, String tipoBusqueda, String estatus, String fecha) {
        super(id, numero, fuente, motivo, descripcion, usuario, tipoBusqueda, estatus, fecha);
        this.conclusiones = conclusiones;
        this.marcadores = marcadores;
        this.exclusiones = exclusiones;
        this.tablaFrecuencias = tablaFrecuencias;
        this.descartarPerfilesRevision = descartarPerfilesRevision;
        this.etiquetasObjetivo = etiquetasObjetivo;
        this.etiquetasAComparar = etiquetasAComparar;
        this.perfilesCompatibles = perfilesCompatibles;
    }
}
