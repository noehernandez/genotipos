/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "RESULTADO_COMPATIBLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ResultadoCompatible.findAll", query = "SELECT r FROM ResultadoCompatible r")
    , @NamedQuery(name = "ResultadoCompatible.findByRcId", query = "SELECT r FROM ResultadoCompatible r WHERE r.rcId = :rcId")
    , @NamedQuery(name = "ResultadoCompatible.findByEdoId", query = "SELECT r FROM ResultadoCompatible r WHERE r.edoId = :edoId")
    , @NamedQuery(name = "ResultadoCompatible.findByRcPerfilObjetivo", query = "SELECT r FROM ResultadoCompatible r WHERE r.rcPerfilObjetivo = :rcPerfilObjetivo")
    , @NamedQuery(name = "ResultadoCompatible.findByRcPerfilCompatible", query = "SELECT r FROM ResultadoCompatible r WHERE r.rcPerfilCompatible = :rcPerfilCompatible")
    , @NamedQuery(name = "ResultadoCompatible.findByRcIp", query = "SELECT r FROM ResultadoCompatible r WHERE r.rcIp = :rcIp")
    , @NamedQuery(name = "ResultadoCompatible.findByRcPp", query = "SELECT r FROM ResultadoCompatible r WHERE r.rcPp = :rcPp")
    , @NamedQuery(name = "ResultadoCompatible.findByRcAmel", query = "SELECT r FROM ResultadoCompatible r WHERE r.rcAmel = :rcAmel")
    , @NamedQuery(name = "ResultadoCompatible.findByRcPerfilMadre", query = "SELECT r FROM ResultadoCompatible r WHERE r.rcPerfilMadre = :rcPerfilMadre")
    , @NamedQuery(name = "ResultadoCompatible.findByRcPerfilPadre", query = "SELECT r FROM ResultadoCompatible r WHERE r.rcPerfilPadre = :rcPerfilPadre")})
public class ResultadoCompatible implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name = "rc_id")
    private Integer rcId;
    @Column(name = "edo_id")
    private Integer edoId;
    @Column(name = "rc_perfil_objetivo")
    private Integer rcPerfilObjetivo;
    @Column(name = "rc_perfil_compatible")
    private Integer rcPerfilCompatible;
    @Column(name = "rc_ip")
    private String rcIp;
    @Column(name = "rc_pp")
    private String rcPp;
    @Column(name = "rc_amel")
    private String rcAmel;
    @Column(name = "rc_perfil_madre")
    private Integer rcPerfilMadre;
    @Column(name = "rc_perfil_padre")
    private Integer rcPerfilPadre;
    @JoinColumn(name = "rc_bus_id", referencedColumnName = "bus_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Busquedas rcBusId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mcRcId", fetch = FetchType.LAZY)
    private List<MarcadorCompatible> marcadorCompatibleList;

    public ResultadoCompatible() {
    }

    public ResultadoCompatible(Integer edoId, Integer rcPerfilObjetivo, Integer rcPerfilCompatible, String rcIp, String rcPp, String rcAmel, Integer rcPerfilMadre, Integer rcPerfilPadre, Busquedas rcBusId) {
        this.edoId = edoId;
        this.rcPerfilObjetivo = rcPerfilObjetivo;
        this.rcPerfilCompatible = rcPerfilCompatible;
        this.rcIp = rcIp;
        this.rcPp = rcPp;
        this.rcAmel = rcAmel;
        this.rcPerfilMadre = rcPerfilMadre;
        this.rcPerfilPadre = rcPerfilPadre;
        this.rcBusId = rcBusId;
    }
    
    

    public ResultadoCompatible(Integer rcId) {
        this.rcId = rcId;
    }

    public Integer getRcId() {
        return rcId;
    }

    public void setRcId(Integer rcId) {
        this.rcId = rcId;
    }

    public Integer getEdoId() {
        return edoId;
    }

    public void setEdoId(Integer edoId) {
        this.edoId = edoId;
    }

    public Integer getRcPerfilObjetivo() {
        return rcPerfilObjetivo;
    }

    public void setRcPerfilObjetivo(Integer rcPerfilObjetivo) {
        this.rcPerfilObjetivo = rcPerfilObjetivo;
    }

    public Integer getRcPerfilCompatible() {
        return rcPerfilCompatible;
    }

    public void setRcPerfilCompatible(Integer rcPerfilCompatible) {
        this.rcPerfilCompatible = rcPerfilCompatible;
    }

    public String getRcIp() {
        return rcIp;
    }

    public void setRcIp(String rcIp) {
        this.rcIp = rcIp;
    }

    public String getRcPp() {
        return rcPp;
    }

    public void setRcPp(String rcPp) {
        this.rcPp = rcPp;
    }

    public String getRcAmel() {
        return rcAmel;
    }

    public void setRcAmel(String rcAmel) {
        this.rcAmel = rcAmel;
    }

    public Integer getRcPerfilMadre() {
        return rcPerfilMadre;
    }

    public void setRcPerfilMadre(Integer rcPerfilMadre) {
        this.rcPerfilMadre = rcPerfilMadre;
    }

    public Integer getRcPerfilPadre() {
        return rcPerfilPadre;
    }

    public void setRcPerfilPadre(Integer rcPerfilPadre) {
        this.rcPerfilPadre = rcPerfilPadre;
    }

    public Busquedas getRcBusId() {
        return rcBusId;
    }

    public void setRcBusId(Busquedas rcBusId) {
        this.rcBusId = rcBusId;
    }

    @XmlTransient
    public List<MarcadorCompatible> getMarcadorCompatibleList() {
        return marcadorCompatibleList;
    }

    public void setMarcadorCompatibleList(List<MarcadorCompatible> marcadorCompatibleList) {
        this.marcadorCompatibleList = marcadorCompatibleList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rcId != null ? rcId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ResultadoCompatible)) {
            return false;
        }
        ResultadoCompatible other = (ResultadoCompatible) object;
        if ((this.rcId == null && other.rcId != null) || (this.rcId != null && !this.rcId.equals(other.rcId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mx.genotipos.dao.model.ResultadoCompatible[ rcId=" + rcId + " ]";
    }
    
}
