/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao;

import com.mx.genotipos.dao.model.VistaBusquedas;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author NS-448
 */
@Transactional
@Repository
public interface VistaBusquedasDAO extends JpaRepository<VistaBusquedas,Integer>{
    
    @Query("SELECT v FROM VistaBusquedas v WHERE v.idUsuario = :idUsuario")
    List<VistaBusquedas> findByIdUsuario(@Param("idUsuario") Integer idUsuario);
    
}
