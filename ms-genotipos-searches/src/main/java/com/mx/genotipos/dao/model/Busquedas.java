/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "BUSQUEDA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Busquedas.findAll", query = "SELECT b FROM Busquedas b")
    , @NamedQuery(name = "Busquedas.findByBusId", query = "SELECT b FROM Busquedas b WHERE b.busId = :busId")
    , @NamedQuery(name = "Busquedas.findByBusNumero", query = "SELECT b FROM Busquedas b WHERE b.busNumero = :busNumero")
    , @NamedQuery(name = "Busquedas.findByCatFuenteId", query = "SELECT b FROM Busquedas b WHERE b.catFuenteId = :catFuenteId")
    , @NamedQuery(name = "Busquedas.findByBusUsuario", query = "SELECT b FROM Busquedas b WHERE b.busUsuario = :busUsuario")
    , @NamedQuery(name = "Busquedas.findByBusFechaBusqueda", query = "SELECT b FROM Busquedas b WHERE b.busFechaBusqueda = :busFechaBusqueda")
    , @NamedQuery(name = "Busquedas.findByBusMotivo", query = "SELECT b FROM Busquedas b WHERE b.busMotivo = :busMotivo")
    , @NamedQuery(name = "Busquedas.findByBusDescripcion", query = "SELECT b FROM Busquedas b WHERE b.busDescripcion = :busDescripcion")
    , @NamedQuery(name = "Busquedas.findByBusMarcadoresMinimos", query = "SELECT b FROM Busquedas b WHERE b.busMarcadoresMinimos = :busMarcadoresMinimos")
    , @NamedQuery(name = "Busquedas.findByBusExclusionesMaximas", query = "SELECT b FROM Busquedas b WHERE b.busExclusionesMaximas = :busExclusionesMaximas")
    , @NamedQuery(name = "Busquedas.findByBusTipo", query = "SELECT b FROM Busquedas b WHERE b.busTipo = :busTipo")
    , @NamedQuery(name = "Busquedas.findByBusEstatus", query = "SELECT b FROM Busquedas b WHERE b.busEstatus = :busEstatus")
    , @NamedQuery(name = "Busquedas.findByBusComentarios", query = "SELECT b FROM Busquedas b WHERE b.busComentarios = :busComentarios")
    , @NamedQuery(name = "Busquedas.findByCatFreId", query = "SELECT b FROM Busquedas b WHERE b.catFreId = :catFreId")
    , @NamedQuery(name = "Busquedas.findByBusDescartarPerRevision", query = "SELECT b FROM Busquedas b WHERE b.busDescartarPerRevision = :busDescartarPerRevision")
    , @NamedQuery(name = "Busquedas.findByBusPerfilObjetivo", query = "SELECT b FROM Busquedas b WHERE b.busPerfilObjetivo = :busPerfilObjetivo")
    , @NamedQuery(name = "Busquedas.findByBusPerfilPadre", query = "SELECT b FROM Busquedas b WHERE b.busPerfilPadre = :busPerfilPadre")
    , @NamedQuery(name = "Busquedas.findByBusPerfilMadre", query = "SELECT b FROM Busquedas b WHERE b.busPerfilMadre = :busPerfilMadre")})
public class Busquedas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "bus_id")
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Integer busId;
    @Column(name = "bus_numero")
    private String busNumero;
    @Column(name = "cat_fuente_id")
    private Integer catFuenteId;
    @Column(name = "bus_usuario")
    private Integer busUsuario;
    @Column(name = "bus_fecha_busqueda")
    @Temporal(TemporalType.DATE)
    private Date busFechaBusqueda;
    @Column(name = "bus_motivo")
    private String busMotivo;
    @Column(name = "bus_descripcion")
    private String busDescripcion;
    @Column(name = "bus_marcadores_minimos")
    private Integer busMarcadoresMinimos;
    @Column(name = "bus_exclusiones_maximas")
    private Integer busExclusionesMaximas;
    @Column(name = "bus_tipo")
    private String busTipo;
    @Column(name = "bus_estatus")
    private String busEstatus;
    @Column(name = "bus_comentarios")
    private String busComentarios;
    @Column(name = "cat_fre_id")
    private Integer catFreId;
    @Column(name = "bus_descartar_per_revision")
    private Integer busDescartarPerRevision;
    @Column(name = "bus_perfil_objetivo")
    private Integer busPerfilObjetivo;
    @Column(name = "bus_perfil_padre")
    private Integer busPerfilPadre;
    @Column(name = "bus_perfil_madre")
    private Integer busPerfilMadre;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "rcBusId", fetch = FetchType.LAZY)
    private List<ResultadoCompatible> resultadoCompatibleList;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "ebBusId", fetch = FetchType.LAZY)
    private List<EtiquetasBusqueda> etiquetasBusquedaList;

    public Busquedas() {
    }

    public Busquedas(Integer catFuenteId, Integer busUsuario, String busMotivo, String busDescripcion, Integer busMarcadoresMinimos, Integer busExclusionesMaximas, String busTipo, String busEstatus, Integer catFreId, Integer busDescartarPerRevision, Integer busPerfilObjetivo, Integer busPerfilPadre, Integer busPerfilMadre) {
        this.catFuenteId = catFuenteId;
        this.busUsuario = busUsuario;
        this.busMotivo = busMotivo;
        this.busDescripcion = busDescripcion;
        this.busMarcadoresMinimos = busMarcadoresMinimos;
        this.busExclusionesMaximas = busExclusionesMaximas;
        this.busTipo = busTipo;
        this.busEstatus = busEstatus;
        this.catFreId = catFreId;
        this.busDescartarPerRevision = busDescartarPerRevision;
        this.busPerfilObjetivo = busPerfilObjetivo;
        this.busPerfilPadre = busPerfilPadre;
        this.busPerfilMadre = busPerfilMadre;
        this.busFechaBusqueda = new Date();
    }
    
    public Busquedas(Integer busId) {
        this.busId = busId;
    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    public String getBusNumero() {
        return busNumero;
    }

    public void setBusNumero(String busNumero) {
        this.busNumero = busNumero;
    }

    public Integer getCatFuenteId() {
        return catFuenteId;
    }

    public void setCatFuenteId(Integer catFuenteId) {
        this.catFuenteId = catFuenteId;
    }

    public Integer getBusUsuario() {
        return busUsuario;
    }

    public void setBusUsuario(Integer busUsuario) {
        this.busUsuario = busUsuario;
    }

    public Date getBusFechaBusqueda() {
        return busFechaBusqueda;
    }

    public void setBusFechaBusqueda(Date busFechaBusqueda) {
        this.busFechaBusqueda = busFechaBusqueda;
    }

    public String getBusMotivo() {
        return busMotivo;
    }

    public void setBusMotivo(String busMotivo) {
        this.busMotivo = busMotivo;
    }

    public String getBusDescripcion() {
        return busDescripcion;
    }

    public void setBusDescripcion(String busDescripcion) {
        this.busDescripcion = busDescripcion;
    }

    public Integer getBusMarcadoresMinimos() {
        return busMarcadoresMinimos;
    }

    public void setBusMarcadoresMinimos(Integer busMarcadoresMinimos) {
        this.busMarcadoresMinimos = busMarcadoresMinimos;
    }

    public Integer getBusExclusionesMaximas() {
        return busExclusionesMaximas;
    }

    public void setBusExclusionesMaximas(Integer busExclusionesMaximas) {
        this.busExclusionesMaximas = busExclusionesMaximas;
    }

    public String getBusTipo() {
        return busTipo;
    }

    public void setBusTipo(String busTipo) {
        this.busTipo = busTipo;
    }

    public String getBusEstatus() {
        return busEstatus;
    }

    public void setBusEstatus(String busEstatus) {
        this.busEstatus = busEstatus;
    }

    public String getBusComentarios() {
        return busComentarios;
    }

    public void setBusComentarios(String busComentarios) {
        this.busComentarios = busComentarios;
    }

    public Integer getCatFreId() {
        return catFreId;
    }

    public void setCatFreId(Integer catFreId) {
        this.catFreId = catFreId;
    }

    public Integer getBusDescartarPerRevision() {
        return busDescartarPerRevision;
    }

    public void setBusDescartarPerRevision(Integer busDescartarPerRevision) {
        this.busDescartarPerRevision = busDescartarPerRevision;
    }

    public Integer getBusPerfilObjetivo() {
        return busPerfilObjetivo;
    }

    public void setBusPerfilObjetivo(Integer busPerfilObjetivo) {
        this.busPerfilObjetivo = busPerfilObjetivo;
    }

    public Integer getBusPerfilPadre() {
        return busPerfilPadre;
    }

    public void setBusPerfilPadre(Integer busPerfilPadre) {
        this.busPerfilPadre = busPerfilPadre;
    }

    public Integer getBusPerfilMadre() {
        return busPerfilMadre;
    }

    public void setBusPerfilMadre(Integer busPerfilMadre) {
        this.busPerfilMadre = busPerfilMadre;
    }

    @XmlTransient
    public List<ResultadoCompatible> getResultadoCompatibleList() {
        return resultadoCompatibleList;
    }

    public void setResultadoCompatibleList(List<ResultadoCompatible> resultadoCompatibleList) {
        this.resultadoCompatibleList = resultadoCompatibleList;
    }

    @XmlTransient
    public List<EtiquetasBusqueda> getEtiquetasBusquedaList() {
        return etiquetasBusquedaList;
    }

    public void setEtiquetasBusquedaList(List<EtiquetasBusqueda> etiquetasBusquedaList) {
        this.etiquetasBusquedaList = etiquetasBusquedaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (busId != null ? busId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Busquedas)) {
            return false;
        }
        Busquedas other = (Busquedas) object;
        if ((this.busId == null && other.busId != null) || (this.busId != null && !this.busId.equals(other.busId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mx.genotipos.dao.model.Busqueda[ busId=" + busId + " ]";
    }
    
}
