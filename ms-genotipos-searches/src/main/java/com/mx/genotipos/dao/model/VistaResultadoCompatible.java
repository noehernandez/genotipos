/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "VISTA_RESULTADO_COMPATIBLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VistaResultadoCompatible.findAll", query = "SELECT v FROM VistaResultadoCompatible v")
    , @NamedQuery(name = "VistaResultadoCompatible.findByRcId", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.rcId = :rcId")
    , @NamedQuery(name = "VistaResultadoCompatible.findByRcBusId", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.rcBusId = :rcBusId")
    , @NamedQuery(name = "VistaResultadoCompatible.findByEstado", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.estado = :estado")
    , @NamedQuery(name = "VistaResultadoCompatible.findByPerfilObjetivoId", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.perfilObjetivoId = :perfilObjetivoId")
    , @NamedQuery(name = "VistaResultadoCompatible.findByPerfilObjetivo", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.perfilObjetivo = :perfilObjetivo")
    , @NamedQuery(name = "VistaResultadoCompatible.findByPerfilCompatibleId", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.perfilCompatibleId = :perfilCompatibleId")
    , @NamedQuery(name = "VistaResultadoCompatible.findByPerfilCompatible", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.perfilCompatible = :perfilCompatible")
    , @NamedQuery(name = "VistaResultadoCompatible.findByPerfilMadreId", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.perfilMadreId = :perfilMadreId")
    , @NamedQuery(name = "VistaResultadoCompatible.findByPerfilMadre", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.perfilMadre = :perfilMadre")
    , @NamedQuery(name = "VistaResultadoCompatible.findByPerfilPadreId", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.perfilPadreId = :perfilPadreId")
    , @NamedQuery(name = "VistaResultadoCompatible.findByPerfilPadre", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.perfilPadre = :perfilPadre")
    , @NamedQuery(name = "VistaResultadoCompatible.findByRcAmel", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.rcAmel = :rcAmel")
    , @NamedQuery(name = "VistaResultadoCompatible.findByRcIp", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.rcIp = :rcIp")
    , @NamedQuery(name = "VistaResultadoCompatible.findByRcPp", query = "SELECT v FROM VistaResultadoCompatible v WHERE v.rcPp = :rcPp")})
public class VistaResultadoCompatible implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Column(name = "rc_id")
    @Id
    private int rcId;
    @Column(name = "rc_bus_id")
    private Integer rcBusId;
    @Column(name = "estado")
    private String estado;
    @Column(name = "perfil_objetivo_id")
    private Integer perfilObjetivoId;
    @Column(name = "perfil_objetivo")
    private String perfilObjetivo;
    @Column(name = "perfil_compatible_id")
    private Integer perfilCompatibleId;
    @Column(name = "perfil_compatible")
    private String perfilCompatible;
    @Column(name = "perfil_madre_id")
    private Integer perfilMadreId;
    @Column(name = "perfil_madre")
    private String perfilMadre;
    @Column(name = "perfil_padre_id")
    private Integer perfilPadreId;
    @Column(name = "perfil_padre")
    private String perfilPadre;
    @Column(name = "rc_amel")
    private String rcAmel;
    @Column(name = "rc_ip")
    private String rcIp;
    @Column(name = "rc_pp")
    private String rcPp;

    public VistaResultadoCompatible() {
    }

    public int getRcId() {
        return rcId;
    }

    public void setRcId(int rcId) {
        this.rcId = rcId;
    }

    public Integer getRcBusId() {
        return rcBusId;
    }

    public void setRcBusId(Integer rcBusId) {
        this.rcBusId = rcBusId;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getPerfilObjetivoId() {
        return perfilObjetivoId;
    }

    public void setPerfilObjetivoId(Integer perfilObjetivoId) {
        this.perfilObjetivoId = perfilObjetivoId;
    }

    public String getPerfilObjetivo() {
        return perfilObjetivo;
    }

    public void setPerfilObjetivo(String perfilObjetivo) {
        this.perfilObjetivo = perfilObjetivo;
    }

    public Integer getPerfilCompatibleId() {
        return perfilCompatibleId;
    }

    public void setPerfilCompatibleId(Integer perfilCompatibleId) {
        this.perfilCompatibleId = perfilCompatibleId;
    }

    public String getPerfilCompatible() {
        return perfilCompatible;
    }

    public void setPerfilCompatible(String perfilCompatible) {
        this.perfilCompatible = perfilCompatible;
    }

    public Integer getPerfilMadreId() {
        return perfilMadreId;
    }

    public void setPerfilMadreId(Integer perfilMadreId) {
        this.perfilMadreId = perfilMadreId;
    }

    public String getPerfilMadre() {
        return perfilMadre;
    }

    public void setPerfilMadre(String perfilMadre) {
        this.perfilMadre = perfilMadre;
    }

    public Integer getPerfilPadreId() {
        return perfilPadreId;
    }

    public void setPerfilPadreId(Integer perfilPadreId) {
        this.perfilPadreId = perfilPadreId;
    }

    public String getPerfilPadre() {
        return perfilPadre;
    }

    public void setPerfilPadre(String perfilPadre) {
        this.perfilPadre = perfilPadre;
    }

    public String getRcAmel() {
        return rcAmel;
    }

    public void setRcAmel(String rcAmel) {
        this.rcAmel = rcAmel;
    }

    public String getRcIp() {
        return rcIp;
    }

    public void setRcIp(String rcIp) {
        this.rcIp = rcIp;
    }

    public String getRcPp() {
        return rcPp;
    }

    public void setRcPp(String rcPp) {
        this.rcPp = rcPp;
    }
    
}
