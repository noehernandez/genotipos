/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "ETIQUETAS_BUSQUEDA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EtiquetasBusqueda.findAll", query = "SELECT e FROM EtiquetasBusqueda e")
    , @NamedQuery(name = "EtiquetasBusqueda.findByEbId", query = "SELECT e FROM EtiquetasBusqueda e WHERE e.ebId = :ebId")
    , @NamedQuery(name = "EtiquetasBusqueda.findByEbNombre", query = "SELECT e FROM EtiquetasBusqueda e WHERE e.ebNombre = :ebNombre")
    , @NamedQuery(name = "EtiquetasBusqueda.findByEbPerfil", query = "SELECT e FROM EtiquetasBusqueda e WHERE e.ebPerfil = :ebPerfil")})
public class EtiquetasBusqueda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name = "eb_id")
    private Integer ebId;
    @Column(name = "eb_nombre")
    private String ebNombre;
    @Column(name = "eb_perfil")
    private String ebPerfil;
    @JoinColumn(name = "eb_bus_id", referencedColumnName = "bus_id")
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Busquedas ebBusId;

    public EtiquetasBusqueda() {
    }

    public EtiquetasBusqueda(String ebNombre, String ebPerfil, Busquedas ebBusId) {
        this.ebNombre = ebNombre;
        this.ebPerfil = ebPerfil;
        this.ebBusId = ebBusId;
    }

    public EtiquetasBusqueda(Integer ebId) {
        this.ebId = ebId;
    }

    public Integer getEbId() {
        return ebId;
    }

    public void setEbId(Integer ebId) {
        this.ebId = ebId;
    }

    public String getEbNombre() {
        return ebNombre;
    }

    public void setEbNombre(String ebNombre) {
        this.ebNombre = ebNombre;
    }

    public String getEbPerfil() {
        return ebPerfil;
    }

    public void setEbPerfil(String ebPerfil) {
        this.ebPerfil = ebPerfil;
    }

    public Busquedas getEbBusId() {
        return ebBusId;
    }

    public void setEbBusId(Busquedas ebBusId) {
        this.ebBusId = ebBusId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ebId != null ? ebId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EtiquetasBusqueda)) {
            return false;
        }
        EtiquetasBusqueda other = (EtiquetasBusqueda) object;
        if ((this.ebId == null && other.ebId != null) || (this.ebId != null && !this.ebId.equals(other.ebId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mx.genotipos.dao.model.EtiquetasBusqueda[ ebId=" + ebId + " ]";
    }
    
}
