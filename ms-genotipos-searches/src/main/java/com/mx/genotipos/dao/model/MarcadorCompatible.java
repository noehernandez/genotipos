/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "MARCADOR_COMPATIBLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MarcadorCompatible.findAll", query = "SELECT m FROM MarcadorCompatible m")
    , @NamedQuery(name = "MarcadorCompatible.findByMcId", query = "SELECT m FROM MarcadorCompatible m WHERE m.mcId = :mcId")
    , @NamedQuery(name = "MarcadorCompatible.findByCatMarId", query = "SELECT m FROM MarcadorCompatible m WHERE m.catMarId = :catMarId")
    , @NamedQuery(name = "MarcadorCompatible.findByCatMar", query = "SELECT m FROM MarcadorCompatible m WHERE m.catMar = :catMar")
    , @NamedQuery(name = "MarcadorCompatible.findByMcAleloxOrigen", query = "SELECT m FROM MarcadorCompatible m WHERE m.mcAleloxOrigen = :mcAleloxOrigen")
    , @NamedQuery(name = "MarcadorCompatible.findByMcAleloyOrigen", query = "SELECT m FROM MarcadorCompatible m WHERE m.mcAleloyOrigen = :mcAleloyOrigen")
    , @NamedQuery(name = "MarcadorCompatible.findByMcAleloxCompatible", query = "SELECT m FROM MarcadorCompatible m WHERE m.mcAleloxCompatible = :mcAleloxCompatible")
    , @NamedQuery(name = "MarcadorCompatible.findByMcAleloyCompatible", query = "SELECT m FROM MarcadorCompatible m WHERE m.mcAleloyCompatible = :mcAleloyCompatible")
    , @NamedQuery(name = "MarcadorCompatible.findByMcCoincideAlelox", query = "SELECT m FROM MarcadorCompatible m WHERE m.mcCoincideAlelox = :mcCoincideAlelox")
    , @NamedQuery(name = "MarcadorCompatible.findByMcCoincideAleloy", query = "SELECT m FROM MarcadorCompatible m WHERE m.mcCoincideAleloy = :mcCoincideAleloy")
    , @NamedQuery(name = "MarcadorCompatible.findByMcAleloxMadre", query = "SELECT m FROM MarcadorCompatible m WHERE m.mcAleloxMadre = :mcAleloxMadre")
    , @NamedQuery(name = "MarcadorCompatible.findByMcAleloyMadre", query = "SELECT m FROM MarcadorCompatible m WHERE m.mcAleloyMadre = :mcAleloyMadre")
    , @NamedQuery(name = "MarcadorCompatible.findByMcCoincideAleloxMadre", query = "SELECT m FROM MarcadorCompatible m WHERE m.mcCoincideAleloxMadre = :mcCoincideAleloxMadre")
    , @NamedQuery(name = "MarcadorCompatible.findByMcCoincideAleloyMadre", query = "SELECT m FROM MarcadorCompatible m WHERE m.mcCoincideAleloyMadre = :mcCoincideAleloyMadre")})
public class MarcadorCompatible implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name = "mc_id")
    private Integer mcId;
    @Column(name = "cat_mar_id")
    private Integer catMarId;
    @Column(name = "cat_mar")
    private String catMar;
    @Column(name = "mc_alelox_origen")
    private String mcAleloxOrigen;
    @Column(name = "mc_aleloy_origen")
    private String mcAleloyOrigen;
    @Column(name = "mc_alelox_compatible")
    private String mcAleloxCompatible;
    @Column(name = "mc_aleloy_compatible")
    private String mcAleloyCompatible;
    @Column(name = "mc_coincide_alelox")
    private Integer mcCoincideAlelox;
    @Column(name = "mc_coincide_aleloy")
    private Integer mcCoincideAleloy;
    @Column(name = "mc_alelox_madre")
    private String mcAleloxMadre;
    @Column(name = "mc_aleloy_madre")
    private String mcAleloyMadre;
    @Column(name = "mc_coincide_alelox_madre")
    private Integer mcCoincideAleloxMadre;
    @Column(name = "mc_coincide_aleloy_madre")
    private Integer mcCoincideAleloyMadre;
    @JoinColumn(name = "mc_rc_id", referencedColumnName = "rc_id")
    @ManyToOne( fetch = FetchType.LAZY)
    private ResultadoCompatible mcRcId;

    public MarcadorCompatible() {
    }

    public MarcadorCompatible(Integer catMarId, String catMar, String mcAleloxOrigen, String mcAleloyOrigen, String mcAleloxCompatible, String mcAleloyCompatible, Integer mcCoincideAlelox, Integer mcCoincideAleloy, String mcAleloxMadre, String mcAleloyMadre, Integer mcCoincideAleloxMadre, Integer mcCoincideAleloyMadre, ResultadoCompatible mcRcId) {
        this.catMarId = catMarId;
        this.catMar = catMar;
        this.mcAleloxOrigen = mcAleloxOrigen;
        this.mcAleloyOrigen = mcAleloyOrigen;
        this.mcAleloxCompatible = mcAleloxCompatible;
        this.mcAleloyCompatible = mcAleloyCompatible;
        this.mcCoincideAlelox = mcCoincideAlelox;
        this.mcCoincideAleloy = mcCoincideAleloy;
        this.mcAleloxMadre = mcAleloxMadre;
        this.mcAleloyMadre = mcAleloyMadre;
        this.mcCoincideAleloxMadre = mcCoincideAleloxMadre;
        this.mcCoincideAleloyMadre = mcCoincideAleloyMadre;
        this.mcRcId = mcRcId;
    }
    
    

    public MarcadorCompatible(Integer mcId) {
        this.mcId = mcId;
    }

    public Integer getMcId() {
        return mcId;
    }

    public void setMcId(Integer mcId) {
        this.mcId = mcId;
    }

    public Integer getCatMarId() {
        return catMarId;
    }

    public void setCatMarId(Integer catMarId) {
        this.catMarId = catMarId;
    }

    public String getCatMar() {
        return catMar;
    }

    public void setCatMar(String catMar) {
        this.catMar = catMar;
    }

    public String getMcAleloxOrigen() {
        return mcAleloxOrigen;
    }

    public void setMcAleloxOrigen(String mcAleloxOrigen) {
        this.mcAleloxOrigen = mcAleloxOrigen;
    }

    public String getMcAleloyOrigen() {
        return mcAleloyOrigen;
    }

    public void setMcAleloyOrigen(String mcAleloyOrigen) {
        this.mcAleloyOrigen = mcAleloyOrigen;
    }

    public String getMcAleloxCompatible() {
        return mcAleloxCompatible;
    }

    public void setMcAleloxCompatible(String mcAleloxCompatible) {
        this.mcAleloxCompatible = mcAleloxCompatible;
    }

    public String getMcAleloyCompatible() {
        return mcAleloyCompatible;
    }

    public void setMcAleloyCompatible(String mcAleloyCompatible) {
        this.mcAleloyCompatible = mcAleloyCompatible;
    }

    public Integer getMcCoincideAlelox() {
        return mcCoincideAlelox;
    }

    public void setMcCoincideAlelox(Integer mcCoincideAlelox) {
        this.mcCoincideAlelox = mcCoincideAlelox;
    }

    public Integer getMcCoincideAleloy() {
        return mcCoincideAleloy;
    }

    public void setMcCoincideAleloy(Integer mcCoincideAleloy) {
        this.mcCoincideAleloy = mcCoincideAleloy;
    }

    public String getMcAleloxMadre() {
        return mcAleloxMadre;
    }

    public void setMcAleloxMadre(String mcAleloxMadre) {
        this.mcAleloxMadre = mcAleloxMadre;
    }

    public String getMcAleloyMadre() {
        return mcAleloyMadre;
    }

    public void setMcAleloyMadre(String mcAleloyMadre) {
        this.mcAleloyMadre = mcAleloyMadre;
    }

    public Integer getMcCoincideAleloxMadre() {
        return mcCoincideAleloxMadre;
    }

    public void setMcCoincideAleloxMadre(Integer mcCoincideAleloxMadre) {
        this.mcCoincideAleloxMadre = mcCoincideAleloxMadre;
    }

    public Integer getMcCoincideAleloyMadre() {
        return mcCoincideAleloyMadre;
    }

    public void setMcCoincideAleloyMadre(Integer mcCoincideAleloyMadre) {
        this.mcCoincideAleloyMadre = mcCoincideAleloyMadre;
    }

    public ResultadoCompatible getMcRcId() {
        return mcRcId;
    }

    public void setMcRcId(ResultadoCompatible mcRcId) {
        this.mcRcId = mcRcId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mcId != null ? mcId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MarcadorCompatible)) {
            return false;
        }
        MarcadorCompatible other = (MarcadorCompatible) object;
        if ((this.mcId == null && other.mcId != null) || (this.mcId != null && !this.mcId.equals(other.mcId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mx.genotipos.dao.model.MarcadorCompatible[ mcId=" + mcId + " ]";
    }
    
}
