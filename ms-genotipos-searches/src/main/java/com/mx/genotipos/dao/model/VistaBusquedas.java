/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.dao.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NS-448
 */
@Entity
@Table(name = "VISTA_BUSQUEDAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VistaBusquedas.findAll", query = "SELECT v FROM VistaBusquedas v")
    , @NamedQuery(name = "VistaBusquedas.findByBusId", query = "SELECT v FROM VistaBusquedas v WHERE v.busId = :busId")
    , @NamedQuery(name = "VistaBusquedas.findByBusNumero", query = "SELECT v FROM VistaBusquedas v WHERE v.busNumero = :busNumero")
    , @NamedQuery(name = "VistaBusquedas.findByFntId", query = "SELECT v FROM VistaBusquedas v WHERE v.fntId = :fntId")
    , @NamedQuery(name = "VistaBusquedas.findByFntNombreFuente", query = "SELECT v FROM VistaBusquedas v WHERE v.fntNombreFuente = :fntNombreFuente")
    , @NamedQuery(name = "VistaBusquedas.findByUsuario", query = "SELECT v FROM VistaBusquedas v WHERE v.usuario = :usuario")
    , @NamedQuery(name = "VistaBusquedas.findByBusFechaBusqueda", query = "SELECT v FROM VistaBusquedas v WHERE v.busFechaBusqueda = :busFechaBusqueda")
    , @NamedQuery(name = "VistaBusquedas.findByBusMotivo", query = "SELECT v FROM VistaBusquedas v WHERE v.busMotivo = :busMotivo")
    , @NamedQuery(name = "VistaBusquedas.findByBusDescripcion", query = "SELECT v FROM VistaBusquedas v WHERE v.busDescripcion = :busDescripcion")
    , @NamedQuery(name = "VistaBusquedas.findByBusComentarios", query = "SELECT v FROM VistaBusquedas v WHERE v.busComentarios = :busComentarios")
    , @NamedQuery(name = "VistaBusquedas.findByBusMarcadoresMinimos", query = "SELECT v FROM VistaBusquedas v WHERE v.busMarcadoresMinimos = :busMarcadoresMinimos")
    , @NamedQuery(name = "VistaBusquedas.findByBusExclusionesMaximas", query = "SELECT v FROM VistaBusquedas v WHERE v.busExclusionesMaximas = :busExclusionesMaximas")
    , @NamedQuery(name = "VistaBusquedas.findByFreId", query = "SELECT v FROM VistaBusquedas v WHERE v.freId = :freId")
    , @NamedQuery(name = "VistaBusquedas.findByFreNombre", query = "SELECT v FROM VistaBusquedas v WHERE v.freNombre = :freNombre")
    , @NamedQuery(name = "VistaBusquedas.findByBusDescartarPerRevision", query = "SELECT v FROM VistaBusquedas v WHERE v.busDescartarPerRevision = :busDescartarPerRevision")})
public class VistaBusquedas implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Column(name = "bus_id")
    @Id
    private int busId;
    @Column(name = "bus_numero")
    private String busNumero;
    @Column(name = "fnt_id")
    private Integer fntId;
    @Column(name = "fnt_nombre_fuente")
    private String fntNombreFuente;
    @Column(name = "usuario")
    private String usuario;
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Column(name = "bus_tipo")
    private String busTipo;
    @Column(name = "bus_estatus")
    private String busEstatus;
    @Column(name = "bus_fecha_busqueda")
    @Temporal(TemporalType.DATE)
    private Date busFechaBusqueda;
    @Column(name = "bus_motivo")
    private String busMotivo;
    @Column(name = "bus_descripcion")
    private String busDescripcion;
    @Column(name = "bus_comentarios")
    private String busComentarios;
    @Column(name = "bus_marcadores_minimos")
    private Integer busMarcadoresMinimos;
    @Column(name = "bus_exclusiones_maximas")
    private Integer busExclusionesMaximas;
    @Column(name = "fre_id")
    private Integer freId;
    @Column(name = "fre_nombre")
    private String freNombre;
    @Column(name = "bus_descartar_per_revision")
    private Integer busDescartarPerRevision;

    public VistaBusquedas() {
    }

    public int getBusId() {
        return busId;
    }

    public void setBusId(int busId) {
        this.busId = busId;
    }

    public String getBusNumero() {
        return busNumero;
    }

    public void setBusNumero(String busNumero) {
        this.busNumero = busNumero;
    }

    public Integer getFntId() {
        return fntId;
    }

    public void setFntId(Integer fntId) {
        this.fntId = fntId;
    }

    public String getFntNombreFuente() {
        return fntNombreFuente;
    }

    public void setFntNombreFuente(String fntNombreFuente) {
        this.fntNombreFuente = fntNombreFuente;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Date getBusFechaBusqueda() {
        return busFechaBusqueda;
    }

    public void setBusFechaBusqueda(Date busFechaBusqueda) {
        this.busFechaBusqueda = busFechaBusqueda;
    }

    public String getBusMotivo() {
        return busMotivo;
    }

    public void setBusMotivo(String busMotivo) {
        this.busMotivo = busMotivo;
    }

    public String getBusDescripcion() {
        return busDescripcion;
    }

    public void setBusDescripcion(String busDescripcion) {
        this.busDescripcion = busDescripcion;
    }

    public String getBusComentarios() {
        return busComentarios;
    }

    public void setBusComentarios(String busComentarios) {
        this.busComentarios = busComentarios;
    }

    public Integer getBusMarcadoresMinimos() {
        return busMarcadoresMinimos;
    }

    public void setBusMarcadoresMinimos(Integer busMarcadoresMinimos) {
        this.busMarcadoresMinimos = busMarcadoresMinimos;
    }

    public Integer getBusExclusionesMaximas() {
        return busExclusionesMaximas;
    }

    public void setBusExclusionesMaximas(Integer busExclusionesMaximas) {
        this.busExclusionesMaximas = busExclusionesMaximas;
    }

    public Integer getFreId() {
        return freId;
    }

    public void setFreId(Integer freId) {
        this.freId = freId;
    }

    public String getFreNombre() {
        return freNombre;
    }

    public void setFreNombre(String freNombre) {
        this.freNombre = freNombre;
    }

    public Integer getBusDescartarPerRevision() {
        return busDescartarPerRevision;
    }

    public void setBusDescartarPerRevision(Integer busDescartarPerRevision) {
        this.busDescartarPerRevision = busDescartarPerRevision;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getBusTipo() {
        return busTipo;
    }

    public void setBusTipo(String busTipo) {
        this.busTipo = busTipo;
    }

    public String getBusEstatus() {
        return busEstatus;
    }

    public void setBusEstatus(String busEstatus) {
        this.busEstatus = busEstatus;
    }
    
}
