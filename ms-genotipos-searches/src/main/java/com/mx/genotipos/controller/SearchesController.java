/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.controller;

import com.mx.genotipos.dto.Response;
import com.mx.genotipos.dto.SearchResult;
import com.mx.genotipos.service.ISearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author NS-448
 */
@RestController
@RequestMapping("/api/searches")
@CrossOrigin(origins = {"http://localhost:4200","http://3.17.128.176:4200","http://18.216.205.13:8080","*"} , allowedHeaders = "*")
public class SearchesController {
    
    @Autowired
    private ISearchService searchService;
    
    @GetMapping("/active/{userId}")
    public Response getSearches(@PathVariable Integer userId){
        return searchService.getSearches(userId);
    }
    
    @PostMapping()
    public Response saveSearch(@RequestBody SearchResult searchResult){
        return searchService.saveSearch(searchResult);
    }
    
    @GetMapping("/{searchId}")
    public Response getSearchResult(@PathVariable Integer searchId){
        return searchService.getSearchResult(searchId);
    }
    
    @GetMapping("/compatible-profile/{compatibleProfileId}")
    public Response getCompatibleProfiles(@PathVariable Integer compatibleProfileId){
        return searchService.getCompatibleProfiles(compatibleProfileId);
    }
}
