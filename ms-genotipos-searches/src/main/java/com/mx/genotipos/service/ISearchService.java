/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service;

import com.mx.genotipos.dto.Response;
import com.mx.genotipos.dto.SearchResult;

/**
 *
 * @author NS-448
 */
public interface ISearchService {
    
    public Response saveSearch(SearchResult searchResult);
    public Response getSearchResult(Integer searchId);
    public Response getSearches(Integer userId);
    public Response getCompatibleProfiles(Integer compatibleProfileId);
}
