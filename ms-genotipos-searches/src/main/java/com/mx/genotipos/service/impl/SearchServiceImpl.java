/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mx.genotipos.service.impl;

import com.mx.genotipos.dao.BusquedaDAO;
import com.mx.genotipos.dao.EtiquetasBusquedaDAO;
import com.mx.genotipos.dao.MarcadorCompatibleDAO;
import com.mx.genotipos.dao.ResultadoCompatibleDAO;
import com.mx.genotipos.dao.VistaBusquedasDAO;
import com.mx.genotipos.dao.VistaResultadoCompatibleDAO;
import com.mx.genotipos.dao.model.Busquedas;
import com.mx.genotipos.dao.model.EtiquetasBusqueda;
import com.mx.genotipos.dao.model.MarcadorCompatible;
import com.mx.genotipos.dao.model.ResultadoCompatible;
import com.mx.genotipos.dao.model.VistaBusquedas;
import com.mx.genotipos.dao.model.VistaResultadoCompatible;
import com.mx.genotipos.dto.Busqueda;
import com.mx.genotipos.dto.Compatible;
import com.mx.genotipos.dto.DetalleBusqueda;
import com.mx.genotipos.dto.PerfilesCompatibles;
import com.mx.genotipos.dto.Label;
import com.mx.genotipos.dto.Response;
import com.mx.genotipos.dto.Result;
import com.mx.genotipos.dto.SearchResult;
import com.mx.genotipos.dto.MarcadoresCompatibles;
import com.mx.genotipos.dto.Objetivo;
import com.mx.genotipos.dto.PerfilCompatible;
import com.mx.genotipos.service.ISearchService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author NS-448
 */
@Service
public class SearchServiceImpl implements ISearchService{
    
    @Autowired
    private BusquedaDAO busquedaDAO;
    
    @Autowired
    private VistaBusquedasDAO vistaBusquedasDAO;
    
    @Autowired
    private EtiquetasBusquedaDAO etiquetasBusquedaDAO;
    
    @Autowired
    private ResultadoCompatibleDAO resultadoCompatibleDAO;
    
    @Autowired
    private VistaResultadoCompatibleDAO vistaResultadoCompatibleDAO;
    
    @Autowired
    private MarcadorCompatibleDAO marcadorCompatibleDAO;
    
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-YYYY");

    @Override
    public Response saveSearch(SearchResult searchResult) {
        System.out.println("entrando a guardado de busqueda");
        Busquedas busqueda = busquedaToEntity(searchResult);
        busqueda = busquedaDAO.save(busqueda);
        return new Response(true, busqueda.getBusId(), "Exito", null);
    }
    
    @Override
    public Response getSearchResult(Integer searchId) {
        VistaBusquedas vistaBusqueda= vistaBusquedasDAO.findById(searchId).orElse(null);
        return new Response(true, 0,"Exito",vistaBusquedaToDetalleBusqueda(vistaBusqueda));
    }
    
    @Override
    public Response getCompatibleProfiles(Integer compatibleProfileId) {
        System.out.println("getCompatibleProfiles");
        VistaResultadoCompatible resultadoCompatible =  vistaResultadoCompatibleDAO.findById(compatibleProfileId).orElse(null);
        if(resultadoCompatible == null){
            return new Response(false, compatibleProfileId, "No existe el resultado", null);
        }
        PerfilesCompatibles perfilCompatible = getPerfilCompatible(resultadoCompatible);
        List<MarcadorCompatible> marcadoresCompatibles = marcadorCompatibleDAO.findByMcId(compatibleProfileId);
        perfilCompatible.setMarcadoresCompatibles(marcadoresCompatiblesToDTO(marcadoresCompatibles));
        System.out.println("End getCompatibleProfiles");
        return new Response(true,compatibleProfileId,"Exito",perfilCompatible);
    }
    
    @Override
    public Response getSearches(Integer userId) {
//        List<VistaBusquedas> vistaBusqueda= vistaBusquedasDAO.findByIdUsuario(userId);
        List<VistaBusquedas> vistaBusqueda= vistaBusquedasDAO.findAll(Sort.by("busId").descending());
        return new Response(true,0,"Exito",vistaBusquedaToBusquedaList(vistaBusqueda));
    }
    
    private List<Busqueda> vistaBusquedaToBusquedaList(List<VistaBusquedas> vistasBusqueda){
        List<Busqueda> busquedas = new ArrayList<>();
        if(vistasBusqueda != null){
            vistasBusqueda.stream().map(this::vistaBusquedaToBusqueda)
                    .forEach(busquedas::add);
        }
        return busquedas;
    }
    
    private Busqueda vistaBusquedaToBusqueda(VistaBusquedas vistaBusqueda){
        return new Busqueda(
                vistaBusqueda.getBusId(),
                    vistaBusqueda.getBusNumero(),
                    vistaBusqueda.getFntNombreFuente(),
                    vistaBusqueda.getBusMotivo(),
                    vistaBusqueda.getBusDescripcion(),
                    vistaBusqueda.getUsuario(),
                    vistaBusqueda.getBusTipo(),
                    vistaBusqueda.getBusEstatus(),
                    vistaBusqueda.getBusFechaBusqueda()==null?"":DATE_FORMAT.format(vistaBusqueda.getBusFechaBusqueda())
        );
    }
    private List<MarcadoresCompatibles> marcadoresCompatiblesToDTO(List<MarcadorCompatible> marcadoresCompatibles){
        List<MarcadoresCompatibles> marcadoresCompatiblesDto = new ArrayList<>();
        if(marcadoresCompatibles!=null){
            marcadoresCompatibles.stream().map(this::marcadorCompatibleToDTO)
                    .forEach(marcadoresCompatiblesDto::add);
        }
        return marcadoresCompatiblesDto;
    }
    
    private MarcadoresCompatibles marcadorCompatibleToDTO(MarcadorCompatible marcadorCompatible){
        return new MarcadoresCompatibles(
            marcadorCompatible.getCatMar(),
            marcadorCompatible.getCatMarId(),
            new Objetivo(marcadorCompatible.getMcAleloxOrigen(),marcadorCompatible.getMcAleloyOrigen()),
            new PerfilCompatible(marcadorCompatible.getMcAleloxCompatible(),marcadorCompatible.getMcCoincideAlelox(),marcadorCompatible.getMcAleloyCompatible(),marcadorCompatible.getMcCoincideAleloy()),
            new PerfilCompatible(),
            new PerfilCompatible(),
            "",
            "",
            "",
            ""
        );
    }
    private DetalleBusqueda vistaBusquedaToDetalleBusqueda(VistaBusquedas vistaBusqueda){
        if(vistaBusqueda!=null){
            List<EtiquetasBusqueda> etiquetasBusqueda = etiquetasBusquedaDAO.findByBusId(vistaBusqueda.getBusId());
            List<VistaResultadoCompatible> resultadosCompatibles =  vistaResultadoCompatibleDAO.findByBusId(vistaBusqueda.getBusId());
            return new DetalleBusqueda(
                vistaBusqueda.getBusComentarios(),
                vistaBusqueda.getBusMarcadoresMinimos()==null?"":vistaBusqueda.getBusMarcadoresMinimos().toString(),
                vistaBusqueda.getBusExclusionesMaximas()==null?"":vistaBusqueda.getBusExclusionesMaximas().toString(),
                vistaBusqueda.getFreNombre(),
                vistaBusqueda.getBusDescartarPerRevision(),
                getEtiquetasObjetivo(etiquetasBusqueda),
                getEtiquetasAComparar(etiquetasBusqueda),
                getPerfilesCompatibles(resultadosCompatibles),
                    vistaBusqueda.getBusId(),
                    vistaBusqueda.getBusNumero(),
                    vistaBusqueda.getFntNombreFuente(),
                    vistaBusqueda.getBusMotivo(),
                    vistaBusqueda.getBusDescripcion(),
                    vistaBusqueda.getUsuario(),
                    vistaBusqueda.getBusTipo(),
                    vistaBusqueda.getBusEstatus(),
                    vistaBusqueda.getBusFechaBusqueda()==null?"":DATE_FORMAT.format(vistaBusqueda.getBusFechaBusqueda())
            );
        }
        return new DetalleBusqueda();
    }
    
    private List<PerfilesCompatibles> getPerfilesCompatibles(List<VistaResultadoCompatible> resultadosCompatibles){
        List<PerfilesCompatibles> perfilesCompatibles= new ArrayList<>();
        resultadosCompatibles.stream().map(this::getPerfilCompatible)
                .forEach(perfilesCompatibles::add);
        return perfilesCompatibles;
    }
    
    private PerfilesCompatibles getPerfilCompatible(VistaResultadoCompatible resultadoCompatible){
        return new PerfilesCompatibles(
                resultadoCompatible.getRcId(),
                resultadoCompatible.getPerfilObjetivo(),
                resultadoCompatible.getPerfilCompatible(),
                resultadoCompatible.getPerfilPadre(),
                resultadoCompatible.getPerfilMadre(),
                resultadoCompatible.getPerfilObjetivo(),
                resultadoCompatible.getEstado(),
                resultadoCompatible.getRcAmel(),
                resultadoCompatible.getRcIp(),
                resultadoCompatible.getRcPp()
        );
    }
    
    private List<String> getEtiquetasObjetivo(List<EtiquetasBusqueda> etiquetasBusqueda){
        List<String> etiquetasObjetivo = new ArrayList<>();
        etiquetasBusqueda.stream().filter(e->e.getEbPerfil().equals("O"))
                .map(e -> e.getEbNombre())
                .forEach(etiquetasObjetivo::add);
        return etiquetasObjetivo;
    }
    private List<String> getEtiquetasAComparar(List<EtiquetasBusqueda> etiquetasBusqueda){
        List<String> etiquetasAComparar = new ArrayList<>();
        etiquetasBusqueda.stream().filter(e->e.getEbPerfil().equals("C"))
                .map(e -> e.getEbNombre())
                .forEach(etiquetasAComparar::add);
        return etiquetasAComparar;
    }
    
    private Busquedas busquedaToEntity(SearchResult searchResult){
        Busquedas busqueda = new Busquedas(
            searchResult.getIdFuente(),
            searchResult.getIdUsuario(),
            searchResult.getMotivo(),
            searchResult.getDescripcion(),
            searchResult.getNumeroMarcadores(),
            searchResult.getNumeroExclusiones(),
            searchResult.getTipo(),
            searchResult.getEstatus(),
            searchResult.getIdTablaFrecuencia(),
            searchResult.isDescartarPerfilesRevision()?1:0,
            searchResult.getIdPerfil(),
            searchResult.getIdPerfilProgenitor(),
            searchResult.getIdPerfilSeguro()
        );
        busqueda.setEtiquetasBusquedaList(etiquetasListToEntity(searchResult.getEtiquetasObjetivo(),searchResult.getEtiquetasAComparar(),busqueda));
        busqueda.setResultadoCompatibleList(resultadoListToEntity(searchResult.getBusquedas(),busqueda));
        return busqueda;
    }
    
    private List<ResultadoCompatible> resultadoListToEntity(List<Result> resultados, Busquedas busqueda){
        List<ResultadoCompatible> resultadoCompatible = new ArrayList<>();
        if(resultados!=null){
            System.out.println("entrando a resultados compatibles "+resultados.size());
            resultados.stream().map(r -> resultadoToEntity(r,busqueda))
                    .forEach(resultadoCompatible::add);
        }
        return resultadoCompatible;
    }
    
    private ResultadoCompatible resultadoToEntity(Result resultado, Busquedas busqueda){
        PerfilesCompatibles resultadoCompatibleDto = resultado.getResultado();
        ResultadoCompatible resultadoCompatible=null; 
        if(resultadoCompatibleDto!=null){
            resultadoCompatible = new ResultadoCompatible(
                parseToNumber(resultadoCompatibleDto.getEstado()),
                resultadoCompatibleDto.getGenotipoObjetivo()!=null?parseToNumber(resultadoCompatibleDto.getGenotipoObjetivo()):parseToNumber(resultadoCompatibleDto.getPerfilObjetivo()),
                parseToNumber(resultadoCompatibleDto.getGenotipoCompatible()),
                resultadoCompatibleDto.getIp(),
                resultadoCompatibleDto.getPp(),
                resultadoCompatibleDto.getAmel(),
                parseToNumber(resultadoCompatibleDto.getPerfilMadre()),
                parseToNumber(resultadoCompatibleDto.getPerfilPadre()),
                busqueda
            );
            resultadoCompatible.setMarcadorCompatibleList(marcadorCompatibleListToEntity(resultado.getComparativoMarcadores(),resultadoCompatible));
        }
        return resultadoCompatible;
    }
    
    private List<MarcadorCompatible> marcadorCompatibleListToEntity(List<MarcadoresCompatibles> marcadoresCompatiblesDto, ResultadoCompatible resultadoCompatible){
        List<MarcadorCompatible> marcadoresCompatibles = new ArrayList<>();
        if(marcadoresCompatiblesDto != null){
            marcadoresCompatiblesDto.stream().map(mc -> marcadorCompatibleToEntity(mc,resultadoCompatible))
                    .forEach(marcadoresCompatibles::add);
        }
        return marcadoresCompatibles;
    }
    
    private MarcadorCompatible marcadorCompatibleToEntity(MarcadoresCompatibles marcadorCompatibleDto, ResultadoCompatible resultadoCompatible){
        return new MarcadorCompatible(
            marcadorCompatibleDto.getIdMarcador(),
                marcadorCompatibleDto.getMarcador(),
                marcadorCompatibleDto.getObjetivo().getAlelo1(),
                marcadorCompatibleDto.getObjetivo().getAlelo2(),
                marcadorCompatibleDto.getCompatible().getAlelo1(),
                marcadorCompatibleDto.getCompatible().getAlelo2(),
                marcadorCompatibleDto.getCompatible().getCoincideAlelo1(),
                marcadorCompatibleDto.getCompatible().getCoincideAlelo2(),
                null,
                null,
                null,
                null,
                resultadoCompatible
        );
    }
    private Integer parseToNumber(String number){
        try{
            return Integer.parseInt(number);
        }catch(NumberFormatException e){
            return null;
        }
    }
    
    private List<EtiquetasBusqueda> etiquetasListToEntity(List<Label> etiquetasObjetivo,List<Label> etiquetasAComparar, Busquedas busqueda){
        List<EtiquetasBusqueda> etiquetasBusqueda = new ArrayList<>();
        if(etiquetasObjetivo!=null){
            etiquetasObjetivo.stream().map(e-> etiquetasToEntity(e,"O",busqueda))
                    .forEach(etiquetasBusqueda::add);
        }
        if(etiquetasAComparar!=null){
            etiquetasAComparar.stream().map(e-> etiquetasToEntity(e,"C",busqueda))
                    .forEach(etiquetasBusqueda::add);
        }
        
        return etiquetasBusqueda;
    }
    
    
    private EtiquetasBusqueda etiquetasToEntity(Label etiquetas, String perfil, Busquedas busqueda){
        return new EtiquetasBusqueda(etiquetas.getName(),perfil,busqueda);
    }
    
}
